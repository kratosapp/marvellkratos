//
//  MessageList.h
//  Marvell
//
//  Copyright (c) 2014 Marvell. All rights reserved.
//

#ifndef Marvell_MessageList_h
#define Marvell_MessageList_h

#define  MARVELL_NO_WIFI @"WiFi not enabled. Please enable WiFi and connect to the home network"
#define  MARVELL_NO_NETWORK @"WiFi is enabled but device is not connected to any network"
#define MARVELL_CHOOSE_PASSOWORD @"Please enter the password !!!"

#define MARVELL_APP_STARTED @"App Started";
#define MARVELL_INFO_SENT @"Please check the status messages. If not received, please retry.";
#define MARVELL_SUCCESSFUL_PROVISIONING @"Device is provisioned to Home Network";
#define INVALID_KEY_LENGTH @"Invalid key. Key must be empty or 16 characters.";
#define INVALID_PASSPHRASE_LENGTH @"Invalid passphrase. Passphrase must be 8 to 63 characters long";

#endif
