#ifndef __USER_SETTINGS_H__
#define __USER_SETTINGS_H__

/*
 * Configuration: Default
 */

#ifndef USE_SOFTWARE_AES_CRYPTO
#define MC200_AES_HW_ACCL
#endif

#define CYASSL_AES_COUNTER
#define NO_ERROR_STRINGS

#ifndef NO_FILESYSTEM
#define NO_FILESYSTEM
#endif

#define NO_DEV_RANDOM
#define NO_PSK

#define HAVE_SNI
#define HAVE_TLS_EXTENSIONS

#endif /* __USER_SETTINGS_H__ */
