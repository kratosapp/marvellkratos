/* cyassl_version.h.in
 *
 * Copyright (C) 2006-2014 wolfSSL Inc.  All rights reserved.
 *
 * This file is part of CyaSSL.
 *
 * Contact licensing@wolfssl.com with any questions or comments.
 *
 * http://www.wolfssl.com
 */



#pragma once

#ifdef __cplusplus
extern "C" {
#endif

#define LIBCYASSL_VERSION_STRING "3.3.0"
#define LIBCYASSL_VERSION_HEX 0x03003000

#ifdef __cplusplus
}
#endif

