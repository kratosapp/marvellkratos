# Copyright (C) 2008-2014, Marvell International Ltd.
# All Rights Reserved.
# Batch Build Applications using GNU Cross-Compiler and Make.
#
# Usage Guide:
#
# A simple Makefile to build the WMSDK and associated components.
#
#
#     mc200_defconfig   set SDK config for mc200 board. User can also change
#                       defconfig according to its requirement. All defconfigs
#                       are available at the path "wmsdk/build/config"
#     all:     		build SDK, boot2, sample applications
#     clean:   		clean everything

# CONFIG_CMD_FILE stores the various build configuration parameters
#
# Following are the build configuration parameters that are being saved
# in this CONFIG_CMD_FILE:
# SDK_DIR: 		SDK directory path.
# BOOT2_DIR: 		boot2 directory path
# SAMPLE_APP_DIR:	Sample applications directory path
# BOARD: 			Name of the board for which the applications
# 					need to be build
#
# Whenever user explicitly passes the value for any one or more of these
# parameters, values are saved in the CONFIG_CMD_FILE.
# Existing values in CONFIG_CMD_FILE are overwritten in this case.
#
# On the next time when user does not pass any values for these parameters,
# the values are read from CONFIG_CMD_FILE and used for building the
# SDK and applications
#
# In case if user does not set these parameters at all, the values for these
# parameters are found and set by this Makefile itself
#
CONFIG_CMD_FILE=.config_cmd

-include Makefile.devel
-include $(CONFIG_CMD_FILE)

# Optional variables to be passed
# All of the bundled components
export SDK_DIR ?= $(abspath $(wildcard wmsdk))
export BOOT2_DIR ?= $(abspath $(wildcard boot2*))
export SAMPLE_APP_DIR ?= $(abspath $(wildcard sample_apps))
export BOARDS_DIR ?= $(SDK_DIR)/src/boards

ifeq ($(strip $(SDK_DIR)),)
${error "SDK_DIR variable undef. Is wmsdk present in current directory ?"}
endif

ifeq ($(strip $(BOOT2_DIR)),)
${error "BOOT2_DIR variable undef. Is boot2 present in current directory ?"}
endif

ifeq ($(strip $(SAMPLE_APP_DIR)),)
${error "SAMPLE_APP_DIR variable undef. Is sample_apps present in current directory ?"}
endif

SDK_LAST_CONFIG=.last_config
# Board File
#
# The WMSDK libraries are developed such that they are independent
# of any specific 88MC200 based board.
#
# This is achieved through a board abstraction layer that needs to
# be implemented in a "board file". Supported boards are available
# in the boards directory.
#
# Boot2 and Sample Applications are built for a specific board.
# The default board is set to mw30x board
#
# Following are the different possible cases:
# * User does not pass values for BOARD and BOARD_FILE:
#   Following steps are executed:
# - Set the default values to BOARD and BOARD_FILE
# - This results copying BOARD_FILE to application
# - Save BOARD to CONFIG_CMD_FILE as S_BOARD
#
# * User passes value for BOARD and not for BOARD_FILE:
#   Following steps are executed:
# - Set BOARD_FILE based on value of BOARD
# - This results copying BOARD_FILE to application
# - Save BOARD to CONFIG_CMD_FILE as S_BOARD
#
# * User passes value for BOARD_FILE and not for BOARD:
#   Following steps are executed:
# - Set BOARD to the value given by S_BOARD in CONFIG_CMD_FILE
# - Use the passed value of BOARD_FILE
# - This results copying BOARD_FILE to application
#
# * User do not pass values for BOARD and BOARD_FILE after
#   going through one or more cases mentioned above:
#   Following steps are executed:
# - Set BOARD to the value given by S_BOARD in CONFIG_CMD_FILE
# - BOARD_FILE is not set, hence no board file is copied to
#   application
#

ifndef BOARD
ifndef S_BOARD
export BOARD ?= mw300_rd
else
export BOARD ?= $(S_BOARD)
endif
endif

ifeq ($(BOARD_FILE), )
BOARD_FILE := $(abspath $(BOARDS_DIR)/$(BOARD).c)
else
override BOARD_FILE := $(realpath $(BOARD_FILE))
BOARD=`basename $(BOARD_FILE) .c`
endif

export BOARD_FILE

include $(SDK_DIR)/build/env/sysutils.mak

# All generated binaries are stored in the directory which is named
# based on board for which they are build
BIN_TARGET= $(BOARD)

# Directory where built components will be installed
export BIN_DIR=bin
# Directory where SDK will be installed.
export SDK_BIN_PATH ?= $(CURDIR)/$(BIN_DIR)/$(notdir $(SDK_DIR))
# Directory where IAR build will be installed
SDK_IAR_PATH = $(CURDIR)/$(BIN_DIR)/wmsdk-iar
IAR_APP_DIR = $(SAMPLE_APP_DIR)
# Directory where the applications will be installed
APP_BIN_PATH = $(CURDIR)/$(BIN_DIR)/$(BIN_TARGET)

# Tools
export OPENOCD_DIR=$(SDK_DIR)/tools/mc200/OpenOCD
export TOOLS_DIR=$(SDK_DIR)/tools/src
export SDK_PATH=$(SDK_BIN_PATH)
export INSTALL_DIR=$(APP_BIN_PATH)
export OPENSSL_INSTALL_DIR
-include $(SDK_DIR)/.config

.PHONY : all clean apps apps_clean sdk sdk_clean boot2 boot2_clean help

## Handle build display
ifeq ($(strip $(NOISY)),)
export MAKEDIRPRINT := -s --no-print-directory
export AT=@
export SILENT=-s
endif

## Secure images. Supported only on mw30x
ifneq ($(strip $(SECURE_BOOT)),)
ifneq ($(strip $(SECURE_BOOT)), 0)
ifeq ($(CONFIG_CPU_MW300), y)
SECURE_CONF_DIR ?= sboot_conf
override SECURE_CONF_DIR := $(abspath $(SECURE_CONF_DIR))
SECURE_CONF_PATH_FILE = $(SECURE_CONF_DIR)/secure-csv-path

# Generate config file name if it's a config number
is_conf_file := $(shell echo $(SECURE_BOOT) | tr -d '[:space:][:digit:]' | wc -c | tr -d '[:space:]')
ifeq ($(is_conf_file), 0)
SECURE_CONF_FILE := $(SECURE_CONF_DIR)/security-c$(SECURE_BOOT).csv
else
SECURE_CONF_FILE := $(realpath $(SECURE_BOOT))
endif

ifeq ($(SECURE_CONF_FILE),)
${error Security Config File $(SECURE_BOOT) does not exist.};
else
OTP_FILE := $(SDK_PATH)/incl/secure_config.h

# Fix paths for cygwin
OS := $(shell uname)
ifneq ($(findstring CYGWIN, ${OS}), )
SECURE_CONF_FILE := $(shell cygpath -m $(SECURE_CONF_FILE))
OTP_FILE := $(shell cygpath -m $(OTP_FILE))
endif

export SECURE_CONF_FILE
export OTP_FILE
$(shell mkdir -p $(SECURE_CONF_DIR) && echo $(SECURE_CONF_FILE) > $(SECURE_CONF_PATH_FILE))
endif
else
${error Secure Boot is NOT supported on MC200};
endif
endif
endif

# By default, build all the components - SDK, boot2, Sample Applications
#
# Build and install the sdk first as boot2 and apps require the
# pre-built SDK (libraries).
#
# When developing applications, it is often convenient to change
# the all target to only build the apps to reduce the build time.


all: sdk boot2 apps

# This creates the security config file from config num
ifneq ($(SECURE_CONF_FILE),)
$(SECURE_CONF_FILE):
	@echo "[Creating Security Configuration $(SECURE_BOOT) ]"
	$(AT)$(SDK_DIR)/build/utils/create-secconf.sh $(SECURE_CONF_FILE) $(SECURE_CONF_DIR)
	@echo " [secure] Generated $(SECURE_CONF_FILE)"
	@echo ""
endif

# This target builds sdk followed by boot2. This target is useful for cases
# where sdk and boot2 need to be built, but not apps
sdk_and_boot2: boot2


# Clean everything
clean: sdk_clean boot2_clean apps_clean
	${AT}rm -fr $(CURDIR)/$(BIN_DIR)/*
	${AT}rm -f $(CONFIG_CMD_FILE) $(SDK_DIR)/$(SDK_LAST_CONFIG)

# Create the bin directory which stores all the application binaries
preq: params
	${AT}mkdir -p $(APP_BIN_PATH)

ifneq ($(findstring CYGWIN, ${OS}), )
 APPS:=$(shell echo -E "$(APPS)" | sed -e 's/\\\([a-zA-Z0-9]\)/\/\1/g;s/\([a-zA-Z]\):/\/cygdrive\/\1/g')
endif

# Build Sample Applications and install them in APP_BIN_PATH directory
ifeq ($(SECURE_CONF_FILE),)
apps: preq sdk
else
apps: preq sdk boot2 $(SECURE_CONF_FILE)
endif
	@echo ""
	@echo "[Building Sample Applications]"
ifeq ($(APPS), )
	${AT}$(MAKE) $(SILENT) $(WMSDK_PARALLEL_MAKE) -C $(SAMPLE_APP_DIR) all $(CYG_COMPAT)
	${AT}$(MAKE) $(SILENT) -C $(SAMPLE_APP_DIR) install $(CYG_COMPAT)
else
	${AT}$(MAKE) $(SILENT) $(WMSDK_PARALLEL_MAKE) -C $(SAMPLE_APP_DIR) APPS=$(APPS) all $(CYG_COMPAT)
	${AT}$(MAKE) $(SILENT) -C $(SAMPLE_APP_DIR) install APPS=$(APPS) $(CYG_COMPAT)
endif

# Clean Sample Applications
apps_clean:
	@echo "[Cleaning Sample Applications]"
	${AT}$(MAKE) $(SILENT) -C $(SAMPLE_APP_DIR) clean

# SDK Configuration
menuconfig config oldconfig:
	$(MAKE) -C $(SDK_DIR) $@
	${AT}$(COPY_CMD) -af $(SDK_DIR)/.config $(SDK_DIR)/$(SDK_LAST_CONFIG)

# User can either give complete path to config file (x/y/x_defconfig). Or
# he can give just the config name x_defconfig
%_defconfig: FORCE
	${AT}touch $(SDK_DIR)/$(SDK_LAST_CONFIG)
	@if [ $$(diff $(SDK_DIR)/build/config/$@ $(SDK_DIR)/$(SDK_LAST_CONFIG) 2> /dev/null | wc -l) -ne "0" ]; then \
		echo "[Config file has changed, hence cleaning the SDK]"; \
		$(MAKE) $(SILENT) clean; \
		echo "[Configuring SDK with $@]"; \
		$(COPY_CMD) -af $(SDK_DIR)/build/config/$@ $(SDK_DIR)/$(SDK_LAST_CONFIG); \
		$(MAKE) -C $(SDK_DIR) $@; \
	elif [ $$(diff $@ $(SDK_DIR)/$(SDK_LAST_CONFIG) 2> /dev/null | wc -l) -ne "0" ]; then \
		echo "[Config file has changed, hence cleaning the SDK]"; \
		$(MAKE) $(SILENT) clean; \
		echo "[Configuring SDK with $@]"; \
		$(COPY_CMD) -af $@ $(SDK_DIR)/$(SDK_LAST_CONFIG); \
		$(MAKE) -C $(SDK_DIR) $(abspath $@); \
	elif [ -e $@ ] || [ -e $(SDK_DIR)/build/config/$@ ]; then \
		echo "[SDK is already configured using $@]"; \
	else \
		echo -e "[Error: Config file not found at given path - $@ !]\n[Please use valid path]\n" ; \
		exit 1; \
	fi;

DEFCONFIG ?=
# Build SDK
sdk: $(DEFCONFIG)
	@echo "[Building SDK]"
	${AT}mkdir -p $(SDK_BIN_PATH)
	${AT}$(MAKE) $(SILENT) -C $(SDK_DIR) all INSTALL_DIR=$(SDK_BIN_PATH) $(CYG_COMPAT)

# Clean SDK
sdk_clean:
	@echo "[Cleaning WMSDK]"
	${AT}$(MAKE) $(SILENT) -C $(SDK_DIR) INSTALL_DIR=$(SDK_BIN_PATH) clean

# Boot2 Build
#
# Build boot2 with default configuration setting.
#
boot2: preq sdk $(SECURE_CONF_FILE)
	${AT}rm -fr $(APP_BIN_PATH)/*
	@echo "[Building Boot2]"
	$(MAKE) $(SILENT) -C $(BOOT2_DIR) $(CYG_COMPAT)
	-${AT}cp $(BOOT2_DIR)/bin/boot2*.bin $(APP_BIN_PATH)

# Clean Boot2
boot2_clean:
	@echo "[Cleaning Boot2]"
	${AT}$(MAKE) $(SILENT) -C $(BOOT2_DIR) clean

# Help
help:
	@echo "1. Select WMSDK configuration"
	@echo "   $$ make <defconfig>"
	@echo "   where defconfig can be any of the following:"
	@echo "      mw300_defconfig - for 88MW30x based boards"
	@echo "      mc200_8801_defconfig - for 8801 based boards"
	@echo "      mc200_defconfig - for 878x based boards"
	@echo "      other deconfigs can be found at wmsdk/build/config"
	@echo "2. Compile SDK and sample applications"
	@echo "   $$ make [BOARD=<board_name>] [sdk | boot2 | apps]"
	@echo "   Supported boards can be found in wmsdk/src/boards/".
	@echo "   Default board is mw300_rd"
	@echo "         sdk   - compiles SDK"
	@echo "         boot2 - compiles boot2 for specified board"
	@echo "         apps  - compiles applications for specified board"
	@echo "   	<none> - Compiles everything (SDK, boot2, applications)"
	@echo "3. Compile an application"
	@echo "   $$ make [BOARD=<board_name>] APPS=<application name>"
	@echo "   where APPS is relative path of application with respect to,"
	@echo "   wmsdk_bundle-3.x.y/sample_apps e.g: wlan/wm_demo"
	@echo "4. Build XIP enabled applications for 88MW30x"
	@echo "   $$ make [BOARD=<board_name>] XIP=1"
	@echo "   Note: With XIP ramload will not be functional for AXF image/s"
	@echo "5. Build secure-boot enabled applications for 88MW30x"
	@echo "   $$ make [BOARD=<board_name>] SECURE_BOOT=<config_num|config_file>"
	@echo "   where config_num is between 1 to 3"
	@echo "   Refer to WMSDK/tools/src/secureboot/README for details"

.PHONY: tools_install tools_clean

# Tools Build
#
tools_install:
	@echo "[Building Tools]"
	$(MAKE) $(SILENT) -C $(TOOLS_DIR)
	@echo "[Installing Tools]"
	$(MAKE) $(SILENT) -C $(TOOLS_DIR) install
	$(MAKE) $(SILENT) -C $(TOOLS_DIR) SDK_PATH=$(abspath $(SDK_DIR)) install

tools_clean:
	@echo "[Cleaning Tools]"
	${AT}$(MAKE) $(SILENT) -C $(TOOLS_DIR) clean

params:
	@echo "SDK_DIR ?= $(SDK_DIR)" > $(CONFIG_CMD_FILE)
	@echo "BOOT2_DIR ?= $(BOOT2_DIR)" >> $(CONFIG_CMD_FILE)
	@echo "SAMPLE_APP_DIR ?= $(SAMPLE_APP_DIR)" >> $(CONFIG_CMD_FILE)
	@echo "REF_APPS_DIR ?= $(REF_APPS_DIR)" >> $(CONFIG_CMD_FILE)
	@echo "S_BOARD ?= $(BOARD)" >> $(CONFIG_CMD_FILE)
	@echo "export LAST_SECURE_BOOT := $(SECURE_BOOT)" >> $(CONFIG_CMD_FILE)

installpkgs:
	$(SDK_DIR)/build/utils/installpkgs.sh

-include Makefile.ref

FORCE:
