#
# You can build this by setting up the following "Make Target" in Eclipse:
#
#   kratos-BCM94390WCD2-FreeRTOS-LwIP-debug download run
#
# (Or 943362WCD4 if the WCD2 board breaks again.)
#

NAME := App_Kratos
HOME := C:/Users/dbane
KRATOS := $(HOME)/rep/kratos

$(NAME)_SOURCES := \
	src/wifi_drv/wifi_drv_loop.c \
	src/wifi_drv/wifi_drv_ser.c \
	src/wifi_drv/wifi_drv_io.c \
	src/wifi_drv/st_fw.c \
	src/settings_mgr/settings_mgr.c \
	src/settings_mgr/prov_fail_log.c \
	src/sw_mgr/ota_client.c \
	src/sw_mgr/crc.c \
	src/main.c \
	src/st_fw.c \
	src/utils/xxtea.c
$(NAME)_CFLAGS := \
	-Iapps/kratos/src/wifi_drv \
	-Iapps/kratos/include \
	-I$(KRATOS)/include \
	-IWICED/internal

$(NAME)_COMPONENTS := protocols/HTTP

GLOBAL_DEFINES := WICED_DISABLE_STDIO DBG_MSG KEY0=$(KEY0) KEY1=$(KEY1) KEY2=$(KEY2) KEY3=$(KEY3) NETWORK_CONFIG_APPLICATION_DEFINED

WIFI_CONFIG_DCT_H := wifi_config_dct.h
