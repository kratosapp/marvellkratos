/** @file
 * @brief Wifi driver.
 *
 * Implement the 
 * Interface with the audio processor. This code performs three tasks:
 * <OL>
 * <LI>Marshal commands and responses into some serialisation format, e.g. AT commands or RPC XDR @ref refs "[3]"</LI>
 * <LI>Provide a reliable byte stream transport, e.g. UART or SPI</LI>
 * <LI>Service the requests by connecting to the cloud service provider.
 * </OL>
 *
 * This module is @em not separate from the audio processor
 * mirror. The two pieces of code should be thought of as the same
 * module, they have an intimate knowledge of each other, e.g. the
 * UART marshalling format.
 *
 * @note
 * Only blocking reads seem to be supported. Read data is sent to the UART.
 *
 * @todo
 * This module has two responsibilities. Perhaps it should be split?
 */

#ifndef _WIFI_DRV_H_
#define _WIFI_DRV_H_

#include <stdint.h>
#include <stdbool.h>
#include <stddef.h>

/**
 * @brief Initialise the Wifi driver.
 *
 * Initialise that state which is required before any of the ISRs are
 * installed or OS task-switching is enabled.
 */
void    wifi_drv_init(void);

/**
 * @brief Initialise the UART port.
 *
 * Initialise the UART.
 */
void    wifi_drv_uart_init(void);

/**
 * @brief Run the main processing loop.
 *
 * Set the initial state, then make network requests
 * in response to incoming data on the UART.
 *
 * @note
 * This function never returns.
 * @param unused will always be NULL
 */
void    wifi_drv(void const *unused);

/**
 * @brief Turn a @c wifi_drv_cmd into a byte array.
 *
 * This routine hides the exact encoding scheme used.
 * @param[in] cmd command to encode
 * @param[in,out] buf location to place the encoded byte array
 * @param[in,out] len PRE: max len of @c buf; POST: usable bytes in @c buf
 * @param[out] resp_len for synchronous messages, length of response to wait for
 */
void    wifi_drv_marshal(struct wifi_drv_cmd cmd, unsigned char *buf,
    size_t * len, size_t * resp_len);

/**
 * @brief Turn a byte array into a @c wifi_drv_cmd.
 *
 * This routine hides the exact encoding scheme used.
 * @param[in] buf the encoded byte array
 * @param[in] len usable bytes in @c buf
 * @param[out] cmd decoded command
 */
void    wifi_drv_unmarshal(unsigned char *buf, size_t len,
    struct wifi_drv_resp *cmd);

/**
 * @brief Fire-and-forget transmit operation.
 *
 * This reliably transmits data to the cloud. It does @em not, however, wait for a reply.
 * @param[in] buf the data to send
 * @param[in] len the length of @c buf
 */
void    wifi_drv_send_async(unsigned char *buf, size_t len);

/**
 * @brief Transmit and wait for reply operation.
 *
 * The reply, which must be @c resp_len bytes long, will be notified as a
 * @c CONTROLLER_EVENT_RX event to the controller state machine.
 *
 * @note
 * If one of many responses is expected from the cloud, the protocol must
 * be designed such that all possible responses are padded to be the
 * same length as the longest one.
 * @param[in] buf the data to send
 * @param[in] len the length of @c buf
 * @param[in] resp_len how many bytes of a response should be waited for
 * @see controller_send_event
 */
void    wifi_drv_send_sync(unsigned char *buf, size_t len, size_t resp_len);

/**
 * @brief Non-blocking read of the transport.
 *
 * Our state machine would like to multiplex on @em either data
 * coming in from the Wifi coproc @em or some ISR enqueueing an event.
 * This can be implemented by arranging for the UART Rx interrupt to
 * itself submit
 * an event using @c controller_submit_event.
 *
 * Blocking reads can not be implemented on top of this, as state machine
 * event handlers must never block. If this becomes a hard requirement we
 * could introduce a helper thread, but non-blocking reads are
 * thought sufficient for this application.
 * @param[in] buf buffer in which to place the received data
 * @param[in] len maximum size of @c buf
 */
void    wifi_drv_recv(unsigned char *buf, size_t len);

#ifdef DBG_MSG

/**
 * @brief Write a simple string to debug output
 *
 * @param[in] msg Message to write
 */
void dbg_puts(const char *msg);

/**
 * @brief Print debug output with formatting
 *
 * @param[in] fmt printf-style format string
 */
void dbg_printf(const char *fmt, ...);

#endif

#endif
