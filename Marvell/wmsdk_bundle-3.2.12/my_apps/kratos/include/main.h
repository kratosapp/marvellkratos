#ifndef _MAIN_H_
#define _MAIN_H_

extern os_semaphore_t wlan_init_done, normal_connected, normal_user_disconnect, tls_certtest_done, tls_post_done, dbg_printf_sema;

#endif
