/** @file
 * @brief Friendly wrapper for the built-in WICED OTA feature
 */

#ifndef _SW_MGR_H_
#define _SW_MGR_H_

#include <stdint.h>

/**
 * @brief Initialise some internal state
 *
 * This must be called once after reset, before any other
 * @c sw_mgr functions.
 */
void sw_mgr_init(void);

/**
 * @brief Configure for local network
 *
 * This must be called after every time you connect to a new
 * network.
 * @return WICED_SUCCESS for success, anything else for failure
 */
int sw_mgr_config(void);

/**
 * @brief Attempt an upgrade of the app to a new version
 *
 * The actual URL of the image that is upgraded to is
 * https://<host>/<filename>.
 * where <host> is @c SW_UPD_SVR in src/sw_mgr/ota_client.c
 * @param[in] filename the filename part of the URL to upgrade to
 * @param[in] app_size length of the resource at @c filename
 * @return WICED_SUCCESS for success, anything else for failure
 */
int sw_mgr_ota_client(char *filename, uint32_t app_size);

// From here on downwards is to calculate CRC32 checksums

/** @brief Starting value for calculating the CRC */
#define CRC_INIT_VAL ((uint32_t)~(0U))

/**
 * @brief Calculate a CRC32
 *
 * @param data data to sum
 * @param data_size length of @c data
 * @return the CRC32
 */
uint32_t calc_crc(const uint8_t *data, uint32_t data_size);

/**
 * @brief Do part of a CRC32 calculation
 *
 * @param start checksum calculated up to now. This should be @c CRC_INIT_VAL for the first block.
 * @param data new data to add to CRC
 * @param data_size length of @c data
 * @return a partial checksum, which must be passed to @c calc_crc_finish after the last block
 * @see CRC_INIT_VAL
 * @see calc_crc_finish
 */
uint32_t calc_crc_partial(uint32_t start, const uint8_t *data, uint32_t data_size);

/**
 * @brief Finish calculation of a CRC32 checksum
 *
 * @param crc value returned from @c calc_crc_partial
 * @return the CRC32 checksum
 * @see calc_crc_partial
 */
static inline uint32_t
calc_crc_finish(uint32_t crc)
{
        return ~crc;
}

#endif
