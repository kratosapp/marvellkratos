/** @file
 * @brief Settings manager for the WICED.
 *
 * Manage persistent settings in the DCT.
 * This code executes in user
 * context.
 *
 * Settings are identifed by an enum (i.e. equivalent to int) key, and have
 * a byte array value (@em not ASCII, we can have bytes >127; and @em not
 * C string, we can have null bytes)
 *
 * @note
 * We keep the working copy in
 * RAM and use @c load and
 * @c save functions to synchronise the EEPROM copy with it. This may
 * be slightly faster if EEPROM access is expensive, but its primary purpose
 * is to allow transaction support for updates.
 *
 * @note
 * It probably makes sense to have a parallel @c settings_mgr
 * on the net copro, similar to @c wifi_drv. It's not worth going over
 * the UART every time you want to retrieve a setting.
 * The same values will probably be stored on both chips
 * then, and must be kept in sync. We arbitrarily choose the audio
 * proc to be the database master and override what is on the net copro.
 *
 * @todo
 * Do we need a way to zap the EEPROM back to factory defaults? May be
 * better done over JTAG ...
 *
 * @todo
 * This will eventually have to play nicely
 * with sw_mgr for access to the flash.
 *
 */

#ifndef _SETTINGS_MGR_H_
#define _SETTINGS_MGR_H_

#include <stdlib.h>
#include <stdbool.h>
#include <stdint.h>
#include "wifi_drv_common.h"

#define TOK_BUF_SZ (32)

#define PSM "psm."
#define PSM_SIGNATURE PSM "signature"
#define PSM_AUTH_TOKEN PSM "auth_token"
#define PSM_IS_PROVISIONED PSM "is_provisioned"
#define PSM_SENSOR_ID PSM "sensorId"
#define PSM_SENSOR_ID_LEN PSM "sensorId_len"
#define PSM_VERSION PSM "version"
#define PSM_BOOT_APP_IDX PSM "boot_app_idx"
#define PSM_SSID PSM "ssid"
#define PSM_SSID_LEN PSM "ssid_len"
#define PSM_PASSWORD PSM "password"
#define PSM_PASSWORD_LEN PSM "password_len"
#define PSM_MAC_ADDRESS PSM "mac_address"

#define PSM_FAIL PSM "fail_"
#define PSM_FAIL_LOG PSM_FAIL "log_"
#define PSM_FAIL_LOG_HD PSM_FAIL_LOG "hd"
#define PSM_FAIL_LOG_TL PSM_FAIL_LOG "tl"
#define PSM_FAIL_LOG_SUMMARY PSM_FAIL_LOG "summary_"
#define PSM_FAIL_LOG_DETAIL PSM_FAIL_LOG "detail_"
/**
 * @brief Different types of config data.
 *
 * We use a fixed set of enums instead of strings to tag config data to
 * avoid string functions. This list will be added to throughout development.
 *
 * @warning
 * Never re-arrange or delete the items in this list, even if you think it
 * will be
 * more aesthetically pleasing. Always add new items to the end. This is
 * because they may be stored as integers in flash.
 */
enum settings_mgr_tag {
	SETTINGS_MGR_IS_PROVISIONED,
		 /**< is device provisioned? */
	SETTINGS_MGR_SSID,
			/**< Wifi SSID to connect to */
	SETTINGS_MGR_PASSWORD,
			    /**< Wifi password */
	SETTINGS_MGR_SENSOR_ID,   /**< device "sensorId" */
	SETTINGS_MGR_AUTH_TOKEN,    /**< auth token from cloud for battery */
	SETTINGS_MGR_MAC_ADDRESS,   /**< mac address from Wiced */
	SETTINGS_MGR_BOOT_APP_IDX,  /**< application index to boot */
};

/**
 * @brief Get a setting from the RAM copy of the settings.
 *
 * @warning You must have loaded the settings from EEPROM to RAM using @c settings_mgr_load before calling this function
 * @param[in] tag the data to retrieve
 * @param[in,out] buf copy the data in here. It is the caller's responsibility to ensure the buffer is big enough.
 * @param[out] len the length of the retrieved data.
 * @return true if the value was got, false if an error occurred
 * @see settings_mgr_load
 */
bool    settings_mgr_get(enum settings_mgr_tag tag, unsigned char *buf,
    uint8_t * len);

/**
 * @brief Set a value in the RAM copy of the settings.
 *
 * This same function is used to create new values, or modify existing ones.
 * @warning You must have loaded the settings from EEPROM to RAM using @c settings_mgr_load before calling this function
 * @param[in] tag the setting to change
 * @param[in] buf value to set
 * @param[in] len the length of @c buf
 * @return true if the value was set, false if an error occurred
 * @see settings_mgr_save
 */
bool    settings_mgr_set(enum settings_mgr_tag tag, unsigned char *buf,
    uint8_t len);


/**
 * @brief Check if the device has been provisioned.
 */
bool is_provisioned(void);

/**
 * @brief Save the RAM copy of the settings to persistent EEPROM.
 *
 * This can be used as the "COMMIT" operation when we want to
 * implement transaction semantics.
 */
void    settings_mgr_save(void);

/**
 * @brief Initialise the RAM copy of the settings from EEPROM.
 *
 * This should be called as part of system initialisation.
 * It can be used as the "ROLLBACK" operation when we want to
 * implement transaction semantics.
 */
void    settings_mgr_load(void);

/**
 * @brief Retrieves the current firmware version
 *
 */
const char * getFirmwareVersion(void);

/**
 * @brief Set the WICED country code
 *
 * This must be done for regulatory reasons before the radio is turned on.
 * @param[in] country_code an element of @c wiced_country_code_t from @c WICED/WWD/include/wwd_constants.h
 * @return The previously-set country code
 */
uint32_t settings_mgr_set_country_code(uint32_t country_code);

/** @brief Deprovision the WICED chip */
void settings_mgr_deprov(void);

// From here on downwards is the WICED half of the provisioning failure log

/**
 * @brief Log a failure
 *
 * @param[in] summary coarse failure reason code
 * @param[in] detail optional error code from the WICED SDK call that failed
 */
void prov_fail_log(enum wifi_drv_conn_status summary, int detail);

/**
 * @brief Convert a log to ASCII format for transmission
 *
 * The output is something like "DATA_DEC,0,WIFI_JOIN,1006".
 * @param[in,out] s string buffer to put the result in
 * @param[in] st_failures log of failures from the ST. WICED entries are replaced with the real failure info.
 * @param[in] st_failure_len length of @c st_failures
 */
void prov_fail_sprintf(char *s, uint8_t st_failures[], uint8_t st_failures_len);

/** @brief Truncate the log after successfully sending it to the cloud */
void prov_fail_truncate(void);

/** @brief Initialise the log feature */
void prov_fail_init(void);

#endif
