// This file is written to work with the Keil Configuration Wizard ( http://www.keil.com/support/man/docs/uv4/uv4_ut_configwizard.htm ), not Doxygen.
// <<< Use Configuration Wizard in Context Menu >>>

#ifndef _CONFIG_H_
#define _CONFIG_H_

#define PARSE_APPLICATION_ID    "9HGLw4hqcjl6Xr9YMemaO0dFeNLHWx8BeXA8aJ4b"
#define PARSE_REST_API_KEY      "VgGZ1eqNXAM9RoCZJOambMkEXtokrWuHCncqSBi6"

// <h> Controller
//   <o> Time for a provisioned device to sleep after booting until first health check
//     <i> Subsequent sleeps are configurable by the cloud to smooth traffic. There are a few reasonable values: a) 1 day; b) a very low value like 60s if we want to do the health check ASAP after booting; c) wait, e.g. 20 mins, until we are sure that a software-upgraded image is good.
#define DFL_SLEEP_DURATION 600
//   <o> Threshold of sound to detect
//     <i> This will likely have to be determined empirically.
#define SOUND_THRESH 1000
// </h>

// <h> ADC
//   <o> Signal threshold
#define SIGNAL_THRESHOLD 256
// </h>

// <h> Data Decoder
//   <o> Maximum value of "Total Payload Length" in provisioning packet
#define TOTAL_PAYLOAD_MAX 512
//   <o> Maximum value of "Payload Length" in provisioning packet
#define PAYLOAD_MAX 128
//   <o> Maximum value of "Field Length" in provisioning packet
#define FIELD_MAX 32
// </h>

// <h>Wifi Driver
//   <o> Max length of SSID in provisioning packet
#define SSID_MAX 32
//   <o> Max length of password in provisioning packet
#define PASSWORD_MAX 64
//   <o> Max length of "sensorId" in provisioning packet
#define SENSOR_ID_MAX 16
//   <o> Max length of cloud DNS address in provisioning packet
#define CLOUD_SVR_DNS_MAX 32
#ifndef UNIT_TEST
//   <o> Sleep time after sending power down to Wi-Fi chip
#define WIFI_SLEEP_TIME  (2 * SECONDS )
#endif
// </h>

// <h>Software Manager
//   <o> Max length of filename on the update Web server
#define UPD_FILENAME_MAX 33
//   <o> Size of area reserved for the Loader at the start of ST flash
#define LOADER_FLASH_MAX 0x2000
//   <o> Size of area reserved for the Loader at the start of ST EEPROM
#define LOADER_EEPROM_MAX 0x20
// </h>

#define PROV_FAILURES_MAX 8

#endif

// <<< end of configuration section >>>
