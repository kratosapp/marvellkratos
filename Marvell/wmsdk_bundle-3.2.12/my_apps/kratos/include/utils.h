/** @file
 * @brief Small utilities that don't fit in a module
 */

#ifndef _UTILS_H_
#define _UTILS_H_

#include <stdint.h>

/**
 * @brief Do an unaligned read of a 32-bit number
 *
 * The implementation is currently for a little-endian machine, but is easily changed.
 * @param bits the start address of the number
 * @return its value
 */
static inline uint32_t
read_unaligned_uint32(unsigned char *bits)
{
        return bits[3] << 24 | bits[2] << 16 | bits[1] << 8 | bits[0];
}

/**
 * @brief Do an unaligned read of a 16-bit number
 *
 * The implementation is currently for a little-endian machine, but is easily changed.
 * @param bits the start address of the number
 * @return its value
 */
static inline uint16_t
read_unaligned_uint16(unsigned char *bits)
{
        return bits[1] << 8 | bits[0];
}

/**
 * @brief Do an unaligned write of a 32-bit number
 *
 * The implementation is currently for a little-endian machine, but is easily changed.
 * @param[in] val value of the number
 * @param[in,out] bits the start address to write
 */
static inline void
write_unaligned_uint32(uint32_t val, unsigned char *bits)
{
        bits[0] = val & 0x000000FF;
        bits[1] = (val & 0x0000FF00) >> 8;
        bits[2] = (val & 0x00FF0000) >> 16;
        bits[3] = (val & 0xFF000000) >> 24;
}

/**
 * @brief Do an unaligned write of a 16-bit number
 *
 * The implementation is currently for a little-endian machine, but is easily changed.
 * @param[in] val value of the number
 * @param[in,out] bits the start address to write
 */
static inline void
write_unaligned_uint16(uint16_t val, unsigned char *bits)
{
        bits[0] = val & 0x00FF;
        bits[1] = (val & 0xFF00) >> 8;
}

/**
 * @brief XXTEA encryption and decryption
 *
 * @note
 * I should probably use something like PKCS#7 padding, but instead
 * just fill the last word with zeroes.
 * @param v array of @c uint32_ts to encrypt or decrypt
 * @param n size of n in @em words. Negative for decryption, positive for encryption.
 * @param key 128-bit key
 * @see the "XXTEA" page on Wikipedia
 */
void btea(uint32_t *v, int n, uint32_t const key[4]);

void uint8s_to_uint32s(const uint8_t *in, int in_len, uint32_t *out);
void uint32s_to_uint8s(uint32_t *in, int in_len, uint8_t *out);

#endif
