/** @file
 * @brief Common code between ST & WICED
 *
 * This primarily defines the byte values of the command types;
 * and the information contained in command parameters.
 */

#ifndef _WIFI_DRV_COMMON_H_
#define _WIFI_DRV_COMMON_H_

#include <stdint.h>
#include "config.h"

/** @brief All auth tokens are the same length */
#define AUTH_TOKEN_LEN 16

/* Used internally - no need to document */
#define PAD_LEN        16

/** @brief All mac addresses are the same length */
#define MAC_ADDR_LEN   18

/** @brief Information retrieved from provisioning audio.
 *
 * We use arrays instead of pointers. We would have to allocate storage
 * anyway. The unused bytes are stripped by the serialisation code and
 * @em not transmitted over audio or the
 * UART.
 *
 * @note
 * Most of these members are defined as byte arrays with an associated length.
 * The native C string type wasn't used in case we need to store null bytes.
 * Obviously this doesn't apply to things that
 * @em must be strings, like DNS addresses.
 */
struct wifi_drv_provisioning_pkt
{
	uint32_t signature;	/**< Validity marker for settings stored to flash */
	uint8_t version;	/**< Struct version number, in case we need to migrate settings in flash */
	uint8_t is_provisioned;	/**< Whether or not we have successfully provisioned */
	unsigned char ssid[SSID_MAX+1];	/**< SSID for Wifi network */
	uint8_t ssid_len;	/**< SSID length */
	unsigned char password[PASSWORD_MAX+1]; /**< Password for Wifi network */
	uint8_t password_len;   /**< Password length */
	unsigned char sensorId[SENSOR_ID_MAX+1];	 /**< Unique identifier for device */
	uint8_t sensorId_len;	 /**< Length of sensorId */
	uint8_t auth_token[AUTH_TOKEN_LEN+1];    /**< auth token for battery to talk to cloud */
	char mac_addr[MAC_ADDR_LEN+1];           /**< mac address for sensor */
	uint8_t boot_app_idx;   /**< application index to boot */
        uint8_t hw_version;     /**< hardware version */
        uint32_t dev_signature[3];
        uint32_t country_code;
};

/**
 * @brief Payload for a software update request
 *
 * This is used for 2 essentially unrelated purposes:
 * 1. Passing the filename on the update Web server and *combined* image length to the WICED for HTTP fetching.
 *    `entry_point` is unused here.
 * 2. Passing the *ST* image length and entry point to the ST for writing to settings_mgr.
 *    `filename` is unused here.
 *
 * @tbd Split the two purposes?
 */
struct wifi_drv_update_pkt {
        char filename[UPD_FILENAME_MAX];         /**< Filename part of the URL for the image */
        uint32_t len;           /**< image length */
        uint32_t entry_point;   /**< start vector in ARM address space */
        uint32_t initial_sp;    /**< Initial MSP to run *entry_point */
};

/** @brief Health check information */
struct health_check_pkt
{
        uint32_t batt_lvl;      /**< battery level */
        uint8_t prov_failures[PROV_FAILURES_MAX]; /**< reasons for past provisioning failures */
        uint8_t prov_failures_len;   /**< usable bytes in @c prov_log */
	uint8_t hw_version;
	uint32_t dev_signature[3];
	uint32_t comp_wake_cnt;
};

/** Sleep response */
struct wifi_drv_sleep_pkt {
	uint32_t sleep_time;           /**< Sleep Time */
	uint32_t regulator_out;   /**< 0: 8V, 1: 6.9V */
};

/**
 * @brief Different command types @em to the Wifi coprocessor.
 *
 * @note
 * Start at 1 because of paranoia about starting a UART
 * transmission with a NUL byte.
 */
enum wifi_drv_cmd_t {
    WIFI_DRV_CMD_ALARM = 1,		/**< alarm notification */
    WIFI_DRV_CMD_SILENCE,       /**< alarm has stopped */
    WIFI_DRV_CMD_FETCH_FLASH,	/**< fetch a flash block */
    WIFI_DRV_CMD_PROVISION,		/**< set wifi net params */
    WIFI_DRV_CMD_SEND_HEALTH_CHECK, /**< send health check info */
    WIFI_DRV_CMD_CHECK_SW_CRC,  /**< check CRC-32 of ST software */
    WIFI_DRV_CMD_POLL,          /**< poll for snooze event */
    WIFI_DRV_CMD_POWERDOWN,     /**< power down Wi-Fi device */
    WIFI_DRV_CMD_DBG_MSGS_ON,   /**< switch on debug output from the WICED. Defaults to off. */
    WIFI_DRV_CMD_RADIO_ON,      /**< switch on the radio core on the WICED */
    WIFI_DRV_CMD_RADIO_ON_NO_JOIN, /**< switch on the radio core but don't try to join a network, useful before provisioning */
    WIFI_DRV_CMD_DEPROV,	/**< deprovision the WICED */
    WIFI_DRV_CMD_PROCEED,	/**< exit UART protocol handler */
    WIFI_DRV_CMD_RADIO_ON_NO_SCAN, /**< switch on the radio core, don't scan, join */
    WIFI_DRV_CMD_TEST_OK,	/**< auto battery test passed */
    WIFI_DRV_CMD_LAST,          /**< Not used, for sizing arrays */
};

/**
 * @brief Command @em to the Wifi coprocessor
 */
struct wifi_drv_cmd {
	enum wifi_drv_cmd_t type;	/**< command type */
	uintptr_t payload;	/**< optional arguments for command */
};


/**
 * @brief Response types @em from the Wifi coprocessor.
 *
 * @note
 * Start at 1 because of paranoia about starting a UART
 * transmission with a NUL byte.
 */
enum wifi_drv_resp_t {
    WIFI_DRV_RESP_PROVISIONING_SUCCESS = 1, /**< provisioned ok */
    WIFI_DRV_RESP_PROVISIONING_FAILURE,             /**< could not provision */
    WIFI_DRV_RESP_SLEEP,                    /**< sleep until next health check, or reboot after a S/W update */
    WIFI_DRV_RESP_SNOOZE,                   /**< snooze the alarm */
    WIFI_DRV_RESP_PROCEED,                  /**< chain to app */
    WIFI_DRV_RESP_UPDATE,                   /**< replace app flash */
    WIFI_DRV_RESP_FLASH_PAGE,               /**< a page of flash */
    WIFI_DRV_RESP_BOOT_SUCCESS,             /**< wifi part has booted successfully */
    WIFI_DRV_RESP_POWERDOWN_SUCCESS,        /**< wifi part has powered down successfully */
    WIFI_DRV_RESP_OK,                       /**< general success code */
    WIFI_DRV_RESP_DBG_MSG,                       /**< general-purpose debug puts() */
    WIFI_DRV_RESP_ERR,                      /**< general failure code */
    WIFI_DRV_RESP_DEPROV,		    /**< request from cloud to deprovision */
    WIFI_DRV_RESP_TEST,			    /**< auto battery test */
    WIFI_DRV_RESP_UPDATE_FAILED,	    /**< software update failed */
    WIFI_DRV_RESP_LAST, /**< Not used, for sizing arrays */
};

/**
 * @brief Response @em from the Wifi coprocessor
 */
struct wifi_drv_resp {
	enum wifi_drv_resp_t type; /**< response type */
	uint32_t payload;          /**< optional argument */
};

/** @brief Possible reasons for connection failure */
enum wifi_drv_conn_status {
        WIFI_DRV_CONN_OK = 1, /**< No problems */
        WIFI_DRV_CONN_ERROR_WIFI_JOIN, /**< wwd_wifi_join, i.e. Wifi association, failed. Probably bad SSID or password. */
        WIFI_DRV_CONN_ERROR_NETWORK_UP, /**< wiced_network_up, i.e. IPv4 init, failed. Probably failed to get DHCP lease. */
        WIFI_DRV_CONN_ERROR_GET, /**< HTTP GET failed. Probably no Internet connection from AP or failed to lookup DNS. */
        WIFI_DRV_CONN_ERROR_EMPTY_RESP, /**< HTTP request returned empty response */
        WIFI_DRV_CONN_ERROR_NO_AUTH_TOKEN, /**< no authToken in response */
        WIFI_DRV_CONN_ERROR_NO_SUCCESS, /**< no 'success' tag in response */
        WIFI_DRV_CONN_ERROR_NO_LINK_UP_NOTIF,  /**< no post to "link up" semaphore */
        WIFI_DRV_CONN_ERROR_WLAN_CONNECTIVITY_INIT, /**< wiced_connectivity_init failed */
        WIFI_DRV_CONN_ERROR_NO_SLEEP,               /**< no 'sleep' tag in response */
        WIFI_DRV_CONN_ERROR_BAD_LAST_REQ,           /**< supposed last request is unknown */
        WIFI_DRV_CONN_ERROR_SSID_NOT_FOUND,         /**< SSID not found in scan, then join failed */
	WIFI_DRV_CONN_ERROR_BAD_PASS,		    /**< invalid Wifi network password */
        WIFI_DRV_CONN_ERROR_TLS_INIT_ROOT_CA,       /**< wiced_tls_init_root_ca_certificates failed */
	WIFI_DRV_CONN_ERROR_REBOOT_REQUIRED,	    /**< unrecoverable error, reboot and try again under ST supervision */
	WIFI_DRV_CONN_ERROR_HOSTNAME_LOOKUP,	    /**< DNS resolution failed */
        WIFI_DRV_CONN_STATUS_LAST,  /**< not used, for sizing arrays */
};

#endif
