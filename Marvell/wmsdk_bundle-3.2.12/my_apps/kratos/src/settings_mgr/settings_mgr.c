/** @file
 * @brief Implementation of the WICED settings manager
 *
 * This maintains an independent copy of the Kratos settings.
 * This copy is a slave, and can be overwritten by commands
 * over the UART.
 *
 * Some of the information already has a home, the Wifi
 * connection settings. Other items are stored in the
 * general-purpose app section.
 *
 * @todo
 * Some code here is copied-and-pasted from the ST project:
 * <UL>
 * <LI>@c VALID_SIGNATURE and @c CURR_VER</LI>
 * <LI>@c init_settings</LI>
 * </UL>
 * Need to think about using a settings_mgr_common.[ch]. This would
 * bring to 3/4 the number of common files, maybe worth using a
 * submodule?
 */

#include <assert.h>
#include "settings_mgr.h"
#include "wifi_drv_common.h"
#include "wifi_drv.h"
#include "version.h"
#include <string.h>
#include "psm-v2.h"
#include "wlan.h"
#include "partition.h"

//#define PREPROVISIONED

/** @brief Tag that flash storage contains a valid struct */
#define VALID_SIGNATURE 0xCAFEBABE

#define STR_BUF_SZ (65)

/** @brief Version of flash storage format */
#define CURR_VER 1

#define VERSION "marvell"

/** @brief Version of the firmware
 *
 * Change this in @c version.h in the ST repo */
const char firmwareVersion[] = VERSION;

#if 0
/** @brief Pointer to RAM copy of wifi config data in flash */
static platform_dct_wifi_config_t *wifi_dct;
#endif

/** @brief Pointer to RAM copy of app-specific config data in flash */
//static struct wifi_drv_provisioning_pkt *app_dct;

/** @brief Our copy of the app-specific & wifi config data */
static struct wifi_drv_provisioning_pkt cache;

/** @brief Set config to factory defaults */
static void
init_settings(void)
{
  uint8_t mac_addr_bytes[6];

	cache.signature = VALID_SIGNATURE;
	cache.version = CURR_VER;
	cache.boot_app_idx = 0; //DCT_APP1_INDEX;
	memset(cache.mac_addr, 0, MAC_ADDR_LEN);

  wlan_get_mac_address(mac_addr_bytes);
  sprintf(cache.mac_addr, "%02X:%02X:%02X:%02X:%02X:%02X",
                mac_addr_bytes[0], mac_addr_bytes[1],
                mac_addr_bytes[2], mac_addr_bytes[3],
                mac_addr_bytes[4], mac_addr_bytes[5]);

#ifdef PREPROVISIONED
	cache.is_provisioned = 1;
	strcpy((char *) cache.ssid, "Apple00472");
	cache.ssid_len = strlen((char *) cache.ssid);
	strcpy((char *) cache.password, "AbbeyHouse");
	cache.password_len = strlen((char *) cache.password);
	strcpy((char *) cache.sensorId, "nYYZni2exL");
	cache.sensorId_len = strlen((char *) cache.sensorId);
	memcpy(cache.auth_token, "ohzf3uxwsh2kcnls", AUTH_TOKEN_LEN);

#else
	cache.is_provisioned = 0;
	cache.ssid_len = 0;
	cache.password_len = 0;
	cache.sensorId_len = 0;
	memset(cache.auth_token, 0, AUTH_TOKEN_LEN);
#endif
}

#if 0
void
settings_cpy(struct wifi_drv_provisioning_pkt *from,
    struct wifi_drv_provisioning_pkt *to)
{
	memset(to, 0, sizeof(struct wifi_drv_provisioning_pkt));

	memcpy(to->auth_token, from->auth_token, AUTH_TOKEN_LEN);

	to->is_provisioned = from->is_provisioned;

	/*
	 * mac_addr is copied from wifi dct 
	 */

	/*
	 * password and password_len are copied from wifi dct 
	 */

	to->sensorId_len = from->sensorId_len;
	memcpy(to->sensorId, from->sensorId, to->sensorId_len);

	to->signature = from->signature;
	/*
	 * ssid and ssid_len are copied from wifi dct 
	 */
	to->version = from->version;

	to->boot_app_idx = from->boot_app_idx;
}
#endif


void
settings_mgr_load(void)
{
  int ret;
  struct partition_entry *p;
  flash_desc_t fl;
  int an_int;

  memset(cache.auth_token, 0, AUTH_TOKEN_LEN+1);
  memset(cache.sensorId, 0, SENSOR_ID_MAX+1);
  memset(cache.ssid, 0, SSID_MAX+1);
  memset(cache.password, 0, PASSWORD_MAX+1);
  memset(cache.mac_addr, 0, MAC_ADDR_LEN+1);

  p = part_get_layout_by_id(FC_COMP_PSM, NULL);

  part_to_flash_desc(p, &fl);

  psm_hnd_t psm_hnd;

  ret = psm_module_init(&fl, &psm_hnd, NULL);

  ret = psm_get_variable_int(psm_hnd, PSM_SIGNATURE,&an_int);
  cache.signature = an_int;

  if(ret < 0) {
    //dbg_printf("No valid signature");
    psm_module_deinit(&psm_hnd);
    init_settings();
    settings_mgr_save();
  } else if (cache.signature != VALID_SIGNATURE) {
    //dbg_printf("Bad signature %08X %08X", VALID_SIGNATURE, cache.signature);
    psm_module_deinit(&psm_hnd);
    init_settings();
    settings_mgr_save();

  }else {
    //dbg_printf("Loading cache from persistent store");

    ret = psm_get_variable_str(psm_hnd, PSM_AUTH_TOKEN, cache.auth_token, sizeof(cache.auth_token));
    ret = psm_get_variable_int(psm_hnd, PSM_IS_PROVISIONED, &an_int);
    cache.is_provisioned = an_int;
    ret = psm_get_variable_str(psm_hnd, PSM_SENSOR_ID, cache.sensorId, sizeof(cache.sensorId));
    ret = psm_get_variable_int(psm_hnd, PSM_SENSOR_ID_LEN, &an_int);
    cache.sensorId_len = an_int;
    ret = psm_get_variable_int(psm_hnd, PSM_VERSION, &an_int);
    cache.version = an_int;
    ret = psm_get_variable_int(psm_hnd, PSM_BOOT_APP_IDX, &an_int);
    cache.boot_app_idx = an_int;
    ret = psm_get_variable_str(psm_hnd, PSM_SSID, cache.ssid, sizeof(cache.ssid));
    ret = psm_get_variable_int(psm_hnd, PSM_SSID_LEN, &an_int);
    cache.ssid_len = an_int;
    ret = psm_get_variable_str(psm_hnd, PSM_PASSWORD, cache.password, sizeof(cache.password));
    ret = psm_get_variable_int(psm_hnd, PSM_PASSWORD_LEN, &an_int);
    cache.password_len = an_int;
    ret = psm_get_variable_str(psm_hnd, PSM_MAC_ADDRESS, cache.mac_addr, sizeof(cache.mac_addr));

    psm_module_deinit(&psm_hnd);

#if 0
    dbg_printf("signature: %08X", cache.signature);
    dbg_printf("auth token: %s", cache.auth_token);
    dbg_printf("is provisioned: %d", cache.is_provisioned);
    dbg_printf("sensorId: %s, len %d", cache.sensorId, cache.sensorId_len);
    dbg_printf("version: %d", cache.version);
    dbg_printf("boot app idx: %d", cache.boot_app_idx);
    dbg_printf("ssid: %s, len: %d", cache.ssid, cache.ssid_len);
    dbg_printf("password: %s, len: %d", cache.password, cache.password_len);
    dbg_printf("mac addr: %s", cache.mac_addr);
#endif
  }


  //strcpy(cache.mac_addr, "11:22:33:44:55:66");
}

void
settings_mgr_save(void)
{
  struct partition_entry *p;
  flash_desc_t fl;

#if 0
  dbg_printf("Saving cache to persistent storage");
  dbg_printf("signature: %08X", cache.signature);
  dbg_printf("auth token: %s", cache.auth_token);
  dbg_printf("is provisioned: %d", cache.is_provisioned);
  dbg_printf("sensorId: %s, len %d", cache.sensorId, cache.sensorId_len);
  dbg_printf("version: %d", cache.version);
  dbg_printf("boot app idx: %d", cache.boot_app_idx);
  dbg_printf("ssid: %s, len: %d", cache.ssid, cache.ssid_len);
  dbg_printf("password: %s, len: %d", cache.password, cache.password_len);
  dbg_printf("mac addr: %s", cache.mac_addr);
#endif
  p = part_get_layout_by_id(FC_COMP_PSM, NULL);

  part_to_flash_desc(p, &fl);

  psm_hnd_t psm_hnd;
  psm_module_init(&fl, &psm_hnd, NULL);

  psm_set_variable_int(psm_hnd, PSM_SIGNATURE, cache.signature);

  psm_set_variable_str(psm_hnd, PSM_AUTH_TOKEN, cache.auth_token);

  psm_set_variable_int(psm_hnd, PSM_IS_PROVISIONED, cache.is_provisioned);

  psm_set_variable(psm_hnd, PSM_SENSOR_ID, cache.sensorId, strlen( (char *)cache.sensorId));

  psm_set_variable_int(psm_hnd, PSM_SENSOR_ID_LEN, cache.sensorId_len);

  psm_set_variable_int(psm_hnd, PSM_VERSION, cache.version);

  psm_set_variable_int(psm_hnd, PSM_BOOT_APP_IDX, cache.boot_app_idx);

  psm_set_variable_str(psm_hnd, PSM_SSID, cache.ssid);

  psm_set_variable_int(psm_hnd, PSM_SSID_LEN, cache.ssid_len);

  psm_set_variable_str(psm_hnd, PSM_PASSWORD, cache.password);

  psm_set_variable_int(psm_hnd, PSM_PASSWORD_LEN, cache.password_len);

  psm_set_variable_str(psm_hnd, PSM_MAC_ADDRESS, cache.mac_addr);

  psm_module_deinit(&psm_hnd);
}

const char *
getFirmwareVersion(void)
{
	return firmwareVersion;
}

bool
is_provisioned(void)
{
	unsigned char provisioned;

	unsigned char len = 1;

	if (!settings_mgr_get(SETTINGS_MGR_IS_PROVISIONED, &provisioned, &len)) {
		return false;
	}
	return provisioned;
}

bool
settings_mgr_set(enum settings_mgr_tag tag, unsigned char *buf, uint8_t len)
{
	switch (tag) {
	case SETTINGS_MGR_IS_PROVISIONED:
		cache.is_provisioned = *buf;
		return true;
	case SETTINGS_MGR_SSID:
		memset(cache.ssid, 0, SSID_MAX);
		memcpy(cache.ssid, buf, len);
		cache.ssid_len = len;
		return true;
	case SETTINGS_MGR_PASSWORD:
		memset(cache.password, 0, PASSWORD_MAX);
		memcpy(cache.password, buf, len);
		cache.password_len = len;
		return true;
	case SETTINGS_MGR_SENSOR_ID:
		memcpy(cache.sensorId, buf, len);
		cache.sensorId_len = len;
		return true;
	case SETTINGS_MGR_AUTH_TOKEN:
		memcpy(cache.auth_token, buf, len);
		// Ignore len
		return true;
	case SETTINGS_MGR_BOOT_APP_IDX:
		cache.boot_app_idx = *buf;
		return true;
	default:
		return false;
	}
	return false;
}

bool
settings_mgr_get(enum settings_mgr_tag tag, unsigned char *buf, uint8_t * len)
{
	switch (tag) {
	case SETTINGS_MGR_IS_PROVISIONED:
		*buf = cache.is_provisioned;
		*len = 1;
		return true;
	case SETTINGS_MGR_SSID:
		memset(buf, 0, SSID_MAX);
		memcpy(buf, cache.ssid, cache.ssid_len);
		*len = cache.ssid_len;
		return true;
	case SETTINGS_MGR_PASSWORD:
		memset(buf, 0, PASSWORD_MAX);
		memcpy(buf, cache.password, cache.password_len);
		*len = cache.password_len;
		return true;
	case SETTINGS_MGR_SENSOR_ID:
		memcpy(buf, cache.sensorId, cache.sensorId_len);
		*len = cache.sensorId_len;
		return true;
	case SETTINGS_MGR_AUTH_TOKEN:
		memcpy(buf, cache.auth_token, AUTH_TOKEN_LEN);
		*len = AUTH_TOKEN_LEN;
		return true;
	case SETTINGS_MGR_MAC_ADDRESS:
		memcpy(buf, cache.mac_addr, MAC_ADDR_LEN);
		*len = MAC_ADDR_LEN;
		return true;
	case SETTINGS_MGR_BOOT_APP_IDX:
		*buf = cache.boot_app_idx;
		*len = 1;
		return true;
	default:
		return false;
	}
	return false;
}

uint32_t
settings_mgr_set_country_code(uint32_t country_code)
{
  return wlan_set_country(country_code);
}

void
settings_mgr_deprov(void)
{
	// Don't mess with the "current app" pointer.
	// See KRAT-583.
	uint8_t current_app = cache.boot_app_idx;
	init_settings();
	cache.boot_app_idx = current_app;
	settings_mgr_save();
}
