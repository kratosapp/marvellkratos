/** @file
 * @brief Dumb "provisioning failure" log, WICED half
 */

#include <assert.h>
#include "config.h"
#include "settings_mgr.h"
#include "wifi_drv.h"
#include <stdio.h>
#include <string.h>
#include "psm-v2.h"
#include "partition.h"

/**
 * @brief Value of placeholder for WICED failures on ST
 *
 * @warning must agree with settings_mgr.h in kratos.git
 */
#define PROV_FAIL_WICED 0

/** @brief Data type for the WICED failure log */
struct prov_fail_log {
        uint8_t hd, tl;	/**< Queue pointers */
        struct {
                uint8_t summary;	/**< Our failure code */
                int detail;	/**< Optional WICED SDK error return code */
        } failures[PROV_FAILURES_MAX];
};

/**
 * @brief Storage for the failure log
 *
 * @note
 * @c log would be a gcc built-in
 */
static struct prov_fail_log my_log;

/** @brief Pointer to RAM copy of app-specific config data in flash */
//static struct prov_fail_log *app_dct;

/** @brief ASCII representations of @c wifi_drv_conn_status */
static const char *prov_failure_names[] = {
        "",
        "OK",
        "WIFI_JOIN",
        "NETWORK_UP",
        "GET",
        "EMPTY_RESP",
        "NO_AUTH_TOKEN",
        "NO_SUCCESS",
        "NO_LINK_UP_NOTIF",
        "WLAN_CONNECTIVITY_INIT",
        "NO_SLEEP",
        "BAD_LAST_REQ",
        "SSID_NOT_FOUND",
        "BAD_PASS",
        "TLS_INIT_ROOT_CA",
        "REBOOT_REQUIRED",
        "HOSTNAME_LOOKUP",
};

/** @brief Number of different failure types */
#define NUM_PROV_FAILURE_NAMES (WIFI_DRV_CONN_STATUS_LAST)

/** @brief Lowest ST-only failure code */
#define ST_FAILURE_OFFSET 0xF8

/** @brief ASCII strings for ST-only failures */
static const char *st_failure_names[] = {
        "PROVISION_TIMEOUT,0",
        "WICED_FAULT,0",
	"WICED_HEALTH_CHECK_TIMEOUT,0",
	"WICED_ALARM_TIMEOUT,0",
	"WICED_REBOOTED,0",
	"WICED_RADIO_ON_TIMEOUT,0",
	"WICED_POWERON_TIMEOUT,0",
	"DATA_DEC,0",
};

/** @brief Number of elements in @c st_failure names */
#define NUM_ST_FAILURE_NAMES 8

/** @brief This is used for out-of-range values */
const char *unknown = "UNKNOWN";


void
prov_populate_log_from_flash(void)
{
   int i = 0;
   int an_int;
   char tokbuf[TOK_BUF_SZ] = { 0 };

   struct partition_entry *p;
   flash_desc_t fl;

   p = part_get_layout_by_id(FC_COMP_PSM, NULL);

   part_to_flash_desc(p, &fl);

   psm_hnd_t psm_hnd;

   psm_module_init(&fl, &psm_hnd, NULL);

   psm_get_variable_int(psm_hnd, PSM_FAIL_LOG_HD, &an_int);
   my_log.hd = an_int;
   psm_get_variable_int(psm_hnd, PSM_FAIL_LOG_TL, &an_int);
   my_log.tl = an_int;

   for(i = 0; i < PROV_FAILURES_MAX; i++) {
     snprintf(tokbuf, TOK_BUF_SZ, "%s%d", PSM_FAIL_LOG_SUMMARY, i);
     psm_get_variable_int(psm_hnd, tokbuf, &an_int);
     my_log.failures[i].summary = an_int;
     snprintf(tokbuf, TOK_BUF_SZ, "%s%d", PSM_FAIL_LOG_DETAIL, i);
     psm_get_variable_int(psm_hnd, tokbuf, &an_int);
     my_log.failures[i].detail = an_int;
   }


   psm_module_deinit(&psm_hnd);
}

void
prov_fail_init(void)
{
  prov_populate_log_from_flash();
  if(my_log.hd >= PROV_FAILURES_MAX || my_log.tl >= PROV_FAILURES_MAX) {
    prov_fail_truncate();
  }
}


/** @brief Persist the current log contents */
static void
prov_fail_sync(void)
{
   int i = 0;
   char tokbuf[TOK_BUF_SZ] = { 0 };

   struct partition_entry *p;
   flash_desc_t fl;

   p = part_get_layout_by_id(FC_COMP_PSM, NULL);

   part_to_flash_desc(p, &fl);

   psm_hnd_t psm_hnd;

   psm_module_init(&fl, &psm_hnd, NULL);

   psm_set_variable_int(psm_hnd, PSM_FAIL_LOG_HD, my_log.hd);
   psm_set_variable_int(psm_hnd, PSM_FAIL_LOG_TL, my_log.tl);

   for(i = 0; i < PROV_FAILURES_MAX; i++) {
     snprintf(tokbuf, TOK_BUF_SZ, "%s%d", PSM_FAIL_LOG_SUMMARY, i);
     psm_set_variable_int(psm_hnd, tokbuf, my_log.failures[i].summary);
     snprintf(tokbuf, TOK_BUF_SZ, "%s%d", PSM_FAIL_LOG_DETAIL, i);
     psm_set_variable_int(psm_hnd, tokbuf, my_log.failures[i].detail);
   }

   psm_module_deinit(&psm_hnd);
}

void
prov_fail_log(enum wifi_drv_conn_status summary, int detail)
{
        my_log.failures[my_log.tl].summary = summary;
        my_log.failures[my_log.tl].detail = detail;
        my_log.tl++;
        my_log.tl %= PROV_FAILURES_MAX;
        if (my_log.tl == my_log.hd) {
                // We have wrapped around, throw away the oldest entry
                my_log.hd = my_log.tl + 1;
                my_log.hd %= PROV_FAILURES_MAX;
        }
        prov_fail_sync();
}

/**
 * @brief Convert an instance of @c wifi_drv_conn_status to an ASCII string
 *
 * @param[in] failure failure code
 * @return a pointer to a const string representation. This may not be mutable.
 */
static const char *
get_failure_name(uint8_t failure)
{
        if (failure < NUM_PROV_FAILURE_NAMES) {
                return prov_failure_names[failure];
        } else {
                return unknown;
        }
}

/**
 * @brief Get the ASCII version of an ST failure code
 *
 * @param[in] failure failure code
 * @return a pointer to a const string representation. This may not be mutable.
 */
static const char *
get_st_failure_name(uint8_t failure)
{
	failure -= ST_FAILURE_OFFSET;
	if (failure < NUM_ST_FAILURE_NAMES) {
		return st_failure_names[failure];
	} else {
		return unknown;
	}
}

void
prov_fail_sprintf(char *s, uint8_t st_failures[], uint8_t st_failures_len)
{
        int i;
        int j = my_log.hd; /* Wiced log will be missing DATA_DEC indices.
                         * Also is still a queue, not linearised. */
        char *p = s;
        
        for (i = 0; i < st_failures_len; i++) {
                if (st_failures[i] == PROV_FAIL_WICED) {
                        // Fill in the blank
                        sprintf(p, "%s,%d", get_failure_name(my_log.failures[j].summary), my_log.failures[j].detail);
                        j++;
                        j %= PROV_FAILURES_MAX;
                } else {
			strcpy(p, get_st_failure_name(st_failures[i]));
                        // Don't increment j
                }
                if (i < st_failures_len - 1) {
                        strcat(p, ",");
                }
                p += strlen(p);
        }
}

void
prov_fail_truncate(void)
{
        if (my_log.hd != 0 || my_log.tl != 0) {
                my_log.hd = my_log.tl = 0;
                prov_fail_sync();
        }
}
