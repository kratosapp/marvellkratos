#include <stdio.h>

void
wifi_drv_send_net(unsigned char *buf, size_t len)
{
	printf("%s: len %d\n", __func__, len);
}

void
wifi_drv_recv_net(unsigned char *buf, size_t len)
{
	strncpy(buf, len, "{'status':0}");
}

void
wifi_drv_send_audio_link(unsigned char *buf, size_t len)
{
	printf("%s: len %d\n", __func__, len);
}

static inline int
min(int x, int y)
{
	return (x < y) ? x : y;
}

void
wifi_drv_recv_audio_link(unsigned char *buf, size_t len)
{
	static unsigned char provision = {
		0x06,
		0x04, 'S', 'S', 'I', 'D',
		0x03, 'p', 'w', 'd',
		0x03, 'U', 'R', 'I',
		0x05, 'C', 'l', 'o', 'u', 'd',
	};

memcpy(buf, provision, min(len, sizeof(provision));}
