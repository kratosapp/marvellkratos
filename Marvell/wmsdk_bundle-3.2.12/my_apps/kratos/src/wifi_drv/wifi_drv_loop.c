/** @file
 * @brief Implementation of the WICED side of the audio-net processor link.
 *
 * This file is simpler than the FSMs in the audio processor code,
 * it's more like a main processing loop. This is because WICED I/O is
 * blocking, and there is no need for multiplexing across different input
 * or event streams.
 *
 * @todo how does power management interact with this?
 *
 * @todo this file has expanded over time, it now does a lot of "controller"'s
 *       task on the audio processor. Split into a separate file?
 *
 * @todo centralise the wiced_network_{up,down} calls in _io.c?
 */

#include <stdio.h>
#include <stdbool.h>
#include <assert.h>
#include <string.h>
#include <stdint.h>
#include "wifi_drv_common.h"
#include "wifi_drv_p.h"
#include "settings_mgr.h"
#include "sw_mgr.h"
#include "utils.h"
#include "wifi_drv.h"
#include "wm_os.h"
#include "app_framework.h"
#include "main.h"

#define BUFSIZ 1024

// Error codes:
#define INVALID_KEY -2

#ifdef DBG_MSG
#include <stdarg.h>

#define PAD_SIZE 133
#endif

#define CMD_LEN 5

#define MAX_CONNECTIVITY_INIT_RETRIES 8

/** @brief Size of the auxiliary buffer for Tx'ing debug messages */
#define DBG_TX_BUFSIZ 256

#ifdef DBG_MSG
static const char *dbg_get_cmd_name(uint32_t dbg_cmd);

static const char *dbg_get_resp_name(uint32_t dbg_resp);
#endif

/**
 * @brief Rx buf
 *
 * This is used for both Rx from the network and from the UART
 */
static unsigned char rx_buf[BUFSIZ];

/**
 * @brief Tx buf
 *
 * This is used for both Tx to the network and to the UART
 */
static unsigned char tx_buf[BUFSIZ];

/** @brief Buf used to Tx debug msgs to ST chip */
static unsigned char dbg_tx_buf[DBG_TX_BUFSIZ];

/**
 * @brief Are we going to be rebooted by the ST next, e.g. after a software update.
 *
 * It's not great encapsulation, but this can be set from wifi_drv_marshal_audio_link too.
 */
bool    reboot_required = false;

/** @brief Number of bad responses received */
unsigned wifi_drv_bad_resps = 0;

static bool dbg_msgs_on = false;

#ifdef DBG_MSG
const char *cmd_names[] = {
	"CMD_NULL",
	"CMD_ALARM",
	"CMD_SILENCE",
	"CMD_FETCH_FLASH",
	"CMD_PROVISION",
	"CMD_SEND_HEALTH_CHECK",
	"CMD_CHECK_SW_CRC",
	"CMD_POLL",
	"CMD_POWERDOWN",
	"CMD_DBG_MSGS_ON",
	"CMD_RADIO_ON",
	"CMD_RADIO_ON_NO_JOIN",
        "CMD_DEPROV",
	"CMD_PROCEED",
	"CMD_TEST_OK",
};

#define DBG_NUM_CMD_NAMES WIFI_DRV_CMD_LAST

const char *resp_names[] = {
	"RESP_NULL",
	"RESP_PROV_SUCC",
	"RESP_PROV_FAIL",
	"RESP_SLEEP",
	"RESP_SNOOZE",
	"RESP_PROCEED",
	"RESP_UPDATE",
	"RESP_FLASH_PAGE",
	"RESP_BOOT_SUCC",
	"RESP_PWRDOWN_SUCC",
	"RESP_OK",
	"RESP_DBG_MSG",
	"WIFI_DRV_RESP_ERR",
        "RESP_DEPROV",
	"RESP_TEST",
	"RESP_UPDATE_FAIL",
};

#define DBG_NUM_RESP_NAMES WIFI_DRV_RESP_LAST
#endif

os_timer_t resend_boot_success;

uint8_t *decrypted_st_image;

/** @brief Does a command require some local processing on the WICED
 * @note
 * Even when only some of the message handling is on the
 * WICED and some is on the cloud, this function should return @c true.
 * @param cmd the command
 * @return true if local processing is required, false otherwise
 *
 * @todo
 * Handle short-circuit, e.g. CHECK_CRC which needs no cloud comms.
 */
static  bool
is_local_cmd(struct wifi_drv_cmd *cmd)
{
	return cmd->type == WIFI_DRV_CMD_PROVISION ||
	    cmd->type == WIFI_DRV_CMD_CHECK_SW_CRC ||
	    cmd->type == WIFI_DRV_CMD_FETCH_FLASH ||
	    cmd->type == WIFI_DRV_CMD_POWERDOWN ||
	    cmd->type == WIFI_DRV_CMD_DBG_MSGS_ON ||
	    cmd->type == WIFI_DRV_CMD_RADIO_ON ||
	    cmd->type == WIFI_DRV_CMD_RADIO_ON_NO_JOIN ||
            cmd->type == WIFI_DRV_CMD_DEPROV;
}

int ipconfig_result;

uint32_t st_image_crc;

static struct wlan_network net;

static bool ssid_found = false;

#ifdef WEP
void
setup_wep_keys(unsigned char *password, uint8_t password_len, uint8_t **psk, uint8_t *pkl)
{
	//uint8_t* security_key;
	//uint8_t key_length;
	static uint8_t wep_key_buffer[64];
	memset(wep_key_buffer, 0, 64);

        int a;
        wiced_wep_key_t* temp_wep_key = (wiced_wep_key_t*)wep_key_buffer;
        char temp_string[3];
        temp_string[2] = 0;
        key_length = password_len/2;

        /* Setup WEP key 0 */
        temp_wep_key[0].index = 0;
        temp_wep_key[0].length = key_length;
        for (a = 0; a < temp_wep_key[0].length; ++a)
        {
        	uint32_t tmp_val;
		memcpy(temp_string, &password[a*2], 2);
		string_to_unsigned( temp_string, 2, &tmp_val, 1 );
		temp_wep_key[0].data[a] = (uint8_t) tmp_val;
        }

        /* Setup WEP keys 1 to 3 */
        memcpy(wep_key_buffer + 1*(2 + key_length), temp_wep_key, (2 + key_length));
        memcpy(wep_key_buffer + 2*(2 + key_length), temp_wep_key, (2 + key_length));
        memcpy(wep_key_buffer + 3*(2 + key_length), temp_wep_key, (2 + key_length));
        wep_key_buffer[1*(2 + key_length)] = 1;
        wep_key_buffer[2*(2 + key_length)] = 2;
        wep_key_buffer[3*(2 + key_length)] = 3;

        security_key = wep_key_buffer;
        key_length = 4*(2 + key_length);

	*psk = security_key;
	*pkl = key_length;
}
#endif

//extern uint16_t wifi_join_dbg_info;
static int
join(unsigned char *password, uint8_t password_len)
{
#ifdef DBG_MSG
	//dbg_printf("%s: password_len %d password %s", __func__, password_len, password);
#endif
#ifdef DBG_MSG
    // TODO dbg_printf("%s: ssid_len %d ssid %s", __func__, ssid.length, ssid.value);
#endif
	//uint8_t *security_key;
	//uint8_t key_length;
#ifdef WEP
	if (ap_info.security == WICED_SECURITY_WEP_PSK) {
		char hex_pwd[27];
		if (password_len == 5) {
			snprintf(hex_pwd, 11, "%02X%02X%02X%02X%02X", password[0], password[1], password[2], password[3], password[4]);
			password = (unsigned char *)hex_pwd;
			password_len = 10;
		} else if (password_len == 13) {
			snprintf(hex_pwd, 27, "%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X%02X", password[0], password[1], password[2], password[3], password[4], password[5], password[6], password[7], password[8], password[9], password[10], password[11], password[12]);
			password = (unsigned char *)hex_pwd;
			password_len = 26;
		}
		setup_wep_keys(password, password_len, &security_key, &key_length);
	} else
#endif
	{
		//security_key = password;
		//key_length = password_len;
	}
	int result;
	result = app_sta_start_by_network(&net);
	if (WM_SUCCESS != result)
	{
		#ifdef DBG_MSG
		//dbg_printf("%s: join 1 returned %d wifi_join err code: %d", __func__, result, wifi_join_dbg_info);
    dbg_printf("%s: app_sta_start_by_network returned err code: %d", __func__, result);
		#endif
		return result;
	}
  dbg_printf("app_sta_start_by_network succeeded");
	os_semaphore_get(&normal_connected, OS_WAIT_FOREVER);

  //dbg_printf("got normal connected semaphore");
	// TODO: wait for error handling state transitions too (can we get one from a set of semaphores?),
	//       return meaningful error code to caller
	//       WLAN_REASON_AUTH_FAILED or WLAN_REASON_NW_NOT_FOUND or WLAN_REASON_ADDR_FAILED events
#ifdef DBG_MSG
	//dbg_printf("%s: join 1 returned %d wifi_join err code: %d", __func__, result, wifi_join_dbg_info);
#endif
#ifdef WEP
	if (result != WICED_SUCCESS && ap_info.security == WICED_SECURITY_WEP_PSK) {
		ap_info.security = WICED_SECURITY_WEP_SHARED;
		for (join_retries = 0; join_retries < WICED_JOIN_RETRY_ATTEMPTS; join_retries++){
			result = wwd_wifi_join(&ssid, ap_info.security, security_key, key_length, NULL);
			if (WICED_SUCCESS == result)
			{
				#ifdef DBG_MSG
				dbg_printf("%s: join 2 returned %d", __func__, result);
				#endif
				return result;
			}
		}
#ifdef DBG_MSG
		dbg_printf("%s: join 2 returned %d", __func__, result);
#endif
	}
#endif	// WEP
	return result;
}

static bool have_already_joined = false;

static enum wifi_drv_conn_status
bring_netif_up(bool is_provisioning, bool is_alarm)
{
	// The Marvell API needs a bit of data interworking.
	uint8_t ssid_len;

	settings_mgr_get(SETTINGS_MGR_SSID, (unsigned char *)(net.ssid), &ssid_len);

	unsigned char password[PASSWORD_MAX];

	uint8_t password_len;

	settings_mgr_get(SETTINGS_MGR_PASSWORD, (unsigned char *)(net.security.psk), &password_len);
  memset(password, 0, PASSWORD_MAX);
  memcpy(password, net.security.psk, net.security.psk_len);
	net.security.psk_len = password_len;

  net.role = WLAN_BSS_ROLE_STA;
  strncpy(net.name, "Station", sizeof("Station"));
  memset(net.bssid, 0x00, 6);
  net.channel = 0; // any/
  net.security.type = WLAN_SECURITY_WPA2;
  net.ip.ipv4.addr_type = ADDR_TYPE_DHCP;
  net.type = WLAN_BSS_ROLE_STA;

	// For provisioning, try join
	// Retry join three times like what is done in the Broadcom code
	//if (is_provisioning)
  if(!have_already_joined)
	{
		int result = 0;

    dbg_printf("Joining %s (password %s)", net.ssid, net.security.psk);

    result = join(password, password_len);
		if (result != WM_SUCCESS) {
			if (ssid_found) {
				if (result == INVALID_KEY) {
					prov_fail_log
						(WIFI_DRV_CONN_ERROR_BAD_PASS,
						result);
					return WIFI_DRV_CONN_ERROR_BAD_PASS;
				}
				else {
					prov_fail_log
						(WIFI_DRV_CONN_ERROR_WIFI_JOIN,
						result);
				}
			}
			else {
				prov_fail_log
					(WIFI_DRV_CONN_ERROR_SSID_NOT_FOUND,
					result);
				return WIFI_DRV_CONN_ERROR_SSID_NOT_FOUND;
			}
			return WIFI_DRV_CONN_ERROR_WIFI_JOIN;
		} else {
      have_already_joined = true;
    }
	}


	// I think the Marvell SDK brings both Wifi and IP layers up

	return wifi_drv_config();	// Look up the cloud server again
					// Continue to try provisioning
					// request to parse.com
}

/** @brief Local command processing
 *
 * If this fails, it aborts handling the current command.
 * @param cmd the command
 * @return true if we should continue on to network I/O, false if we should send an early response and wait for next command
 */
static  bool
process_cmd(struct wifi_drv_cmd *cmd, struct wifi_drv_resp *early_resp)
{
	struct wifi_drv_provisioning_pkt *prov_pkt =
	    (struct wifi_drv_provisioning_pkt *) cmd->payload;
	//int wcd_result = WM_FAIL;

	//int wwd_result;

	enum wifi_drv_conn_status conn_result;

  settings_mgr_load();

	switch (cmd->type) {
	case WIFI_DRV_CMD_PROVISION:
		settings_mgr_set(SETTINGS_MGR_SSID, prov_pkt->ssid,
		    prov_pkt->ssid_len);
		settings_mgr_set(SETTINGS_MGR_PASSWORD, prov_pkt->password,
		    prov_pkt->password_len);
		settings_mgr_set(SETTINGS_MGR_SENSOR_ID, prov_pkt->sensorId,
		    prov_pkt->sensorId_len);

		// Now we need to check two things:
		// 1. we can connect to the new Wifi network
		// 2. we can log into the cloud server
		// 
		// If either of these fail, ROLLBACK.
		//wcd_result = app_sta_stop();
    // can't seem to stop a "stopped STA"
    // Nor is there an obvious mechanism to tell if it is stopped or not
    //if(wcd_result == WM_SUCCESS) {
		//  os_semaphore_get(&normal_user_disconnect, OS_WAIT_FOREVER);
    //} 

		// wiced_join_ap() uses the saved Wifi DCT for credentials.
		// We don't
		// want to save the new settings quite yet, until we've
		// verified
		// that they work.

                uint32_t old_country = settings_mgr_set_country_code(prov_pkt->country_code);
		if ((conn_result = bring_netif_up(true, false)) != WIFI_DRV_CONN_OK) {
			// ROLLBACK
			settings_mgr_load();
                        settings_mgr_set_country_code(old_country);
			early_resp->type = WIFI_DRV_RESP_PROVISIONING_FAILURE;
			early_resp->payload = conn_result;
			// Have already logged the failure reason
			return false;
		}
		// No need to set early_resp because we're going to connect
		// to the cloud before sending a normal resp
		return true;
		break;
#ifdef OTA
	case WIFI_DRV_CMD_CHECK_SW_CRC:
        {
                static struct wifi_drv_update_pkt upd;
                
		st_fw_open();
		if (cmd->payload == st_fw_crc) {
                        st_fw_close();
                        
			early_resp->type = WIFI_DRV_RESP_PROCEED;

                        // Can put any payload we like in here for debugging purposes
                        //early_resp->payload = XXX;

			reboot_required = true;
		} else {
			early_resp->type = WIFI_DRV_RESP_UPDATE;
			// upd.crc32 is ignored by the ST
			upd.len = ST_FW_SIZE;
			upd.entry_point = ST_ENTRY_POINT;
			upd.initial_sp = ST_INITIAL_SP;
			early_resp->payload = (uintptr_t) & upd;
		}
		return false;		/* No network comms needed */
        }
        break;
	case WIFI_DRV_CMD_FETCH_FLASH:
		early_resp->type = WIFI_DRV_RESP_FLASH_PAGE;
                
		early_resp->payload = cmd->payload;
                                /* See wifi_drv_marshal_audio_link */
                
		return false;		/* No network comms needed */
		break;
#endif
	case WIFI_DRV_CMD_POWERDOWN:
		app_sta_stop();
		os_semaphore_get(&normal_user_disconnect, OS_WAIT_FOREVER);
		early_resp->type = WIFI_DRV_RESP_POWERDOWN_SUCCESS;
		early_resp->payload = 0;
		reboot_required = true;
		return false;
		break;
	case WIFI_DRV_CMD_DBG_MSGS_ON:
		dbg_msgs_on = true;
#ifdef DBG_MSG
		dbg_printf
		    ("Roost Smart Battery App Wifi coprocessor debug enabled. Version: %s", getFirmwareVersion());
#endif
		early_resp->type = WIFI_DRV_RESP_OK;
		return false;
		break;
#ifdef POWER_ON_RADIO_SEPARATELY
	case WIFI_DRV_CMD_RADIO_ON:
	case WIFI_DRV_CMD_RADIO_ON_NO_JOIN:
	case WIFI_DRV_CMD_RADIO_ON_NO_SCAN:
		{
			// It looks like after a call to
			// wiced_wlan_connectivity_init has failed,
			// the only safe thing to do is power down the
			// WICED. I used to continue
			// on and try bring up the network, but that caused
			// the apps core to lock
			// up. I tried calling connectivity_init multiple
			// times, but the second
			// call caused the apps core to lock up.
			// 
			// We'll just have to hope that zeroing stack and
			// heap will avoid the
			// sticky failure we used to see, and any failures
			// of connectivity_init
			// are one-off.
			os_thread_sleep(os_msec_to_ticks(2000));
			wcd_result = wiced_wlan_connectivity_init();
			if (wcd_result != WM_SUCCESS) {
				early_resp->type = WIFI_DRV_RESP_ERR;
				early_resp->payload =
				    WIFI_DRV_CONN_ERROR_WLAN_CONNECTIVITY_INIT;
#ifdef DBG_MSG
				dbg_printf("wiced_wlan_connectivity_init returned %d", wcd_result);
#endif
				prov_fail_log(WIFI_DRV_CONN_ERROR_WLAN_CONNECTIVITY_INIT, wcd_result);
				return false;
			}

			wiced_wifi_down();

			if ((wwd_result =
				wwd_wifi_set_11n_support(WWD_STA_INTERFACE,
				    WICED_TRUE)) != WWD_SUCCESS) {
#ifdef DBG_MSG
				dbg_printf(
				    "wwd_wifi_set_11n_support returned %d",
				    wwd_result);
#endif
			}

			// See Broadcom product case #921758
			if ((wwd_result =
				wwd_wifi_select_antenna(WICED_ANTENNA_AUTO)) !=
			    WWD_SUCCESS) {
#ifdef DBG_MSG
				dbg_printf(
				    "wwd_wifi_select_antenna returned %d",
				    wwd_result);
#endif
			}

#if 0
			// See Issue #334
			if ((wwd_result =
				wwd_wifi_set_legacy_rate(WWD_STA_INTERFACE,
				    6 * 2)) != WWD_SUCCESS) {
#ifdef DBG_MSG
				dbg_printf(
				    "wwd_wifi_set_legacy_rate returned %d",
				    wwd_result);
#endif
			}
#endif

			wiced_wifi_up();

			settings_mgr_load();
			if (cmd->type == WIFI_DRV_CMD_RADIO_ON_NO_JOIN) {
				early_resp->type = WIFI_DRV_RESP_OK;
				return false;
			}

			if (is_provisioned()
			    && (conn_result =
				bring_netif_up(false, cmd->type == WIFI_DRV_CMD_RADIO_ON_NO_SCAN)) != WIFI_DRV_CONN_OK) {
				early_resp->type = WIFI_DRV_RESP_ERR;
				early_resp->payload = conn_result;
				// Have already logged the failure reason
				return false;
			}
			early_resp->type = WIFI_DRV_RESP_OK;
			return false;
		}
		break;
#else
	case WIFI_DRV_CMD_RADIO_ON:
	case WIFI_DRV_CMD_RADIO_ON_NO_JOIN:
	case WIFI_DRV_CMD_RADIO_ON_NO_SCAN:

      dbg_printf("Got a radio on");

      settings_mgr_load();
      if(cmd->type == WIFI_DRV_CMD_RADIO_ON_NO_JOIN) {
        early_resp->type = WIFI_DRV_RESP_OK;
        return false;
      } 

      if(is_provisioned()) 
      {
        dbg_printf("I'm Provisioned");
        conn_result =
          bring_netif_up(false, cmd->type == WIFI_DRV_CMD_RADIO_ON_NO_SCAN);
        if(conn_result != WIFI_DRV_CONN_OK) {
          early_resp->type = WIFI_DRV_RESP_ERR;
          early_resp->payload = conn_result;
          return false;
        }
      }
      early_resp->type = WIFI_DRV_RESP_OK;
      return false;
    break;
#endif
	case WIFI_DRV_CMD_DEPROV:
		settings_mgr_deprov();
		early_resp->type = WIFI_DRV_RESP_OK;
		return false;
		break;
	default:
		early_resp->type = 0;	/* Invalid value */
		return false;
	}
}

/** @brief Does a response require some local processing on the WICED
 * @note
 * Even when only some of the response handling is on the
 * WICED and some is on the ST, this function should return @c true.
 * @param resp the response
 * @return true if local processing is required, false otherwise
 */
static  bool
is_local_resp(struct wifi_drv_resp *resp)
{
	return resp->type == WIFI_DRV_RESP_PROVISIONING_SUCCESS ||
	    resp->type == WIFI_DRV_RESP_PROVISIONING_FAILURE ||
	    resp->type == WIFI_DRV_RESP_UPDATE ||
	    resp->type == WIFI_DRV_RESP_DEPROV;
}

/** @brief Local response processing
 *
 * @param resp the response
 */
static void
process_resp(struct wifi_drv_resp *resp)
{
	uint8_t is_provisioned = 1;

	struct wifi_drv_update_pkt *upd;

	switch (resp->type) {
	case WIFI_DRV_RESP_PROVISIONING_SUCCESS:
		settings_mgr_set(SETTINGS_MGR_IS_PROVISIONED, &is_provisioned,
		    1);
		settings_mgr_save();	// COMMIT
		resp->type = WIFI_DRV_RESP_SLEEP;	// This is what it
							// looks like to the 
							// ST
		break;
	case WIFI_DRV_RESP_PROVISIONING_FAILURE:
		// Need to undo connecting to the new network too. Less
		// surprising.
		app_sta_stop();
		os_semaphore_get(&normal_user_disconnect, OS_WAIT_FOREVER);
		app_sta_start();
		os_semaphore_get(&normal_connected, OS_WAIT_FOREVER);

		settings_mgr_load();	// ROLLBACK
		break;
	case WIFI_DRV_RESP_UPDATE:
		upd = (struct wifi_drv_update_pkt *) resp->payload;

		sw_mgr_init();
		if (sw_mgr_config() != WM_SUCCESS) {
			// Wait until last minute, we know
			// by now that the network is up
			resp->type = WIFI_DRV_RESP_UPDATE_FAILED;
			return;
		}
		if (sw_mgr_ota_client(upd->filename, upd->len) != WM_SUCCESS) {
			resp->type = WIFI_DRV_RESP_UPDATE_FAILED;
			return;
		}
		// This blocks until completion.
		// Safe to tell ST to reboot now--leave resp->type alone.
		// ST should power down the WICED when it reboots.

		// For testing purposes force a reboot here too.
		reboot_required = true;

		break;
	case WIFI_DRV_RESP_DEPROV:
		settings_mgr_deprov();
                reboot_required = true;
		break;
	default:
		wifi_drv_bad_resps++;
	}
}

static struct wifi_drv_cmd
wifi_drv_recv_cmd(void)
{
	int wcd_result = wifi_drv_recv_audio_link(rx_buf, CMD_LEN);
	// No timeouts
	if (wcd_result != WM_SUCCESS) {
		struct wifi_drv_cmd result;

		result.type = 0;	// Invalid
		return result;
	}
	return wifi_drv_unmarshal_audio_link(rx_buf);
}

static void
send_boot_success(void *unused)
{
	struct wifi_drv_resp boot_resp;

	size_t  tx_buf_len;

	boot_resp.type = WIFI_DRV_RESP_BOOT_SUCCESS;
	boot_resp.payload = 0;
	tx_buf_len = wifi_drv_marshal_audio_link(boot_resp, tx_buf);
	wifi_drv_send_audio_link(tx_buf, tx_buf_len);
	// This isn't thread-safe, but if we're called it's because
	// app_thread
	// is hanging waiting for the WICED so there should be no race.
}

unsigned msgs_sent_to_cloud = 0;
unsigned cmds_recvd[WIFI_DRV_CMD_LAST] = { 0 };
unsigned resps_sent_to_st[WIFI_DRV_RESP_LAST] = { 0 };

void
wifi_drv(void const *unused)
{
	size_t  tx_buf_len;

	os_thread_sleep(os_msec_to_ticks(100));	// Allow ST time to listen
						// for byte
	send_boot_success(NULL);
	if (os_timer_create(&resend_boot_success, "resend_boot_success",
		os_msec_to_ticks(3000), send_boot_success, NULL,
		OS_TIMER_PERIODIC, OS_TIMER_AUTO_ACTIVATE) != WM_SUCCESS) {
#ifdef DBG_MSG
		dbg_puts("wiced_rtos_init_timer failed");
#endif
	}

	reboot_required = false;
	do {
		// In pseudocode:
		// 
		// while (1) {
		// recv_from_audio_link();
		// send_to_net();
		// if (is_sync) {
		// recv_from_net();
		// send_to_audio_link();
		// }
		// }

		struct wifi_drv_cmd cmd = wifi_drv_recv_cmd();

		// Got our first command, we can stop sending "I've booted"
		// messages
		if (os_timer_deactivate(&resend_boot_success) !=
		    WM_SUCCESS) {
#ifdef DBG_MSG
			dbg_puts("wiced_rtos_stop_timer failed");
#endif
		}
		if (os_timer_delete(&resend_boot_success) !=
		    WM_SUCCESS) {
#ifdef DBG_MSG
			dbg_puts("wiced_rtos_deinit_timer failed");
#endif
		}

		cmds_recvd[cmd.type]++;
#ifdef DBG_MSG
		//dbg_puts(dbg_get_cmd_name(cmd.type));
#endif

		// There are two types of messages:
		// 1. those that are just translated from UART to JSON
		// and passed onwards to the cloud and
		// 2. those that require at least some local processing
		// on the WICED. Of these some will continue on to
		// do network I/O, others won't.
		// 
		// Similarly with responses below.
		if (is_local_cmd(&cmd)) {
			struct wifi_drv_resp early_resp;

			if (!process_cmd(&cmd, &early_resp)) {
				// The audio processor is waiting for a
				// response. Send it.
				tx_buf_len =
				    wifi_drv_marshal_audio_link(early_resp,
				    tx_buf);
				wifi_drv_send_audio_link(tx_buf, tx_buf_len);
				if (reboot_required) {
#ifdef DBG_MSG
					dbg_puts("reboot_required");
#endif
					break;
				} else {
#ifdef DBG_MSG
					// Important not to send debug
					// messages after the ST thinks
					// we're powered down. Otherwise
					// they just stay in a FIFO
					// somewhere and screw up the boot
					// message on next power-up.
					//dbg_puts(dbg_get_resp_name
					//    (early_resp.type));
#endif
					continue;
				}
			}
			// else keep going and do some network I/O
		}

		tx_buf_len = BUFSIZ;
		wifi_drv_marshal_net(cmd, tx_buf, &tx_buf_len);
		// This function uses C string operations, so tx_buf will be 
		// null-terminated
#ifdef DBG_MSG
		if (tx_buf_len > BUFSIZ)
			dbg_puts("tx_buf overrun");
#endif

    //dbg_puts("calling wifi_drv_send_net");

		enum wifi_drv_conn_status status =
		    wifi_drv_send_net(tx_buf, tx_buf_len, rx_buf, BUFSIZ);
		msgs_sent_to_cloud++;

		// Require a reply. However, it has already been received by
		// the HTTP client!
    //dbg_printf("handling response from cloud: status = %d", status);

		struct wifi_drv_resp resp;

		wifi_drv_unmarshal_net(rx_buf, BUFSIZ, status, &resp);

		//dbg_printf("resp.type = %s", dbg_get_resp_name(resp.type));

		if (is_local_resp(&resp)) {
			process_resp(&resp);
		} else if (cmd.type == WIFI_DRV_CMD_SEND_HEALTH_CHECK &&
		    status == WIFI_DRV_CONN_OK) {
			prov_fail_truncate();
		}

		tx_buf_len = wifi_drv_marshal_audio_link(resp, tx_buf);
		wifi_drv_send_audio_link(tx_buf, tx_buf_len);
		resps_sent_to_st[tx_buf[0]]++;
#ifdef DBG_MSG
		//dbg_printf("resp.type = %s", dbg_get_resp_name(resp.type));
#endif
	} while (!reboot_required);

	/*
	 * Give some for the response to be sent properly 
	 */
	os_thread_sleep(os_msec_to_ticks(100));

	/*
	 * Wait for a reboot!!! 
	 */
	app_sta_stop();
	os_semaphore_get(&normal_user_disconnect, OS_WAIT_FOREVER);
	while (1);
}

#ifdef DBG_MSG
static const char *
dbg_get_cmd_name(uint32_t dbg_cmd)
{
	if (dbg_cmd < DBG_NUM_CMD_NAMES)
		return cmd_names[dbg_cmd];
	else
		return "CMD_UNKNOWN";
}

static const char *
dbg_get_resp_name(uint32_t dbg_resp)
{
	if (dbg_resp < DBG_NUM_RESP_NAMES)
		return resp_names[dbg_resp];
	else
		return "RESP_UNKNOWN";
}

void
dbg_puts(const char *msg)
{
	if (0) // TODO (!dbg_msgs_on)
		return;
	struct wifi_drv_resp resp;

	resp.type = WIFI_DRV_RESP_DBG_MSG;
	resp.payload = (uintptr_t) msg;
	size_t  tx_buf_len = wifi_drv_marshal_audio_link(resp, dbg_tx_buf);

	wifi_drv_send_audio_link(dbg_tx_buf, tx_buf_len);
}

void
dbg_printf(const char *fmt, ...)
{
	va_list arg;
	static char pad[PAD_SIZE];

  os_semaphore_get(&dbg_printf_sema, OS_WAIT_FOREVER);

	va_start(arg, fmt);
	vsnprintf(pad, PAD_SIZE, fmt, arg);
	va_end(arg);
	dbg_puts(pad);

  os_semaphore_put(&dbg_printf_sema);
}
#endif
