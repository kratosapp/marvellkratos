/** @file
 * @brief Serialisation routines to run on the Broadcom chip.
 *
 * There are two serialisation schemes in use here:
 * <OL>
 * <LI>A proprietary one over the UART to the audio processor.</LI>
 * <LI>JSON over HTTPS to the cloud service provider.</LI>
 * </OL>
 */

//#include <stdio.h>
//#include <string.h>
//#include <stddef.h>
//#include <assert.h>
#include "wlan_11d.h"
#include "settings_mgr.h"
#include "wifi_drv_p.h"
#include "st_fw_info.h"
#include "utils.h"
#include "wifi_drv.h"

/** @brief Size of a flash page on the *ST* chip */
#define PAGESIZE 0x80
#define HALF_PAGESIZE (PAGESIZE >> 1)

#define RESP_LEN 5

/** @brief Max size of an extended payload */
#define EXT_PAYLOAD_BUFSIZ 256

extern const uint32_t key[];

unsigned wifi_drv_bad_resp_types;

/**
 * @brief The boilerplate start of a HTTP request
 *
 * @todo
 * There are some parts of this that should be retrieved from @c settings_mgr
 */
#if 0
http requests are handled slightly differently on Marvell
static const char *put_prefix =
    "POST /1/functions/updateSensor HTTP/1.1\r\n"
    "Host: api.parse.com\r\n"
    "Connection: close\r\n"
    "X-Parse-Application-Id: " PARSE_APPLICATION_ID "\r\n"
    "X-Parse-REST-API-Key: " PARSE_REST_API_KEY "\r\n"
    "Content-Type: application/json\r\n"
    "Content-Length:    \r\n"
    "\r\n";
#endif

static enum wifi_drv_cmd_t last_req;	// For testing only

/**
 * @brief Portray a byte array as a JSON string
 *
 * In practice, this involves escaping any double-quotes. Everything else is assumed to be safe to
 * pass through unchanged.
 * @param[in] in_bytes the array to portray
 * @param[in] in_len length of @c in_bytes
 * @param[in,out] out storage to put the result string in. Must be at least 2 * @c in_len long.
 * @result One beyond the end of the result string
 */
char   *
escape(unsigned char *in_bytes, size_t in_len, char *out)
{
	int     i;

	for (i = 0; i < in_len; i++) {
		if (in_bytes[i] == '"')
			*out++ = '\\';
		*out++ = in_bytes[i];
	}
	// add a null terminator to the end of the string
	*out = '\0';

	return out;
}

size_t
wifi_drv_marshal_audio_link(struct wifi_drv_resp resp, unsigned char *buf)
{
	struct wifi_drv_update_pkt *upd;

	buf[0] = resp.type;
	switch (resp.type) {
	case WIFI_DRV_RESP_SNOOZE:
	case WIFI_DRV_RESP_PROVISIONING_SUCCESS:
	case WIFI_DRV_RESP_BOOT_SUCCESS:
	case WIFI_DRV_RESP_POWERDOWN_SUCCESS:
	case WIFI_DRV_RESP_PROVISIONING_FAILURE:
	case WIFI_DRV_RESP_PROCEED:
	case WIFI_DRV_RESP_OK:
	case WIFI_DRV_RESP_ERR:
        case WIFI_DRV_RESP_DEPROV:
	case WIFI_DRV_RESP_TEST:
	case WIFI_DRV_RESP_UPDATE_FAILED:
		write_unaligned_uint32(resp.payload, buf + 1);
		return RESP_LEN;
		break;
	case WIFI_DRV_RESP_FLASH_PAGE:
		write_unaligned_uint32(PAGESIZE, buf + 1);
                st_fw_page_in(resp.payload);
                memcpy(buf + RESP_LEN, &(st_fw_plaintext[resp.payload % CRYPTO_PAGE_SIZE]), PAGESIZE);
		return RESP_LEN + PAGESIZE;
		break;
	case WIFI_DRV_RESP_SLEEP:{
#define SIZEOF_PACKED_SLEEP_PKT (4 + 4)
		write_unaligned_uint32(SIZEOF_PACKED_SLEEP_PKT, buf + 1);
		struct wifi_drv_sleep_pkt *sleep;
		sleep = (struct wifi_drv_sleep_pkt *) resp.payload;
		write_unaligned_uint32(sleep->sleep_time, buf + RESP_LEN);
		write_unaligned_uint32(sleep->regulator_out, buf + RESP_LEN + 4);
		return RESP_LEN + SIZEOF_PACKED_SLEEP_PKT;
		break;
	}
	case WIFI_DRV_RESP_UPDATE:
		// Don't use sizeof(), because we don't transmit padding
#define SIZEOF_PACKED_UPDATE_PKT (2 + 4 + 4)
		write_unaligned_uint32(SIZEOF_PACKED_UPDATE_PKT, buf + 1);
		upd = (struct wifi_drv_update_pkt *) resp.payload;
		write_unaligned_uint16(upd->len, buf + RESP_LEN);
		write_unaligned_uint32(upd->entry_point, buf + RESP_LEN + 2);
		write_unaligned_uint32(upd->initial_sp, buf + RESP_LEN + 6);
		return RESP_LEN + SIZEOF_PACKED_UPDATE_PKT;
		break;
	case WIFI_DRV_RESP_DBG_MSG:
		{
			size_t  msg_len = strlen((char *) resp.payload);

			if (msg_len > 236)
				msg_len = 236;
			write_unaligned_uint32(msg_len + 1, buf + 1);
			memcpy(buf + RESP_LEN, (char *) resp.payload, msg_len);
			buf[RESP_LEN + msg_len] = '\x00';
			return RESP_LEN + msg_len + 1;
		}
		break;
	default:
		wifi_drv_bad_resp_types++;
		return 0;
	}
}

// We don't have an unmarshal_audio_link because the
// low-level Rx call needs to know how many bytes to request,
// so Rx and unmarshalling are done together in wifi_drv_io.c.
unsigned unhandled_cmds = 0;

struct wifi_drv_provisioning_pkt prov;
struct health_check_pkt health;

uint32_t batteryLevel;

/**
 * @brief Receive one length-prefixed parameter from the UART
 *
 * @c Result will be null-terminated in case it's a string.
 * @param[in] rx_buf scratch storage for internal use
 * @param[in,out] result long-term storage for the result
 * @param[out] len_out how long is the string in @c result
 */
static unsigned char *
parse_param(unsigned char *rx_buf, unsigned char *result, uint8_t * len_out)
{
	size_t  len = rx_buf[0];

	memcpy(result, rx_buf + 1, len);
	if (len_out != NULL)
		*len_out = len;
	return rx_buf + 1 + len;	// +1 for length byte
}

/**
 * @brief Receive a Wifi command from the UART
 *
 * @warning This routine is @em not re-entrant
 * @return The @c wifi_drv_cmd struct
 */
struct wifi_drv_cmd
wifi_drv_unmarshal_audio_link(unsigned char *rx_buf)
{
	struct wifi_drv_cmd result;

	result.type = rx_buf[0];

  //dbg_printf("result.type: %02X", result.type);

	switch (result.type) {
	case WIFI_DRV_CMD_PROVISION:
        case WIFI_DRV_CMD_SEND_HEALTH_CHECK:
		{
			static unsigned char extended_payload[EXT_PAYLOAD_BUFSIZ];

			uint32_t extended_payload_len =
			    read_unaligned_uint32(rx_buf + 1);
			wifi_drv_recv_audio_link(extended_payload,
			    extended_payload_len);

			unsigned char *p = extended_payload;

                        switch (result.type) {
                        case WIFI_DRV_CMD_PROVISION:
                                p = parse_param(p, prov.ssid, &(prov.ssid_len));
                                p = parse_param(p, prov.password,
                                                &(prov.password_len));
                                p = parse_param(p, prov.sensorId,
                                                &(prov.sensorId_len));
                                p = parse_param(p, &(prov.hw_version), NULL);
                                p = parse_param(p, (unsigned char *)prov.dev_signature, NULL);
                                (void) parse_param(p, (unsigned char *)&(prov.country_code), NULL);
                                if (prov.country_code == 0)
                                        prov.country_code = COUNTRY_US;
                                result.payload = (uintptr_t) & prov;
                                break;
                        case WIFI_DRV_CMD_SEND_HEALTH_CHECK:
                                p = parse_param(p, (unsigned char *)&(health.batt_lvl), NULL);
								p = parse_param(p, health.prov_failures, &(health.prov_failures_len));
				p = parse_param(p, &(health.hw_version), NULL);
				p = parse_param(p, (unsigned char *)&(health.comp_wake_cnt), NULL);
				(void)parse_param(p, (unsigned char *)health.dev_signature, NULL);
                                result.payload = (uintptr_t) & health;
                                break;
                        default:
                                unhandled_cmds++;
                                result.type = 0; /* Invalid value */
                        }
		}
		break;
	case WIFI_DRV_CMD_CHECK_SW_CRC:
	case WIFI_DRV_CMD_FETCH_FLASH:
	case WIFI_DRV_CMD_ALARM:
	case WIFI_DRV_CMD_SILENCE:
	case WIFI_DRV_CMD_POLL:
	case WIFI_DRV_CMD_POWERDOWN:
	case WIFI_DRV_CMD_DBG_MSGS_ON:
	case WIFI_DRV_CMD_RADIO_ON:
	case WIFI_DRV_CMD_RADIO_ON_NO_JOIN:
	case WIFI_DRV_CMD_RADIO_ON_NO_SCAN:
	case WIFI_DRV_CMD_DEPROV:
	case WIFI_DRV_CMD_TEST_OK:
		result.payload = read_unaligned_uint32(rx_buf + 1);
		break;
	default:
		unhandled_cmds++;
		result.type = 0;	/* Invalid value */
	}
	return result;
}


#ifdef ETHERNET_MAC
char   *
ether_etoa(const wl_ether_addr_t * n)
{
	static char etoa_buf[ETHER_ADDR_LEN * 3];

	char   *c = etoa_buf;

	int     i;

	for (i = 0; i < ETHER_ADDR_LEN; i++) {
		if (i)
			*c++ = ':';
		c += sprintf(c, "%02X", n->octet[i] & 0xff);
	}
	return etoa_buf;
}
#endif


void
wifi_drv_marshal_net(struct wifi_drv_cmd cmd, unsigned char *buf, size_t * len)
// TODO: check against len
{
	struct wifi_drv_provisioning_pkt *prov;

	static char pad[PAD_LEN];

	unsigned char ssid[SSID_MAX];

	uint8_t ssid_len;

        uint8_t password_len = 0;

        uint8_t hardwareRevision = 0;

        uint32_t *devSignature = NULL;

	unsigned char sensorId[SENSOR_ID_MAX];

	uint8_t sensorId_len;

	uint8_t auth_token[AUTH_TOKEN_LEN];

	uint8_t auth_token_len;

  //dbg_printf("buf: %x, len: %d", buf, len);

	memset(ssid, 0, SSID_MAX);
	memset(sensorId, 0, SENSOR_ID_MAX);
	memset(auth_token, 0, AUTH_TOKEN_LEN);
	memset(pad, 0, PAD_LEN);

	// strings need for dynamic JSON data
	static const char *event_tag = "\"event\":";

        static const char *passwordLength_tag = "\"passwordLength\":";

        static const char *hardwareRevision_tag = "\"hardwareVersion\":";
		static const char *compWakeCount_tag = "\"compWakeCount\":";

        static const char *devSignature_tag = "\"devSignature\":";

	static const char *ssid_tag = "\"ssid\":";

#ifdef ETHERNET_MAC
	static const char *bssid_tag = "\"bssid\":";
#endif

	static const char *firmware_tag = "\"firmwareVersion\":";

	static const char *mac_tag = "\"macAddress\":";

	static const char *sensor_tag = "\"sensorId\":";

	static const char *auth_tag = "\"authToken\":";

	static const char *battery_tag = "\"batteryLevel\":";

        static const char *notes_tag = "\"sensorLog\":";

#ifdef ETHERNET_RSSI
        static const char *rssi_tag = "\"rssi\":";
#endif

	static const char *provision_val = "\"provision\"";

	static const char *checkin_val = "\"check in\"";

	static const char *alarm_val = "\"alarm\"";

	static const char *silence_val = "\"alarm off\"";

	static const char *poll_val = "\"poll\"";

	static const char *testok_val = "\"testOK\"";

	static const char *open_curly = "{";

	static const char *close_curly = "}";

	static const char *quote = "\"";

	static const char *comma = ",";

	uint8_t mac_addr_buf[MAC_ADDR_LEN];
#ifdef ETHERNET_MAC
	uint8_t bssid_buf[MAC_ADDR_LEN] = { 0 };
#endif
	uint8_t mac_addr_len = MAC_ADDR_LEN;

#ifdef ETHERNET_MAC
	wiced_bss_info_t ap_info;
	wiced_security_t security;
#endif


	// clean buf before use
	memset(buf, 0, *len);

	// use p as a movable pointer within the destination buffer
	char   *p = (char *) buf;

#if 0
	// copy the static prefix to the buffer
	strcpy(p, put_prefix);
	// increment p to point after the static prefix
	p += strlen(put_prefix);

	// the content-length field will need to be filled in later, save a
	// pointer to it for now
	char   *content_length = p - 7;
#endif

	// save a pointer to the start of the dynamic JSON data
	//char   *data_start = p;

	// if this is a provisioning command
	if (cmd.type == WIFI_DRV_CMD_PROVISION) {
		// get parameters from provisioning packet
		prov = (struct wifi_drv_provisioning_pkt *) cmd.payload;
		strcpy((char *) ssid, (const char *) prov->ssid);
		ssid_len = prov->ssid_len;
                password_len = prov->password_len;
		strcpy((char *) sensorId, (const char *) prov->sensorId);
		sensorId_len = prov->sensorId_len;
		strcpy((char *) auth_token, (const char *) prov->auth_token);
                hardwareRevision = prov->hw_version;
                devSignature = prov->dev_signature;
	} else {
		// else get parameters from settings manager
		settings_mgr_get(SETTINGS_MGR_SSID, ssid, &ssid_len);
		settings_mgr_get(SETTINGS_MGR_SENSOR_ID, sensorId,
		    &sensorId_len);
		settings_mgr_get(SETTINGS_MGR_AUTH_TOKEN, auth_token,
		    &auth_token_len);
	}
	// sanity check
	//assert(ssid_len == strlen((char *) ssid));
	//assert(sensorId_len == strlen((char *) sensorId));
	// assert(auth_token_len == AUTH_TOKEN_LEN);

	// common JSON data for all commands
	strcat(p, open_curly);
	// SSID
	strcat(p, ssid_tag);
	strcat(p, quote);
	p += strlen(p);
	p = escape(ssid, ssid_len, p);
	strcat(p, quote);
	strcat(p, comma);

	// MAC Address
	strcat(p, mac_tag);
	strcat(p, quote);
	p += strlen(p);
	settings_mgr_get(SETTINGS_MGR_MAC_ADDRESS, mac_addr_buf,
	    &mac_addr_len); 
  //strcpy(mac_addr_buf, "11:22:33:44:55:66");
	p = escape(mac_addr_buf, MAC_ADDR_LEN - 1, p);
	strcat(p, quote);
	strcat(p, comma);
#ifdef ETHERNET_MAC
	// BSSID
	if (wiced_network_is_up(WICED_STA_INTERFACE)) {
		// associated - format and return bssid
		wiced_wifi_get_ap_info(&ap_info, &security);
	} else {
		// not associated - return empty string
		memset(ap_info.BSSID.octet, 0, 6);
	}
	strncpy((char *) bssid_buf, ether_etoa(&ap_info.BSSID),
	    MAC_ADDR_LEN - 1);
	strcat(p, bssid_tag);
	strcat(p, quote);
	p += strlen(p);
	p = escape(bssid_buf, MAC_ADDR_LEN - 1, p);
	strcat(p, quote);
	strcat(p, comma);
#endif
	// Firmware Version
	strcat(p, firmware_tag);
	strcat(p, quote);
	p += strlen(p);
	strcat(p, getFirmwareVersion());
	strcat(p, quote);
	strcat(p, comma);
	// Sensor ID
	strcat(p, sensor_tag);
	strcat(p, quote);
	p += strlen(p);
	p = escape(sensorId, sensorId_len, p);

	strcat(p, quote);
	strcat(p, comma);
	// Authentication Token (ignored for provisioning)
	strcat(p, auth_tag);
	strcat(p, quote);
	p += strlen(p);
	if (auth_token[0] != '\0') {
		p = escape(auth_token, AUTH_TOKEN_LEN, p);
	} else {
		p = escape(auth_token, 0, p);
	}
	strcat(p, quote);
	strcat(p, comma);

	// record the last request
	last_req = cmd.type;
	// determine the type of command to send
	switch (cmd.type) {
	case WIFI_DRV_CMD_PROVISION:
		strcat(p, event_tag);
		strcat(p, provision_val);
                strcat(p, comma);
                strcat(p, passwordLength_tag);
                p += strlen(p);
                sprintf(p, "%u", password_len);
                strcat(p, comma);
                strcat(p, hardwareRevision_tag);
                strcat(p, quote);
                p += strlen(p);
                sprintf(p, "%u", hardwareRevision);
                strcat(p, quote);
                strcat(p, comma);
                strcat(p, devSignature_tag);
                strcat(p, quote);
                p += strlen(p);
                sprintf(p, "%08x%08x%08x", (unsigned int)devSignature[0], (unsigned int)devSignature[1], (unsigned int)devSignature[2]);
                strcat(p, quote);
		break;

	case WIFI_DRV_CMD_SEND_HEALTH_CHECK:
		strcat(p, event_tag);
		strcat(p, checkin_val);
		strcat(p, comma);
		strcat(p, battery_tag);
		strcat(p, quote);
		p += strlen(p);
                struct health_check_pkt *health = (struct health_check_pkt *)cmd.payload;
		sprintf(p, "%u", (unsigned int)health->batt_lvl);
		                strcat(p, quote);
                strcat(p, comma);
				strcat(p, compWakeCount_tag);
				strcat(p, quote);
				p += strlen(p);
				sprintf(p, "%u", (unsigned int)health->comp_wake_cnt);
		strcat(p, quote);
                strcat(p, comma);
                strcat(p, notes_tag);
                strcat(p, quote);
                p += strlen(p);
                prov_fail_sprintf(p, health->prov_failures, health->prov_failures_len);
                strcat(p, quote);
		strcat(p, comma);
#ifdef ETHERNET_RSSI
		strcat(p, rssi_tag);
		strcat(p, quote);
		p += strlen(p);
		int32_t rssi;
		wwd_result_t result = wwd_wifi_get_rssi( &rssi );
		if (result == WWD_SUCCESS)
			sprintf(p, "%d", (int)rssi);
		else
			strcat(p, "0");
		strcat(p, quote);
		strcat(p, comma);
#endif
                strcat(p, hardwareRevision_tag);
                strcat(p, quote);
                p += strlen(p);
                sprintf(p, "%u", health->hw_version);
				strcat(p, quote);
				strcat(p, comma);
                strcat(p, devSignature_tag);
                strcat(p, quote);
                p += strlen(p);
                sprintf(p, "%08x%08x%08x", (unsigned int)health->dev_signature[0], (unsigned int)health->dev_signature[1], (unsigned int)health->dev_signature[2]);
                strcat(p, quote);
		break;

	case WIFI_DRV_CMD_ALARM:
		strcat(p, event_tag);
		strcat(p, alarm_val);
		break;

	case WIFI_DRV_CMD_SILENCE:
		strcat(p, event_tag);
		strcat(p, silence_val);
		break;

	case WIFI_DRV_CMD_POLL:
		strcat(p, event_tag);
		strcat(p, poll_val);
		break;

	case WIFI_DRV_CMD_TEST_OK:
		strcat(p, event_tag);
		strcat(p, testok_val);
		break;

	default:
		// exit gracefully
		*len = 0;
		return;
	}
	strcat(p, close_curly);
	// increment p past the preceeding JSON data (needed as p is used
	// to calculate the size of the dynamic data)
	p += strlen(p);

#if 0
	// calculate length of the dynamic JSON part
	*len = (unsigned char *) p - buf;

	// convert the length to string and store it
	sprintf(pad, "%3d", p - data_start);
	// FIXME: clean up assertion for production code
	//assert(p - data_start < 1000);
	// "fill in the blank" for the content length using the pointer we
	// saved earlier
	memcpy(content_length, pad, 3);
#endif
}

static void
parse_sleep_resp(unsigned char *buf, struct wifi_drv_resp *resp)
{
	const char *sleep_tag = "'sleep':";
	const char *reg_tag = "'regulatorOut':";
	static struct wifi_drv_sleep_pkt sleep_upd;


	char   *sleep;
	char   *reg_out;

	sleep_upd.regulator_out = 0;
	sleep_upd.sleep_time = DFL_SLEEP_DURATION;

	sleep = strstr((char *) buf, sleep_tag);
	if (sleep == NULL) {
		resp->type = WIFI_DRV_RESP_ERR;
		resp->payload = WIFI_DRV_CONN_ERROR_NO_SLEEP;
		return;
	}
	sleep += strlen(sleep_tag);
	sleep++;			// Skip leading '

	// there is no need to find end of number string
	// as atoi stops once it gets a non-digit

	// convert string to integer
	sleep_upd.sleep_time = atoi(sleep);

	reg_out = strstr((char *)buf, reg_tag);
	if (reg_out != NULL) {
		reg_out += strlen(reg_tag);
		reg_out++;			// Skip leading '
		sleep_upd.regulator_out = atoi(reg_out);
	}
	else
	{
		sleep_upd.regulator_out = 0;
	}
	
	resp->payload = (uintptr_t)& sleep_upd;

	resp->type = WIFI_DRV_RESP_SLEEP;
}

/**
 * @brief Look for 'firmwareUpdateAvailable':'yes'
 *
 * @param[in] buf HTTP response
 * @param[in,out] resp parsed problem-domain response
 * @return true if an update is available, false otherwise
 */
bool
update_found(char *buf, struct wifi_drv_resp *resp)
{
	const char *firmwareUpdateAvailable_tag =
	    "'firmwareUpdateAvailable':";

	char *result = strstr(buf, firmwareUpdateAvailable_tag);
	if (result == NULL) {
		return false;
	}
	result += strlen(firmwareUpdateAvailable_tag);
	if (strncmp(result, "'yes'", 5) == 0) {
		const char *firmwareUpdateLocation_tag =
		    "'firmwareUpdateLocation':";
		char   *firmwareUpdateLocation;

		const char *firmwareUpdateSize_tag =
		    "'firmwareUpdateSize':";
		char   *firmwareUpdateSize;

		static struct wifi_drv_update_pkt upd;

		firmwareUpdateLocation =
		    strstr((char *) buf,
		    firmwareUpdateLocation_tag);
		if (result == NULL) {
			return false;
		} else {
			firmwareUpdateLocation +=
			    strlen(firmwareUpdateLocation_tag);
			firmwareUpdateLocation++;

			// Some lunatic better not make a
			// file with a ' in its name
			ptrdiff_t loc_len =
			    strchr(firmwareUpdateLocation,
			    '\'') - firmwareUpdateLocation;

			//assert(loc_len < UPD_FILENAME_MAX);
			strncpy(upd.filename,
			    firmwareUpdateLocation, loc_len);
			upd.filename[loc_len] = '\x00';
		}
		firmwareUpdateSize =
		    strstr((char *) buf,
		    firmwareUpdateSize_tag);
		if (result == NULL) {
			return false;
		} else {
			firmwareUpdateSize +=
			    strlen(firmwareUpdateSize_tag);
			firmwareUpdateSize++;
			upd.len = atoi(firmwareUpdateSize);
		}

		resp->type = WIFI_DRV_RESP_UPDATE;
		resp->payload = (uint32_t) & upd;
	} else {	// firmwareUpdateAvailable present,
			// but != yes
		return false;
	}
	return true;
}

/**
 * @brief Look for "runTest":"yes"
 *
 * @param[in] buf HTTP response
 * @param[in,out] resp parsed problem-domain response
 * @return true if an update is available, false otherwise
 */
bool
test_found(char *buf, struct wifi_drv_resp *resp)
{
#if 0
	resp->type = WIFI_DRV_RESP_TEST;
	return true;
#endif

	const char *runTest_tag =
	    "'runTest':";

	char *result = strstr(buf, runTest_tag);
	if (result == NULL) {
		return false;
	}
	result += strlen(runTest_tag);
	if (strncmp(result, "'yes'", 5) == 0) {
		resp->type = WIFI_DRV_RESP_TEST;
	} else {	// runTest present,
			// but != yes
		return false;
	}
	return true;
}

void
wifi_drv_unmarshal_net(unsigned char *buf, size_t len,
    enum wifi_drv_conn_status status, struct wifi_drv_resp *resp)
{
	const char *result_tag = "'result':";

	char   *result;

#ifdef DBG_MSG
	const char *conn_status_names[] = {
		"none",
		"WIFI_DRV_CONN_OK",
		"WIFI_DRV_CONN_ERROR_WIFI_JOIN",
		"WIFI_DRV_CONN_ERROR_NETWORK_UP",
		"WIFI_DRV_CONN_ERROR_GET",
		"WIFI_DRV_CONN_ERROR_EMPTY_RESP",
		"WIFI_DRV_CONN_ERROR_NO_AUTH_TOKEN",
		"WIFI_DRV_CONN_ERROR_NO_SUCCESS",
		"WIFI_DRV_CONN_ERROR_NO_LINK_UP_NOTIF",
		"WIFI_DRV_CONN_ERROR_WLAN_CONNECTIVITY_INIT",
                "WIFI_DRV_CONN_ERROR_NO_SLEEP",
                "WIFI_DRV_CONN_ERROR_BAD_LAST_REQ",
		"WIFI_DRV_CONN_ERROR_SSID_NOT_FOUND",
		"WIFI_DRV_CONN_ERROR_BAD_PASS",
		"WIFI_DRV_CONN_ERROR_TLS_INIT_ROOT_CA",
		"WIFI_DRV_CONN_ERROR_REBOOT_REQUIRED",
        };
#endif

	if (status != WIFI_DRV_CONN_OK) {
#ifdef DBG_MSG
                dbg_printf("status = %s", conn_status_names[status]);
#endif
		resp->payload = status;
		// Fill in the ->type depending on last_req
	}

	switch (last_req) {
	case WIFI_DRV_CMD_PROVISION:
		if (status != WIFI_DRV_CONN_OK) {
			if (status == WIFI_DRV_CONN_ERROR_REBOOT_REQUIRED) {
				resp->type = WIFI_DRV_RESP_BOOT_SUCCESS;
				resp->payload = WIFI_DRV_CONN_ERROR_REBOOT_REQUIRED;
			} else {
				resp->type = WIFI_DRV_RESP_PROVISIONING_FAILURE;
				resp->payload = 0;
			}
                        // prov_fail_log should have already been called
			return;
		}
		result = strstr((char *) buf, result_tag);
		if (result == NULL) {
			resp->type = WIFI_DRV_RESP_PROVISIONING_FAILURE;
			resp->payload = WIFI_DRV_CONN_ERROR_EMPTY_RESP;
                        prov_fail_log(WIFI_DRV_CONN_ERROR_EMPTY_RESP, 0);
			return;
		}
		result += strlen(result_tag);
		if (strncmp(result, "'success'", 9) == 0) {
			const char *auth_token_tag = "'authToken':";

			char   *auth_token;

			auth_token = strstr((char *) buf, auth_token_tag);
			if (auth_token == NULL) {
				resp->type =
				    WIFI_DRV_RESP_PROVISIONING_FAILURE;
				resp->payload =
				    WIFI_DRV_CONN_ERROR_NO_AUTH_TOKEN;
                                prov_fail_log(WIFI_DRV_CONN_ERROR_NO_AUTH_TOKEN, 0);
				return;
			}
			auth_token += strlen(auth_token_tag);
			auth_token++;	// Skip leading '

			settings_mgr_set(SETTINGS_MGR_AUTH_TOKEN,
			    (unsigned char *) auth_token, AUTH_TOKEN_LEN);
			parse_sleep_resp(buf, resp);
			resp->type = WIFI_DRV_RESP_PROVISIONING_SUCCESS;	// Override temporarily for process_resp
		} else {
			resp->type = WIFI_DRV_RESP_PROVISIONING_FAILURE;
			resp->payload = WIFI_DRV_CONN_ERROR_NO_SUCCESS;
                        prov_fail_log(WIFI_DRV_CONN_ERROR_NO_SUCCESS, 0);
		}
		break;
	case WIFI_DRV_CMD_SEND_HEALTH_CHECK:
	case WIFI_DRV_CMD_SILENCE:
	case WIFI_DRV_CMD_TEST_OK:
		if (status != WIFI_DRV_CONN_OK) {
			resp->type = WIFI_DRV_RESP_ERR;
			// Already logged the failure, in wifi_drv_io.c
			return;
		}
		result = strstr((char *) buf, result_tag);
		if (result == NULL) {
#ifdef DBG_MSG
			dbg_puts("no result");
#endif
                        resp->type = WIFI_DRV_RESP_ERR;
                        resp->payload = WIFI_DRV_CONN_ERROR_EMPTY_RESP;
						prov_fail_log(WIFI_DRV_CONN_ERROR_EMPTY_RESP, 0);
			return;
		}
		result += strlen(result_tag);
		if (strncmp(result, "'success'", 9) == 0) {
			// We can get three responses here: SLEEP,
			// UPDATE or TEST

			if (update_found((char *)buf, resp))
				return;
			if (test_found((char *)buf, resp))
				return;
			parse_sleep_resp(buf, resp);
		} else if (strncmp(result, "'deprov'", 8) == 0) {
			resp->type = WIFI_DRV_RESP_DEPROV;
		} else {		// result != success
#ifdef DBG_MSG
			dbg_puts("result != success");
#endif
			resp->type = WIFI_DRV_RESP_ERR;
                        resp->payload = WIFI_DRV_CONN_ERROR_NO_SUCCESS;
		}

		break;

	case WIFI_DRV_CMD_POLL:
		resp->type = WIFI_DRV_RESP_SNOOZE;
		if (status != WIFI_DRV_CONN_OK) {
			resp->payload = 0;
			return;
		}
		result = strstr((char *) buf, result_tag);
		if (result == NULL) {
			resp->payload = 0;	// don't snooze on failure
			return;
		}
		result += strlen(result_tag);
		if (strncmp(result, "'success'", 9) == 0) {
			const char *snooze_tag = "'snooze':";

			char   *snooze;

			snooze = strstr((char *) buf, snooze_tag);
			if (snooze == NULL) {
				resp->payload = 0;	// don't snooze on
							// failure
				return;
			}
			snooze += strlen(snooze_tag);
			snooze++;	// Skip leading '

                        // there is no need to find end of number string
                        // as atoi stops once it gets a non-digit

			// convert string to integer
			resp->payload = atoi(snooze);
		}
		break;

	case WIFI_DRV_CMD_ALARM:
		if (status != WIFI_DRV_CONN_OK) {
			resp->type = WIFI_DRV_RESP_ERR;
			// Already logged the failure, in wifi_drv_io.c
			return;
		}
		result = strstr((char *) buf, result_tag);
		if (result == NULL) {
#ifdef DBG_MSG
			dbg_puts("no result");
#endif
                        resp->type = WIFI_DRV_RESP_ERR;
                        resp->payload = WIFI_DRV_CONN_ERROR_EMPTY_RESP;
						prov_fail_log(WIFI_DRV_CONN_ERROR_EMPTY_RESP, 0);
			return;
		}
		result += strlen(result_tag);
		if (strncmp(result, "'success'", 9) == 0) {
                        resp->type = WIFI_DRV_RESP_OK;
		} else {
#ifdef DBG_MSG
			dbg_puts("result != success");
#endif
			resp->type = WIFI_DRV_RESP_ERR;
                        resp->payload = WIFI_DRV_CONN_ERROR_NO_SUCCESS;
		}

		break;

	default:
		resp->type = WIFI_DRV_RESP_ERR;
                resp->payload = WIFI_DRV_CONN_ERROR_BAD_LAST_REQ;
		return;
	}
}
