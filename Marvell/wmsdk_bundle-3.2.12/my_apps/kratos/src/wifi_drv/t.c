/** @file
 * @brief Unit test the net copro part of wifi_drv
 */

#include <pthread.h>
#include "wifi_drv.h"

void   *
fsm_entry(void *unused)
{
	wifi_drv(NULL);
	return NULL;
}

void
provision(void)
{
	struct wifi_drv_cmd req;

	static unsigned char provision_bytes[] = {
		0x05,
		0x04, 'S', 'S', 'I', 'D',
		0x08, 'P', 'a', 's', 's', 'w', 'o', 'r', 'd',
		0x03, 'U', 'R', 'I',
		0x05, 'C', 'l', 'o', 'u', 'd',
	};

	wifi_drv_unmarshal(provision_bytes, &req);

	struct wifi_drv_event e;

	e.type = WIFI_DRV_EVENT_RX;
	e.payload = (uintptr_t) & req;
	wifi_drv_send_event(&e);
}

int
main(void)
{
	pthread_t fsm_thr;

	wifi_drv_init();
	pthread_create(&fsm_thr, NULL, fsm_entry, NULL);
	// The main thread simulates the audio hardware and processor
	sleep(1);
	provision();
	halt();
	pthread_join(fsm_thr, NULL);
}
