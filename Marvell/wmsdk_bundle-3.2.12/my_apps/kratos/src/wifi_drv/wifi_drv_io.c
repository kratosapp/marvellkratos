/** @file
 * @brief Low-level I/O used by the Broadcom application.
 *
 * There are two devices that we route between for I/O:
 * <OL>
 * <LI>The IP network.</LI>
 * <LI>The UART.</LI>
 * </OL>
 */

#include <assert.h>
#include <stdbool.h>
#include "wm_os.h"
#include "wm_net.h"
#include "mdev_uart.h"
#include "wifi_drv_common.h"
#include "config.h"
#include "settings_mgr.h"
#include "wifi_drv.h"
#include "wm-tls.h"
#include "httpc.h"
#include "ntpc.h"
#include "main.h"

#define MAX_DOWNLOAD_DATA (1024)

/** @brief Size of UART ring buffer */
#define UART_RX_BUFFER_SIZE    64

/**
 * @brief Length of a command on the UART, in bytes.
 *
 * All commands must be the same length.
 * Once we know the command type we know the length of its parameters and can
 * read those.
 */
#define CMD_LEN 1

/**
 * @brief Time after which we assume the ST is dead
 *
 * We can use this to try do an orderly shutdown and try avoid WICED
 * flash corruption.
 */
#define ST_DEAD_TIMEOUT 120

#if 0
 // UART is setup differently on Marvell
// Because kratos.mk defines WICED_DISABLE_STDIO, this struct is used and not the one in platform.c
wiced_uart_config_t uart_config = {
	.baud_rate = 230400,
	.data_width = DATA_WIDTH_8BIT,
	.parity = NO_PARITY,
	.stop_bits = STOP_BITS_1,
	.flow_control = FLOW_CONTROL_CTS_RTS,
	//.flow_control = FLOW_CONTROL_DISABLED,
};

/** @brief Internal ring buffer for the UART */
wiced_ring_buffer_t uart_rx_buffer;
#endif

/** @brief Actual storage for the UART ring buffer */
uint8_t uart_rx_data[UART_RX_BUFFER_SIZE];

/** @brief <em>IP address</em> (not DNS address) of the cloud server */
//static wiced_ip_address_t cloud_ip_address;
static unsigned long cloud_ip_address;

/** @brief Pointer to where a 3-char "Content-Length" value should be placed in @c tx_buf */
char   *content_length;

/**
 * @brief DNS address of the cloud edge server
 *
 * This is hardcoded so would require a software update to change.
 * This and the software update address should be treated the same.
 */
static const char *cloud_dns_address = "api.parse.com";

#define PARSE_URL_STR "https://api.parse.com/"
#define PARSE_URL_STR2 "https://api.parse.com/1/functions/updateSensor"


static const char digicert_root_ca_certificate[] =
    "-----BEGIN CERTIFICATE-----\n"\
    "MIIDxTCCAq2gAwIBAgIQAqxcJmoLQJuPC3nyrkYldzANBgkqhkiG9w0BAQUFADBs\n"\
    "MQswCQYDVQQGEwJVUzEVMBMGA1UEChMMRGlnaUNlcnQgSW5jMRkwFwYDVQQLExB3\n"\
    "d3cuZGlnaWNlcnQuY29tMSswKQYDVQQDEyJEaWdpQ2VydCBIaWdoIEFzc3VyYW5j\n"\
    "ZSBFViBSb290IENBMB4XDTA2MTExMDAwMDAwMFoXDTMxMTExMDAwMDAwMFowbDEL\n"\
    "MAkGA1UEBhMCVVMxFTATBgNVBAoTDERpZ2lDZXJ0IEluYzEZMBcGA1UECxMQd3d3\n"\
    "LmRpZ2ljZXJ0LmNvbTErMCkGA1UEAxMiRGlnaUNlcnQgSGlnaCBBc3N1cmFuY2Ug\n"\
    "RVYgUm9vdCBDQTCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAMbM5XPm\n"\
    "+9S75S0tMqbf5YE/yc0lSbZxKsPVlDRnogocsF9ppkCxxLeyj9CYpKlBWTrT3JTW\n"\
    "PNt0OKRKzE0lgvdKpVMSOO7zSW1xkX5jtqumX8OkhPhPYlG++MXs2ziS4wblCJEM\n"\
    "xChBVfvLWokVfnHoNb9Ncgk9vjo4UFt3MRuNs8ckRZqnrG0AFFoEt7oT61EKmEFB\n"\
    "Ik5lYYeBQVCmeVyJ3hlKV9Uu5l0cUyx+mM0aBhakaHPQNAQTXKFx01p8VdteZOE3\n"\
    "hzBWBOURtCmAEvF5OYiiAhF8J2a3iLd48soKqDirCmTCv2ZdlYTBoSUeh10aUAsg\n"\
    "EsxBu24LUTi4S8sCAwEAAaNjMGEwDgYDVR0PAQH/BAQDAgGGMA8GA1UdEwEB/wQF\n"\
    "MAMBAf8wHQYDVR0OBBYEFLE+w2kD+L9HAdSYJhoIAu9jZCvDMB8GA1UdIwQYMBaA\n"\
    "FLE+w2kD+L9HAdSYJhoIAu9jZCvDMA0GCSqGSIb3DQEBBQUAA4IBAQAcGgaX3Nec\n"\
    "nzyIZgYIVyHbIUf4KmeqvxgydkAQV8GK83rZEWWONfqe/EW1ntlMMUu4kehDLI6z\n"\
    "eM7b41N5cdblIZQB2lWHmiRk9opmzN6cN82oNLFpmyPInngiK3BD41VHMWEZ71jF\n"\
    "hS9OMPagMRYjyOfiZRYzy78aG6A9+MpeizGLYAiJLQwGXFK3xPkKmNEVX58Svnw2\n"\
    "Yzi9RKR/5CYrCsSXaQ3pjOLAEFe4yHYSkVXySGnYvCoCWw9E1CAx2/S6cCZdkGCe\n"\
    "vEsXCS+0yx5DaMkHJ8HSXPfqIbloEpw8nL+e/IBcm2PN7EeqJSdnoDfzAIJ9VNep\n"\
    "+OkuE6N36B9K\n"\
    "-----END CERTIFICATE-----\n";

mdev_t *uart_dev;
unsigned send_net_successes = 0, send_net_failures = 0;

typedef struct s_args {
  unsigned char * tx_buf;
  int tx_buf_len;
  unsigned char * rx_buf;
  int rx_buf_len;
} t_args;

t_args args;

static int last_error;

static int get_last_error(void)
{
   return last_error;
}

static void set_last_error(int error)
{
  last_error = error;
}

static void tls_post(t_args * p_args)
{
	int get_retries;
  int get_result;
  http_session_t handle;
  http_resp_t *http_resp = NULL;
  int dsize = 0;

  unsigned char * tx_buf = 0;
  int tx_buf_len = 0;
  unsigned char * rx_buf = 0;
  int rx_buf_len = 0;

  //dbg_printf("In tls_post");
  //dbg_printf("p_args: %x", p_args);

  if(p_args != NULL) {
    tx_buf = p_args->tx_buf;
    tx_buf_len = p_args->tx_buf_len;
    rx_buf = p_args->rx_buf;
    rx_buf_len = p_args->rx_buf_len;
    
    //dbg_printf("tx_buf: %x, tx_buf_len %d, rx_buf: %x, rx_buf_len: %d", tx_buf, tx_buf_len, rx_buf, rx_buf_len);
  } else {
    dbg_printf("bad p_args");
  }

	#define HTTPS_GET_RETRY_ATTEMPTS 3
	// wiced_https_get is misnamed, it can handle any HTTP method
	*(tx_buf + tx_buf_len) = '\x00';

	for (get_retries = 0; get_retries < HTTPS_GET_RETRY_ATTEMPTS; get_retries++)
	{
    get_result = http_open_session(&handle, PARSE_URL_STR2, NULL);
    if(get_result != WM_SUCCESS) {
        dbg_printf("Failed to open session to %s", PARSE_URL_STR2);
        goto close_http_session;
    } else {
        dbg_printf("Opened session to %s", PARSE_URL_STR2);
    }

    // set up the parameters for the HTTP Request.  This only prepares
    // the request to be sent out
    http_req_t http_req;
    http_req.type = HTTP_POST;
    http_req.resource = PARSE_URL_STR2;
    http_req.version = HTTP_VER_1_1;
    http_req.content = (char *)tx_buf;
    http_req.content_len = strlen((char *)tx_buf);

#if 0
    {
    int i;
    char * foo = tx_buf;
    dbg_printf("tx_buf");
    for(i = 0; i < strlen((char *)tx_buf); i++) 
    {
       dbg_printf("%c", *foo);
       foo++;
    }
    }
#endif

    get_result = http_prepare_req(handle, &http_req, HDR_ADD_CONN_CLOSE | HDR_ADD_CONTENT_TYPE_JSON);
    if(get_result != WM_SUCCESS) {
        dbg_printf("Request prepare failed: URL: %s", PARSE_URL_STR2);
        goto http_session_log_status;
    //} else {
    //  dbg_printf("Request prepare succeeded to %s", PARSE_URL_STR2);
    }
    
    get_result = http_add_header(handle, &http_req, "X-Parse-Application-Id",
        PARSE_APPLICATION_ID);
    if(get_result != WM_SUCCESS) {
      dbg_printf("Failed to add X-Parse-Application-Id to header");
      goto http_session_log_status;
    //} else {
    //  dbg_printf("Add X-Parse-Application-Id okay");
    }

    get_result = http_add_header(handle, &http_req, "X-Parse-REST-API-Key",
        PARSE_REST_API_KEY);
    if(get_result != WM_SUCCESS) {
       dbg_printf("Failed to add X-Parse-REST-API-Key to header");
       goto http_session_log_status;
    //} else {
    //   dbg_printf("X-Parse-REST-API-Key okay");
    }

    /* Send the http get request that we've just prepared to the web server
     */
    get_result = http_send_request(handle, &http_req);
    if(get_result != WM_SUCCESS) {
      dbg_printf("Request send failed: URL: %s", PARSE_URL_STR2);
      goto http_session_log_status;
    }
    else {
       send_net_successes++;
       dbg_printf("Sent POST okay");
    }

    /* Read the response and make sure an HTTP_OK response is received */
    get_result = http_get_response_hdr(handle, &http_resp);
    if(get_result != WM_SUCCESS) {
      dbg_printf("Unable to get response header: %d", get_result);
      goto http_session_log_status;
    //} else {
    //  dbg_printf("Got Response Header");
    }

    if(http_resp->status_code != HTTP_OK) {
      dbg_printf("Unexpected status code received from server: %d", http_resp->status_code);
      dbg_printf("Reason Phrase: %s", http_resp->reason_phrase);
      get_result = -WM_FAIL;
    //} else {
    //  dbg_printf("Good response status code");
    }

    dsize = 0;
    //dbg_printf("Content-Length: %d", http_resp->content_length);
    if(http_resp->content_length < rx_buf_len) {
       rx_buf_len = http_resp->content_length;
    }
    do {
      //dbg_printf("Reading response content");

      dsize = http_read_content(handle, rx_buf, rx_buf_len);

      //dbg_printf("After response");

      if(dsize < 0) {
        //dbg_printf("request %d bytes ...", rx_buf_len);

        dbg_printf("Read data failed: %d", dsize);
        set_last_error(-WM_FAIL);
        break;
      }

      else if(dsize == 0) {
        //dbg_printf("All data read");
        dbg_printf("RX Buf: %s", rx_buf);
      }

      else if(dsize > 0) {
        //dbg_printf("Data read");
        //dbg_printf("RX Buf: %s", rx_buf);
      }

      os_thread_sleep(os_msec_to_ticks(rand() % 10));

    } while(dsize > 0) ;

    //dbg_printf("Completed read of response");
    set_last_error(WIFI_DRV_CONN_OK);

		//get_result = wiced_https_get(&cloud_ip_address, (const char *)tx_buf, rx_buf, rx_len, cloud_dns_address);
    //get_result = httpc_get(cloud_dns_address, &handle, &http_resp, &httpc_cfg);
		//if (get_result == WM_SUCCESS) {
		//	send_net_successes++;
		//	return WIFI_DRV_CONN_OK;
		//}
close_http_session:
    http_close_session(&handle);

http_session_log_status:
    if(get_last_error() != WIFI_DRV_CONN_OK) {
      prov_fail_log(WIFI_DRV_CONN_ERROR_GET, get_result);
	    rx_buf[0] = '\x00';
	    send_net_failures++;
#ifdef DBG_MSG
	    dbg_printf("httpc_get returned %d", get_result);
#endif
    } else {
      break; // leave retry loop early
    }
	} // end retry loop


  if(get_result == WM_SUCCESS) {
     set_last_error(WIFI_DRV_CONN_OK);
  }

	//if (0) // get_result == WICED_TLS_UNTRUSTED_CERTIFICATE)
	//{
	//	extern bool reboot_required;
	//	reboot_required = true;
	//	set_last_error(WIFI_DRV_CONN_ERROR_REBOOT_REQUIRED);
  //} 
	else{
    set_last_error(WIFI_DRV_CONN_ERROR_GET);
	}

  os_semaphore_put(&tls_post_done);

}

static void tls_cert_test(void)
{
  int result;
  http_session_t handle;
  httpc_cfg_t httpc_cfg;
  dbg_printf("Synchronizing time with NTP");

  ntpc_sync("pool.ntp.org", 0);

  /*
   * We as a client want to verify server certificate. Server
   * does not need to verify ours to pass NULL.
   */
  tls_client_t cfg;
  memset(&cfg, 0x00, sizeof(tls_client_t));
  cfg.ca_cert = (unsigned char *)digicert_root_ca_certificate;
  cfg.ca_cert_size = sizeof(digicert_root_ca_certificate) - 1;
  cfg.client_cert = NULL;
  cfg.client_cert_size = 0;

  SSL_CTX *ctx = tls_create_client_context(&cfg);
  if(!ctx) {
    dbg_printf("%s: failed to create client context\r\n", __func__);
  //} else {
  //  dbg_puts("Set up client context");
  }

  http_resp_t *http_resp;
  memset(&httpc_cfg, 0x00, sizeof(httpc_cfg_t));
  httpc_cfg.ctx = ctx;

  result = httpc_get(PARSE_URL_STR, &handle, &http_resp, &httpc_cfg);
  if(result != WM_SUCCESS) {
    tls_purge_client_context(ctx);
    dbg_printf("%s: httpc_get: failed", __func__);
    set_last_error(WIFI_DRV_CONN_ERROR_TLS_INIT_ROOT_CA);
    //return WIFI_DRV_CONN_ERROR_TLS_INIT_ROOT_CA;
  } else {
    //dbg_puts("Set up SSL session");
    //dbg_printf("Status code: %d", http_resp->status_code);

    int dsize;
    char buf[MAX_DOWNLOAD_DATA];
    while(1) {
      /* keep reading data over the HTTPS session and print it out on
       * the console
       */
       dsize = http_read_content(handle, buf, MAX_DOWNLOAD_DATA);
       if(dsize < 0) {
         //dbg_printf("Unable to read data on http session: %d", dsize);
         break;
       }

       if(dsize == 0) {
         //dbg_printf("All Data Read");
         break;
       }
    }

    set_last_error(WIFI_DRV_CONN_OK);
  }

  http_close_session(&handle);
  tls_purge_client_context(ctx);

  os_semaphore_put(&tls_certtest_done);
}

/*
 * In this case we do not run our code in the main thread context. This
 * is because tls library requires more stack than what the main thread
 * has.  So it has its own thread.
 */
static os_thread_stack_define(tls_stack, 8192+4096);

static void tls_certtest_main(os_thread_arg_t data)
{
  //dbg_printf("Heap before %d", os_get_free_size());

  tls_cert_test();

  //dbg_printf("Heap after %d", os_get_free_size());

  /* Delete myself */
  os_thread_delete(NULL);
}

static void tls_post_main(os_thread_arg_t data)
{
  //dbg_printf("Heap before %d", os_get_free_size());

  //dbg_printf("os_thread_arg: %x", data);

  tls_post(data);

  //dbg_printf("Heap after %d", os_get_free_size());

  /* delete myself */
  os_thread_delete(NULL);
}

void wifi_drv_uart_init(void)
{
	int ret;

  wmstdio_init(UART1_ID, 0);
  wmprintf("You are connected to UART1. Change to UART0\r\n");

	ret = uart_drv_init(UART0_ID, UART_8BIT);
	if (ret != WM_SUCCESS)
		return;

  /* This is an optional call to make uart_drv_read() call blocking.
     By default it is non-blocking.  This call should be made
     between uart_drv_init() and before uart_drv_open() to overwrite
     the default configuration */
  ret = uart_drv_blocking_read(UART0_ID, true);
  if (ret != WM_SUCCESS)
    return;

	uart_dev = uart_drv_open(UART0_ID, 230400);
	if (uart_dev == NULL)
		return;
}


enum wifi_drv_conn_status
wifi_drv_config(void)
{
  struct hostent *hentry = NULL;
  const char * host = cloud_dns_address;

#ifdef DBG_MSG
	dbg_printf("Resolving IP address of HTTPS server: %s", cloud_dns_address);
#endif
	//wiced_result_t result = wiced_hostname_lookup(cloud_dns_address, &cloud_ip_address, 10000);
  int result = net_gethostbyname(cloud_dns_address, &hentry);

   //dbg_printf("back from net_gethostbyname()");

   if(hentry) {
    struct sockaddr_in tmp;;
    memset(&tmp, 0, sizeof(struct sockaddr_in));
    memcpy(&tmp.sin_addr.s_addr, hentry->h_addr_list[0], hentry->h_length);
    host = inet_ntoa(tmp.sin_addr);
    cloud_ip_address = inet_addr(host);

#ifdef DBG_MSG
	dbg_printf("Server is at %s", host);
#endif
  }
  else {
#ifdef DBG_MSG
		dbg_printf("net_gethostbyname failed - returned %d", result);
#endif
		prov_fail_log(WIFI_DRV_CONN_ERROR_HOSTNAME_LOOKUP, result);
		return WIFI_DRV_CONN_ERROR_HOSTNAME_LOOKUP;
	}

	/*
	 * Initialize the root CA certificate 
	 */
	//result = wiced_tls_init_root_ca_certificates( digicert_root_ca_certificate );

  /*
   * We as client want to verify server certificate.  Server does not
   * need to verify ours, so pass NULL
   */
  dbg_puts("Setting up TLS session");

  /*
   * tls requires about 8K of stack.  So, we cannot run tls API's in
   * the main thread context.  Create a separate thread.
   */
  os_thread_create(NULL,       /* thread handle */
                   "tls",      /* thread name */
                   tls_certtest_main,   /* entry function */
                   NULL,        /* argument */
                   &tls_stack, /* stack */
                   OS_PRIO_2); /* priority - medium low */


  os_semaphore_get(&tls_certtest_done, OS_WAIT_FOREVER);

	return get_last_error();
}

enum wifi_drv_conn_status
wifi_drv_send_net(unsigned char *tx_buf, size_t len, unsigned char *rx_buf,
    size_t rx_len)
{
  int i;
  //dbg_puts("in wifi_drv_send_net");
  args.tx_buf = tx_buf;
  args.tx_buf_len = len;
  args.rx_buf = rx_buf;
  args.rx_buf_len = rx_len;

  //dbg_printf("args: %x", args);
  //dbg_printf("tx_buf: %x, tx_buf_len: %d, rx_buf: %x, rx_buf_len: %d", tx_buf, len, rx_buf, rx_len);

  os_thread_create(NULL,       /* thread handle */
                   "tls",      /* thread name */
                   tls_post_main,   /* entry function */
                   &args,      /* argument */
                   &tls_stack, /* stack */
                   OS_PRIO_2); /* priority - medium low */


  os_semaphore_get(&tls_post_done, OS_WAIT_FOREVER);

  //dbg_printf("Acquired tls_post_done semaphore");

  return get_last_error();

}

// The fake response parser in @c wifi_drv_unmarshal_net means
// that this function isn't needed quite yet.
void
wifi_drv_recv_net(unsigned char *buf, size_t len)
{
	strncpy((char *) buf, "{'status':0}", len);
}

int
wifi_drv_send_audio_link(unsigned char *buf, size_t len)
{
	if (uart_drv_write(uart_dev, buf, len) != len)
		return -WM_FAIL;
	return WM_SUCCESS;
}

int
wifi_drv_recv_audio_link(unsigned char *buf, size_t len)
{
  int read_bytes = 0;
  //dbg_printf("Waiting for %d bytes", len);

  while(read_bytes != len) {
	  read_bytes += uart_drv_read(uart_dev, &(buf[read_bytes]), len-read_bytes);
  }
	return WM_SUCCESS;
}
