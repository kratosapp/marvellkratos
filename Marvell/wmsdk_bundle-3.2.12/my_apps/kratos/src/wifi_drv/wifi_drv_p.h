/** @file
 * @brief Private declarations for use by the wifi_drv module @em only.
 *
 * Most of this file specifies the interface to the net copro's
 * @c wifi_drv FSM.
 *
 * The @c *_audio_link and @c *_net functions differ from the
 * equivalents in the audio processor code in that on the WICED
 * platform I/O is blocking, whereas on the ST we use non-blocking
 * I/O. This makes a big difference to the state machine
 * implementation too, it isn't really event-driven any more but more
 * linear code.
 *
 * @warning The definìtions in this file @em must be identical to those in the audio processor code
 */

#ifndef _WIFI_DRV_P_H_
#define _WIFI_DRV_P_H_

#include <stdbool.h>
#include <stddef.h>
#include <stdint.h>

#include "wifi_drv_common.h"

/** @brief Initialise whatever hardware/OS facilities are used by both transports */
void    wifi_drv_init(void);

/** @brief Configure the client lib for a new Wifi network topology
 *
 * This should be called after associating with a new Wifi network.
 */
enum wifi_drv_conn_status wifi_drv_config(void);

/**
 * @brief Low-level send of data over a reliable byte-stream transport.
 *
 * In our implementation, this uses a UART.
 * @param[in] p buffer to transmit
 * @param[in] l length of @c p
 */
int    wifi_drv_send_audio_link(unsigned char *p, size_t l);

/**
 * @brief Receive from the UART.
 */
int wifi_drv_recv_audio_link(unsigned char *buf, size_t len);

/**
 * @brief Low-level send of data over a reliable byte-stream transport.
 *
 * In our implementation, this uses a UART.
 * @param[in] p buffer to transmit
 * @param[in] l length of @c p
 */
enum wifi_drv_conn_status    wifi_drv_send_net(unsigned char *p, size_t l, unsigned char *rx_buf,
    size_t rx_len);

/**
 * @brief Low-level receive of data over a reliable byte-stream transport.
 *
 * In our implementation, this uses a UART.
 * @param[in] p buffer to hold received data
 * @param[in] l length of received data
 */
void    wifi_drv_recv_net(uint8_t * rxBuffer, size_t rxBufferSize);

/**
 * @brief Turn a @c wifi_drv_resp into a byte array
 *
 * This uses our proprietary encoding.
 *
 * @todo
 * Write a @c protocol.dox describing the encoding.
 * @param[in] resp response to serialise
 * @param[in] buf buffer to store bytes
 * @return number of bytes written to buf
 * @see wifi_drv_cmd
 */
size_t    wifi_drv_marshal_audio_link(struct wifi_drv_resp resp,
    unsigned char *buf);

/**
 * @brief Parse input read from the UART
 * @param[in] buf buffer containing received bytes
 * @param[in,out] cmd storage to place parsed value in
 */
struct wifi_drv_cmd wifi_drv_unmarshal_audio_link(unsigned char *buf);

/**
 * @brief Turn a @c wifi_drv_cmd into a byte array
 *
 * This uses JSON encoding.
 * @param[in] cmd command to serialise
 * @param[in] buf buffer to store bytes
 * @param[in] len PRE: length of @c buf; POST: used bytes in @c buf
 */
void    wifi_drv_marshal_net(struct wifi_drv_cmd cmd, unsigned char *buf,
    size_t * len);

/**
 * @brief Parse JSON response read from cloud
 * @param[in] buf buffer containing received bytes
 * @param[in] len length of received bytes
 * @param[in] status any error return from wiced_http_get()
 * @param[in,out] resp storage to place parsed value in
 * @see wifi_drv_resp
 */
void    wifi_drv_unmarshal_net(unsigned char *buf, size_t len,
    enum wifi_drv_conn_status status, struct wifi_drv_resp *resp);

// From here on downwards handles decryption of the ST firmware.

/** @brief Size of each independently-encrypted block of the firmware */
#define CRYPTO_PAGE_SIZE 4096

/** @brief Current decrypted page of the ST firmware */
extern unsigned char *st_fw_plaintext;

/** @brief CRC32 of the @em full firmware */
extern uint32_t st_fw_crc;

/** @brief Set up for reading the ST firmware */
void st_fw_open(void);

/**
 * @brief Ensure that the firmware starting at @c offset is loaded into @c st_fw_plaintext
 *
 * @param offset offset into the firmware that we're going to read from
 */
void st_fw_page_in(int offset);

/** @brief Free any resources for reading the ST firmware */
void st_fw_close(void);

#endif
