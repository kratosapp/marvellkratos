/** @file
 * @brief Manage the ST firmware
 */

#include <string.h>
#include <stdlib.h>
#ifdef UNIT_TEST
#include <stdio.h>
#endif
#include "st_fw_info.h"
#include "utils.h"
#include "sw_mgr.h"
#include "wifi_drv_p.h"
#include "wm_os.h"

/** @brief We decrypt this many bytes at a time */
#define CRYPTO_PAGE_SIZE 4096

/** @brief log_2 @c CRYPTO_PAGE_SIZE */
#define PAGE_SHIFT 12

uint32_t st_fw_crc;
unsigned char *st_fw_plaintext;

static int curr_page = -1;      /* Must be invalid at prog init so page 0 is decrypted */

#define KEY0 (0)
#define KEY1 (1)
#define KEY2 (2)
#define KEY3 (3)

void
st_fw_page_in(int offset)
{
        static const uint32_t key[] = { KEY0, KEY1, KEY2, KEY3 };
        // TODO: fetch from settings_mgr, where it was placed by an
        //       UPDATE message from the cloud
        
        int page_no = offset >> PAGE_SHIFT;
        if (page_no == curr_page)
                return;

        // Is it the last block?
        int page_size;
        if (page_no >= (ST_FW_SIZE >> PAGE_SHIFT))
                page_size = ST_FW_SIZE % CRYPTO_PAGE_SIZE;
        else
                page_size = CRYPTO_PAGE_SIZE;
        
        memcpy(st_fw_plaintext, &st_fw[page_no << PAGE_SHIFT], page_size);
        btea((uint32_t *)st_fw_plaintext, -(page_size >> 2), key);
        curr_page = page_no;
}

void
st_fw_open(void)
{
        if (st_fw_plaintext != NULL)
                return;
        
        st_fw_plaintext = os_mem_alloc(CRYPTO_PAGE_SIZE);
        if (st_fw_plaintext == NULL)
                return;
        
        st_fw_crc = CRC_INIT_VAL;
#ifdef UNIT_TEST
        dbg_printf("%s: st_fw_crc %x\n", __func__, st_fw_crc);
#endif
        int i;
        for (i = 0; i < ST_FW_SIZE; i += CRYPTO_PAGE_SIZE) {
                st_fw_page_in(i);
#ifdef UNIT_TEST
                if (i == 0) {
                        dbg_printf("%s: i 0 st_fw_plaintext %x %x %x %x ... %x %x %x %x\n",
                               __func__,
                               st_fw_plaintext[0], st_fw_plaintext[1],
                               st_fw_plaintext[2], st_fw_plaintext[3],
                               st_fw_plaintext[4092], st_fw_plaintext[4093],
                               st_fw_plaintext[4094], st_fw_plaintext[4095]);
                } else if (i == CRYPTO_PAGE_SIZE) {
                        dbg_printf("%s: i 1 st_fw_plaintext %x %x %x %x ...\n",
                               __func__,
                               st_fw_plaintext[0], st_fw_plaintext[1],
                               st_fw_plaintext[2], st_fw_plaintext[3]);
                }
#endif

                // Is it the last block
                int page_size;
                if (i > (ST_FW_SIZE - CRYPTO_PAGE_SIZE))
                        page_size = ST_FW_SIZE % CRYPTO_PAGE_SIZE;
                else
                        page_size = CRYPTO_PAGE_SIZE;
#ifdef UNIT_TEST
                printf("%s: page_size %d\n", __func__, page_size);
#endif
                
#ifdef UNIT_TEST
                printf("%s: i %d st_fw_crc %x\n", __func__, i, st_fw_crc);
#endif
                st_fw_crc = calc_crc_partial(st_fw_crc, st_fw_plaintext, page_size);
        }
        st_fw_crc = calc_crc_finish(st_fw_crc);
}

void
st_fw_close(void)
{
        os_mem_free(st_fw_plaintext);
        st_fw_plaintext = NULL;
}
