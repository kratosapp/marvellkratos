// gcc -o t -I../../include -DUNIT_TEST=1 -DKEY0=0xcafe -DKEY1=0xd00d -DKEY2=0xcafe -DKEY3=0xbabe -I../wifi_drv t.c crc.c ../st_fw.c ../wifi_drv/st_fw.c ../utils/xxtea.c ../st_fw_plaintext.c

#include <stdlib.h>
#include <stdio.h>
#include "wifi_drv_p.h"
#include "st_fw_info.h"

extern const unsigned char st_fw_full_plaintext[];

int
main(void)
{
        st_fw_open();
        dbg_printf("%x\n", st_fw_crc);

        dbg_printf("%x\n", calc_crc(st_fw_full_plaintext, ST_FW_SIZE));

        dbg_printf("st_fw_full_plaintext %x %x %x %x ... %x %x %x %x %x %x %x %x ...\n",
               st_fw_full_plaintext[0], st_fw_full_plaintext[1],
               st_fw_full_plaintext[2], st_fw_full_plaintext[3],
               st_fw_full_plaintext[4092], st_fw_full_plaintext[4093],
               st_fw_full_plaintext[4094], st_fw_full_plaintext[4095],
               st_fw_full_plaintext[4096], st_fw_full_plaintext[4097],
               st_fw_full_plaintext[4098], st_fw_full_plaintext[4099]);
        
        exit(EXIT_SUCCESS);
}
