/** @file
 * @brief Client analogue of the standard WICED ota_server library
 *
 * This file was written with process_upgrade_chunk() in
 * wiced_ota_server.c open in another window.
 */

#include <stdbool.h>
#include <assert.h>
//#include "wiced_network.h"
//#include "platform_dct.h"
//#include "wiced_waf_common.h"
//#include "wiced_framework.h"
//#include "waf_platform.h"
//#include "wiced_tcpip.h"
#include "sw_mgr.h"
#include "settings_mgr.h"
#include "wifi_drv.h"
#include "wm_os.h"

/**
 * @brief Size of each chunk of an image fetched from the server
 *
 * Empirically derived, a malloc of 64k failed
 *
 * @todo Since we're using zero-copy and looping over many recv calls
 * for a HTTP request, perhaps we can just request the whole image in
 * one HTTP request and don't need chunking at the HTTP level?
 */
#define CHUNK_SIZE 4000

/** @brief Offset of the filename part of the URL in `get_request` */
#define FILENAME_OFFSET 5

#define HEAD_FILENAME_OFFSET 6

/** @brief Offset of the "from bytes" part of `get_request` */
#define BYTES_FROM_OFFSET 61

/** @brief Offset of the "to bytes" part of `get_request` */
#define BYTES_TO_OFFSET 68

/** @brief Size of a scratchpad for printing numbers */
#define PAD_MAX 8

#define BUF_SIZE (1024)

#define SECURE_HTTP

#if 0
/** @brief State of a connection with the server */
struct http_pipeline_context {
    wiced_tcp_socket_t         socket; /**< TCP socket */
#ifdef SECURE_HTTP
    wiced_tls_simple_context_t context; /**< TLS context */
#endif
};
#endif

/**
 * @brief Software update server DNS address
 *
 * This is hardcoded so would require a software update to change.
 * This and the cloud address should be treated the same.
 */
#define SW_UPD_SVR "roost-dev.parseapp.com"
static const char *sw_upd_svr = SW_UPD_SVR;

/**
 * @brief HTTP GET request to send to server
 *
 * @note
 * This can't be a char *, because this will cause the string itself to be put into flash.
 * We want to change parts of it later, so it must be an array.
 */
char get_request[] =
    "GET /                                 HTTP/1.1\r\n"
    "Range: bytes=012345-012345\r\n"
    "Host: " SW_UPD_SVR "\r\n"
    "\r\n";

char head_request[] = 
    "HEAD /                                 HTTP/1.1\r\n"
    "Host: " SW_UPD_SVR "\r\n"
    "\r\n";

static const char *header_body_boundary = "\r\n\r\n";
static const char *partial_content_code = "HTTP/1.1 206";
static const char *content_length_tag = "Content-Length: ";

/** @brief Place to paste in filename */
static char *filename_placeholder;

static char *head_filename_placeholder;

/** @brief Place to paste in "from" part of byte range */
static char *bytes_from_placeholder;

/** @brief Place to paste in "to" part of byte range */
static char *bytes_to_placeholder;

/** @brief *IP address* (not DNS address) of the software update server */
//static wiced_ip_address_t upd_ip_address;

uint16_t buflen = BUF_SIZE;
char buf[BUF_SIZE];

void
sw_mgr_init(void)
{
    filename_placeholder = get_request + FILENAME_OFFSET;
    head_filename_placeholder = head_request + HEAD_FILENAME_OFFSET;
    bytes_from_placeholder = get_request + BYTES_FROM_OFFSET;
    bytes_to_placeholder = get_request + BYTES_TO_OFFSET;
}

int
sw_mgr_config(void)
{
    return 0; // TODO
    //return wiced_hostname_lookup(sw_upd_svr, &upd_ip_address, 10000);

    /*
     * Root CA certificate was already initialised in wifi_drv, so doesn't need to happen here.
     * Luckily enough Facebook are smart enough to have api.parse.com and *.parseapp.com signed
     * by the same CA.
     */
}

/**
 * @brief Find the currently-running app index
 *
 * I have no idea if this implementation makes use of public or private APIs.
 * I can't distinguish the two in the WICED libraries.
 *
 * @todo Is there no easy way to do this using the WICED libraries?
 * Storing it in settings_mgr means it's possible for the two to fall
 * out of sync.
 *
 * @return DCT_APP0_INDEX or DCT_APP1_INDEX
 */
static uint8_t
get_curr_app_idx(void)
{
    uint8_t result;
    uint8_t result_len;

    result_len = 1;
    settings_mgr_get(SETTINGS_MGR_BOOT_APP_IDX, &result, &result_len);
    assert(result_len == 1);
    return result;
}

/**
 * @brief Get the app index to upgrade
 *
 * This will be whichever of APP0 or APP1 that is *not* currently running.
 * @return an index suitable for passing to `wiced_framework_app_open`
 * @see wiced_framework_app_open
 */
static uint8_t
other_app_idx(void)
{
        uint8_t curr_app_idx = get_curr_app_idx();

        if (curr_app_idx == 0) // TODO DCT_APP0_INDEX)
                return 0; // TODO DCT_APP1_INDEX;
        else
                return 1; // TODO DCT_APP0_INDEX;
}

int open_app_status;

/**
 * @brief Open a DCT app
 *
 * This also sets the app size in the DCT area
 * @param[in] app_size the app size
 * @param[in,out] app empty data structure to fill in
 * @return true for success, false for failure
 */
// TODO
#if 0
static wiced_result_t
open_app(uint32_t app_size, wiced_app_t *app)
{
        uint32_t curr_size;

        if ((open_app_status = wiced_framework_app_open(other_app_idx(), app)) != WICED_SUCCESS)
                return open_app_status;
        if ((open_app_status = wiced_framework_app_get_size(app, &curr_size)) != WICED_SUCCESS)
                return open_app_status;
        if ((open_app_status = wiced_framework_app_set_size(app, app_size)) != WICED_SUCCESS)
                return open_app_status;
#ifdef DBG_MSG
	dbg_printf("open_app: increased size of app %d from %d to %d", app->app_id, curr_size, app_size);
#endif
        if ((open_app_status = wiced_framework_app_get_size(app, &curr_size)) != WICED_SUCCESS)
                return open_app_status;
        if (curr_size < app_size)
                return WICED_ERROR;
        return WICED_SUCCESS;
}
#endif

static void
set_curr_app_idx(uint8_t i)
{
    settings_mgr_set(SETTINGS_MGR_BOOT_APP_IDX, &i, 1);
    settings_mgr_save();
}

// TODO
#if 0
/**
 * @brief Close the write-handle to an app image in flash
 *
 * @param[in,out] app previously-allocated app using `open_app`
 * @param[in] succeeded did the upgrade succeed?
 * @see open_app
 */
static void
close_app(wiced_app_t *app, bool succeeded)
{
    uint8_t new_app_idx = other_app_idx();

#ifdef DBG_MSG
    dbg_puts(__func__);
#endif
        // TODO wiced_framework_app_close(app);
        if (succeeded) {
                // TODO wiced_framework_set_boot(new_app_idx, PLATFORM_DEFAULT_LOAD);
                set_curr_app_idx(new_app_idx);
#ifdef DBG_MSG
                dbg_puts("curr_app_idx set");
#endif
        }
}
#endif

/**
 * @brief Open a HTTP connection to a server that will work with "Connection: keepalive"
 *
 * @param[in] address IP address of server
 * @param[in] dns_name DNS name of server, verified against cert "Common Name"
 * @param[in,out] result struct to fill in with connection details
 * @return WICED_SUCCESS for success, anything else for failure
 */
// TODO
#if 0
static wiced_result_t
open_http_pipeline(wiced_ip_address_t *address, const char *dns_name, struct http_pipeline_context *result)
{
    wiced_result_t status = WICED_ERROR;

#ifdef SECURE_HTTP
    wiced_tls_init_simple_context( &(result->context), dns_name );
#endif
    if ((status = wiced_tcp_create_socket( &(result->socket), WICED_STA_INTERFACE )) != WICED_SUCCESS)
            return status;
#ifdef SECURE_HTTP
    wiced_tcp_enable_tls( &(result->socket), &(result->context) );

    status = wiced_tcp_connect( &(result->socket), address, 443, 10000 );
#else
    status = wiced_tcp_connect( &(result->socket), address, 80, 10000 );
#endif
    if ( status != WICED_SUCCESS )
    {
        wiced_tcp_delete_socket( &(result->socket) );
        return ( status );
    }

    return ( status );
}

/**
 * @brief Clean up once we're finished with a HTTP pipeline
 *
 * @param[in,out] pipe_ctx previously filled-in struct from `open_http_pipeline`
 * @see open_http_pipeline
 */
static void
close_http_pipeline(struct http_pipeline_context *pipe_ctx)
{
    wiced_tcp_disconnect( &(pipe_ctx->socket) );
    wiced_tcp_delete_socket( &(pipe_ctx->socket) );
}

size_t bytes_fetched = 0;

/**
 * @brief Write received data to flash
 *
 * This function was written by reference to the WICED C code.
 * The calls you need to make are undocumented and have no examples to illustrate their use.
 * And there are useful-looking functions prototypes in header files with no implementation
 * to waste your time (wiced_packet_get_next_fragment).
 *
 * @todo This interface is zero-copy. So rx_buf points into the mbuf payload. So we don't need
 * to allocate any space for it.
 *
 * @param[in] p result of wiced_tcp_receive
 * @param[in,out] rx_buf scratch buffer to use
 * @param[out] rx_len how many bytes were received, *excluding* HTTP headers
 * @return WICED_SUCCESS if everything went ok, an error code if there was an error
 */
static wiced_result_t
write_mbuf_chain_to_flash(wiced_packet_t *p, uint16_t *bin_rx_len, wiced_app_t *app, bool first_mbuf_chain, size_t *content_length)
{
    wiced_result_t status = WICED_ERROR;
    uint16_t available;
    uint16_t rxd_this_frag;
    char *rx_buf;

    // Process the first fragment to skip past the HTTP headers.
    // This will break if the HTTP headers ever split across two fragments.
    wiced_packet_get_data( p, 0, (uint8_t **)&rx_buf, &rxd_this_frag, &available );

    ptrdiff_t header_len = 0;
    char *start_of_payload = rx_buf;
    if (first_mbuf_chain) {
        // Check we really got a "206"
        if (strncmp(rx_buf, partial_content_code, strlen(partial_content_code))) {
#ifdef DBG_MSG
	    dbg_puts(rx_buf);
#endif
            status = WICED_ERROR;
            goto cleanup;
        }

        // Get the "Content-Length"
        char *content_length_str = strstr(rx_buf, content_length_tag);
        content_length_str += strlen(content_length_tag);
        *content_length = atoi(content_length_str);

        // Now we need to skip the HTTP headers
        start_of_payload = strstr(rx_buf, header_body_boundary);
        start_of_payload += strlen(header_body_boundary);
        header_len = start_of_payload - rx_buf;
    }
    *bin_rx_len = rxd_this_frag - header_len;

    // Update bin_rx_len even if errors here,
    // so that caller can update offset into HTTP resource.
#ifdef DBG_MSG
    dbg_printf("write_chunk 1: offset %d start_of_payload %p *bin_rx_len %d", app->offset, start_of_payload, *bin_rx_len);
#endif
    status = wiced_framework_app_write_chunk(app, (const uint8_t *)start_of_payload, *bin_rx_len);  // Offset increments, and is maintained inside app
    if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("wiced_framework_app_write_chunk(1) returned %d", status);
#endif
        goto cleanup;
    }

    // Now loop over the rest of the chain
    // Note: available is what's left in the chain *including* the buffer we just got.
    // Recommend never designing an interface like this, check out BSD mbufs for a more sane alternative.

    // TODO: remaining_bytes_in_chain + offset = init available. Can prob. eliminate one of them.
    int remaining_bytes_in_chain = available - rxd_this_frag;
    unsigned offset = rxd_this_frag;
    while (remaining_bytes_in_chain > 0) {
        wiced_packet_get_data(p, offset, (uint8_t **)&rx_buf, &rxd_this_frag, &available);

        *bin_rx_len += rxd_this_frag;

#ifdef DBG_MSG
    	dbg_printf("write_chunk 2: offset %d start_of_payload %p *bin_rx_len %d", app->offset, start_of_payload, *bin_rx_len);
#endif
        status = wiced_framework_app_write_chunk(app, (const uint8_t *)rx_buf, rxd_this_frag); // Again, offset increments automatically
        if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	    dbg_printf("wiced_framework_app_write_chunk(2) returned %d", status);
#endif
            goto cleanup;
	}
        remaining_bytes_in_chain = available - rxd_this_frag;
        offset += rxd_this_frag;
    }
    status = WICED_SUCCESS;
cleanup:
    return status;
}

/**
 * @brief Receive all of one HTTP reply
 *
 * In our application, a HTTP reply comprises one chunk to be written.
 * @param[in] s connected socket to read
 * @param[in,out] app handle to app storage in flash to overwrite
 * @param[out] bin_rx_len bytes read *excluding* HTTP headers. Note that in case of errors this is set to the number of bytes written to flash.
 * @return WICED_SUCCESS if everything worked, an error code otherwise
 * @see CHUNK_SIZE
 */
static wiced_result_t
receive_http_reply(wiced_tcp_socket_t *s, wiced_app_t *app, size_t *bin_rx_len)
{
    wiced_result_t status = WICED_ERROR;
    uint16_t one_rx_len;
    wiced_packet_t *reply_packet = NULL;
    size_t content_length;

    *bin_rx_len = 0;

    // TODO: correct size for last chunk, not CHUNK_SIZE?
    status = wiced_tcp_receive( s, &reply_packet, CHUNK_SIZE );
    if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("wiced_tcp_receive 1 returned %d", status);
#endif
        goto cleanup;
    }
    status = write_mbuf_chain_to_flash(reply_packet, &one_rx_len, app, /* first_mbuf_chain = */ true, &content_length);
    if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("write_mbuf_chain_to_flash returned %d", status);
#endif
        goto cleanup;
    }
    *bin_rx_len = one_rx_len;

    wiced_packet_delete( reply_packet );
    reply_packet = NULL;

    int len_remaining = content_length - one_rx_len;
    while (len_remaining > 0) {
        status = wiced_tcp_receive(s, &reply_packet, len_remaining);
        if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	    dbg_printf("wiced_tcp_receive 2 returned %d", status);
#endif
            goto cleanup;
	}
        status = write_mbuf_chain_to_flash(reply_packet, &one_rx_len, app, /* first_mbuf_chain = */ false, NULL);
        if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	    dbg_printf("write_mbuf_chain_to_flash returned %d", status);
#endif
            goto cleanup;
	}
        *bin_rx_len += one_rx_len;

        wiced_packet_delete( reply_packet );
        reply_packet = NULL;

        len_remaining -= one_rx_len;
    }
cleanup:
    if (reply_packet)
        wiced_packet_delete( reply_packet );
    return status;
}
#endif

int get_length(char * buf, uint32_t buf_len, uint32_t * p_len)
{
    char * p_ascii = 0;
    int status = -WM_FAIL;
    *p_len = 0;
    

    p_ascii = strstr(buf, "Content-Length: ");

    if(p_ascii) {
        p_ascii += 16; // length of "Content-Length: "
        *p_len = atoi(p_ascii);
        status = WM_SUCCESS;
    }

    return status;
}

#if 0
static wiced_result_t
send_one_head_request(struct http_pipeline_context *pipe_ctx, char *tx_buf, uint32_t* p_len)
{
    wiced_result_t status = WICED_ERROR;
    wiced_packet_t *reply_packet = NULL;

    status = wiced_tcp_send_buffer( &(pipe_ctx->socket), tx_buf, (uint16_t) strlen( tx_buf ) );

    if(status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("wiced_tcp_send_buffer returned %d", status);
#endif
        goto cleanup;
    }

    status = wiced_tcp_receive( &(pipe_ctx->socket), &reply_packet, BUF_SIZE );
    if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("wiced_tcp_receive returned %d", status);
#endif
        goto cleanup;
    }

    uint16_t available;
    uint16_t datalen;
    uint8_t * data;
    wiced_packet_get_data(reply_packet, 0, &data, &datalen, &available);
    if(datalen > BUF_SIZE) {
        datalen = BUF_SIZE;
    }
    memcpy(buf, data, datalen);

    status = get_length(buf, buflen, p_len);
    if(status != WICED_SUCCESS) {
#ifdef DBG_MSG
        dbg_printf("get_length() call failed");
#endif
        goto cleanup;
    }

    status = WICED_SUCCESS;

cleanup:
    wiced_packet_delete( reply_packet );
    reply_packet = NULL;
    return status;
}
/**
 * @brief Send one request to the connected server
 *
 * @param[in] pipe_ctx previously filled-in struct from `open_http_pipeline`
 * @param[in] tx_buf request to send to server
 * @param[in,out] rx_buf buffer to put response in. Must be at least 32k.
 * @param[in] rx_len length of rx_buf
 * @see open_http_pipeline
 */
static wiced_result_t
send_one_request(struct http_pipeline_context *pipe_ctx, char *tx_buf, size_t* bin_rx_len, wiced_app_t *app)
{
    wiced_result_t status = WICED_ERROR;

    wiced_tcp_send_buffer( &(pipe_ctx->socket), tx_buf, (uint16_t) strlen( tx_buf ) );

    // parse.com has no problem serving us the full CHUNK_SIZE. However, the WICED
    // software introduces two layers of looping:
    //
    // 1. wiced_tcp_receive returns many times while fetching one HTTP "Content-Length"
    // 2. wiced_tcp_receive returns an mbuf *chain*, although I couldn't find any docs
    //    on it.

    status = receive_http_reply(&(pipe_ctx->socket), app, bin_rx_len);
    if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
	dbg_printf("receive_http_reply returned %d", status);
#endif
        goto cleanup;
    }
    status = WICED_SUCCESS;
cleanup:
    return status;
}

/*
 * wiced_https_get isn't an appropriate basis for this function.
 * The response buffer is only 5000 bytes long, and it won't do pipelining.
 * At the same time, this algorithm is inspired by wiced_https_get.
 */
wiced_result_t
sw_mgr_ota_client(char *filename, uint32_t app_size)
{
        wiced_app_t app;
        struct http_pipeline_context pipe_ctx;
        static char pad[PAD_MAX];
	wiced_result_t status;

#ifdef DBG_MSG
        dbg_puts(__func__);
        dbg_puts(filename);
#endif
        open_http_pipeline(&upd_ip_address, sw_upd_svr, &pipe_ctx);

        memcpy(filename_placeholder, filename, strlen(filename));

        memcpy(head_filename_placeholder, filename, strlen(filename));

        unsigned offset = 0;
        bool is_valid_app = false;

        status = send_one_head_request(&pipe_ctx, head_request, &app_size);
        if(status != WICED_SUCCESS) {
#ifdef DBG_MSG
            dbg_printf("HTTP HEAD returned %d", status);
#endif
            goto cleanup;
        }

#ifdef DBG_MSG
        dbg_printf("OTA file size = %d", app_size);
#endif

        if ((status = open_app(app_size, &app)) != WICED_SUCCESS)
                goto cleanup;
        is_valid_app = true;
        while (offset < app_size) {
                sprintf(pad, "%06d", offset);
                memcpy(bytes_from_placeholder, pad, 6);

                sprintf(pad, "%06d", offset + CHUNK_SIZE - 1);
                    // -1 because HTTP ranges are inclusive

                memcpy(bytes_to_placeholder, pad, 6);
                size_t chunk_bin_rx_len = 0;
                status = send_one_request(&pipe_ctx, get_request, &chunk_bin_rx_len, &app);
                if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
                        dbg_printf("HTTP GET 1 returned %d, retrying once", status);
#endif
			close_http_pipeline(&pipe_ctx);
			open_http_pipeline(&upd_ip_address, sw_upd_svr, &pipe_ctx);
			offset += chunk_bin_rx_len;
			sprintf(pad, "%06d", offset);
                	memcpy(bytes_from_placeholder, pad, 6);
                	sprintf(pad, "%06d", offset + CHUNK_SIZE - 1);
                	memcpy(bytes_to_placeholder, pad, 6);
			chunk_bin_rx_len = 0;
			status = send_one_request(&pipe_ctx, get_request, &chunk_bin_rx_len, &app);
			if (status != WICED_SUCCESS) {
#ifdef DBG_MSG
                        	dbg_printf("HTTP GET 2 returned %d, giving up", status);
#endif
                        	goto cleanup;
			}
                }
#ifdef DBG_MSG
                dbg_puts("HTTP GET succeeded");
#endif
                bytes_fetched += chunk_bin_rx_len;

                // Request will be updated next time around loop
                offset += chunk_bin_rx_len;
        }
cleanup:
        if (is_valid_app) {
                close_app(&app, offset >= app_size);
        }
        close_http_pipeline(&pipe_ctx);
	return status;
}
#endif
int
sw_mgr_ota_client(char *filename, uint32_t app_size)
{
  return 0;
}

