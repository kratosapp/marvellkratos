/** @file
 * @brief Entry point for Broadcom application.
 */

#include <stdlib.h>
#include "wifi_drv_p.h"
#include "wifi_drv.h"
#include "settings_mgr.h"
#include "sw_mgr.h"
#include "app_framework.h"
#include "wm_os.h"
#include "main.h"
#include "mdev_aes.h"
#include "wm-tls.h"
#include "partition.h"
//#include "wm_aes.h"

os_semaphore_t wlan_init_done, normal_connected, normal_user_disconnect, tls_certtest_done, tls_post_done, dbg_printf_sema;

static int
event_handler(int event, void *data)
{
  int retcode = WM_SUCCESS;
  char ip[16];

	switch (event) {
  case AF_EVT_INIT_DONE:
    dbg_puts("AF_EVT_INIT_DONE");
    /* The WM is up and all basic modules have been intialised,
       perform any notification cleanup/inits if required.
       The structure app_init_state is passed as data with this event.
     */
    break;

	case AF_EVT_WLAN_INIT_DONE:
    dbg_puts("AF_EVT_WLAN_INIT_DONE");
    /* Safe now to set country code */
		os_semaphore_put(&wlan_init_done);
		break;

  case AF_EVT_NORMAL_INIT:
    dbg_puts("AF_EVT_NORMAL_INIT");
    break;

  case AF_EVT_NORMAL_CONNECTING:
    dbg_puts("Connecting");
   break;

	case AF_EVT_NORMAL_CONNECTED:
    app_network_ip_get(ip);
    dbg_printf("Connected to home network with IP address = %s", ip);
		os_semaphore_put(&normal_connected);
		break;

  case AF_EVT_NORMAL_CONNECT_FAILED:
    dbg_puts("Connect Attempt Failed");
    break;

  case AF_EVT_NORMAL_LINK_LOST:
    dbg_puts("Link Lost");
    break;

  case AF_EVT_NORMAL_CHAN_SWITCH:
    dbg_puts("Chan Switch");
    break;

  case AF_EVT_NORMAL_WPS_DISCONNECT:
    dbg_puts("WPS Disconnect");
    break;

	case AF_EVT_NORMAL_USER_DISCONNECT:
    dbg_puts("User Disconnect");
		os_semaphore_put(&normal_user_disconnect);
		break;

  case AF_EVT_NW_SET:
    dbg_puts("Network settings updated");
    break;

  case AF_EVT_NORMAL_DHCP_RENEW:
    dbg_puts("DHCP Lease Renew");
    break;

  case AF_EVT_PROV_DONE:
    dbg_puts("Provisioning is complete");
    break;

	default:
		dbg_printf("%s: unhandled event %d\n", __func__, event);
	}

  return retcode;
}

//extern mdev_t *uart_dev;

void
main(void)
{
  int ret;
  int status;

 	wifi_drv_uart_init();

  /* Initialise the partition module */
  ret = part_init();
  if(ret != 0) {
    dbg_printf("Failed to initialise partition");
  }

	/*
	 * Initialise the Wifi device 
	 */
	status = os_semaphore_create(&wlan_init_done, "wlan_init_done");
	status += os_semaphore_create(&normal_connected, "normal_connected");
	status += os_semaphore_create(&normal_user_disconnect, "normal_user_disconnect");
  status += os_semaphore_create(&tls_certtest_done, "tls_certtest_done");
  status += os_semaphore_create(&tls_post_done, "tls_post_done");
  status += os_semaphore_create(&dbg_printf_sema, "dbg_printf_sema");

  if(status != WM_SUCCESS) {
    dbg_printf("Problem at init time");
  }

  os_semaphore_get(&wlan_init_done, OS_WAIT_FOREVER);
  os_semaphore_get(&normal_connected, OS_WAIT_FOREVER);
  os_semaphore_get(&normal_user_disconnect, OS_WAIT_FOREVER);
  os_semaphore_get(&tls_certtest_done, OS_WAIT_FOREVER);
  os_semaphore_get(&tls_post_done, OS_WAIT_FOREVER);

	app_framework_start(event_handler);

  /* Should be able to get until event handler has put */
	os_semaphore_get(&wlan_init_done, OS_WAIT_FOREVER);

  prov_fail_init();

  ret = aes_drv_init();
  if(ret != WM_SUCCESS) {
    dbg_printf("failed to init aes\r\n");
  }

  ret = tls_lib_init();
  if(ret != WM_SUCCESS) {
    dbg_printf("failed to init tls\r\n");
  }


#ifndef PREPROVISIONED
	if(is_provisioned())
#endif
	{
            sw_mgr_init();
	    wifi_drv_config();
            sw_mgr_config();
	}

	wifi_drv(NULL);
}
