#include <stdio.h>
#include <stdbool.h>
#include <string.h>
#include <stdint.h>
#include <wm_os.h>

#include "dbg.h"

#define DBG_RESP_LEN (5)

#define DBG_MSG      (11)

#define DBG_PAD_SIZE (133)

#define DBG_TX_BUFSIZE (256)

#define NLCR "\r\n"

typedef struct dbg_resp {
  uint32_t type;
  uint32_t payload;
} dbg_resp_t;

static bool dbg_msgs_on = false;

static os_semaphore_t dbg_print_sem;

static mdev_t * dbg_uart_dev;

static uint8_t dbg_tx_buf[DBG_TX_BUFSIZE];

void dbg_init(mdev_t * new_uart_dev)
{
  int sem_ret = os_semaphore_create(&dbg_print_sem, "dbg_print_sem");
  if(WM_SUCCESS == sem_ret) {
    dbg_uart_dev = new_uart_dev;
    dbg_msgs_on = true;
    memset(dbg_tx_buf, 0, DBG_TX_BUFSIZE);
  }
}

static inline void write_unaligned_uint32(uint32_t val, uint8_t * buf)
{
  buf[0] = val & 0x000000ff;
  buf[1] = (val & 0x0000ff00) >> 8;
  buf[2] = (val & 0x00ff0000) >> 16;
  buf[3] = (val & 0xff000000) >> 24;
}

static size_t dbg_marshal(dbg_resp_t resp, unsigned char * buf)
{
  size_t msg_len = strlen((char *)resp.payload);
  if(msg_len > 236) {
    msg_len = 236;
  }

  write_unaligned_uint32(msg_len + 1, buf + 1);
  memcpy(buf + DBG_RESP_LEN, (char *) resp.payload, msg_len);
  buf[DBG_RESP_LEN + msg_len] = '\x00';
  return DBG_RESP_LEN + msg_len + 1;
}

static void dbg_send(uint8_t * buf, size_t buf_len)
{
   uart_drv_write(dbg_uart_dev, buf, buf_len);
   uart_drv_write(dbg_uart_dev, (uint8_t*)NLCR, 4);
}

void dbg_puts(const char *msg)
{
  if (!dbg_msgs_on)
     return;
  dbg_resp_t resp;

  resp.type = DBG_MSG;
  resp.payload = (uintptr_t) msg;
  size_t  tx_buf_len = dbg_marshal(resp, dbg_tx_buf);

  dbg_send(dbg_tx_buf, tx_buf_len);
}

void
dbg_printf(const char *fmt, ...)
{
  va_list arg;
  static char pad[DBG_PAD_SIZE];

  os_semaphore_get(&dbg_print_sem, OS_WAIT_FOREVER);

  va_start(arg, fmt);
  vsnprintf(pad, DBG_PAD_SIZE, fmt, arg);
  va_end(arg);
  dbg_puts(pad);

  os_semaphore_put(&dbg_print_sem);
}

