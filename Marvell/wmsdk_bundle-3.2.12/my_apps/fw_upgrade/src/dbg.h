#ifndef DBG_H
#define DBG_H

void dbg_init(mdev_t * uart_dev);

void dbg_puts(const char *msg);
void dbg_printf(const char *fmt, ...);


#endif
