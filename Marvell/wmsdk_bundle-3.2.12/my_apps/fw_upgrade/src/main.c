/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/* Application demonstrating secure firmware upgrades
 *
 * Summary:
 *
 * This application showcases the ED25519-Chacha20 and RSA-AES
 * firmware upgrade support in http server mode as well as http
 * client mode.
 *
 * Description:
 *
 * In order to use firmware upgrades, the device first needs to be provisioned.
 * If the device boots up in unconfigured mode, it starts the micro-AP with
 * an SSID of the form "fwupg_demo-xxxx" and a default passphrase of
 * "marvellwm"
 *
 * The device then starts the web-server which will listen to incoming http
 * connections (e.g, from a browser).
 *
 * Device can be accessed at URI http://192.168.10.1
 * Through the Web UI, user can provision the device to an Access Point.
 * Once the device is provisioned successfully, the IP address will be
 * printed on the console. This can be then used to demonstrate firmware
 * upgrade.
 *
 * By default, ED25519-Chacha20 based upgrades are enabled. This can be
 * changed to RSA-AES by changing the config option in
 * fw_upgrade_demo/config.mk. You will also have to enable the support
 * in WMSDK by setting CONFIG_FWUPG_RSA_AES=y in your .config or
 * enabling it using make menuconfig and selecting it under
 * Modules -> Firmware Upgrades
 *
 * Two upgrade APIs are exposed by this app.
 * 1. Client mode firmware upgrades
 * curl -d '{"url":"<link/to/fw.bin.upg"}' http://<ip>/fw_url
 * - The secure upgrade image is fetched from the remote server based on
 *   the url.
 *
 * 2. Server mode firmware upgrades
 * curl -F 'file=@fw.bin.upg' http://<ip>/fw_data
 * - The secure upgrade image is POSTed directly to the device
 *
 * For details on steps to generate the secure upgrade image, refer to the
 * README.ed_chacha and README.rsa_aesctr files in
 * wmsdk/tools/src/fw_generator.
 * The config files to be used for fw_generator have been added as
 * fwupg.ed_chacha.config and fwupg.rsa_aes.config in this app's root folder.
 *
 * New configurations should be generated and used for production samples.
 *
 */
#include <wm_os.h>
#include <app_framework.h>
//#include <appln_cb.h>
//#include <appln_dbg.h>
//#include <cli.h>
//#include <wmstdio.h>
//#include <wm_net.h>
//#include <httpd.h>
//#include <board.h>
//#include <wmtime.h>
//#include <psm.h>
#include <ftfs.h>
#include <rfget.h>

#include "dbg.h"

/*-----------------------Global declarations----------------------*/
static os_thread_stack_define(uart_rx_stack, 1024);
static os_thread_stack_define(ota_stack, 8192);

#include <fw_upgrade_ed_chacha.h>
const uint8_t fwupg_verification_key[] =
    "785904c6b0f0f6b5c27bc7f976ae2b564107de28106d36b4002eea9484d2d534";
const uint8_t fwupg_encrypt_decrypt_key[] =
    "5c588367ff30c9ee54f8b7066db32b2ae69693343ca9c696f549506d66bf8682";

char app_fw_url[] =  "http://muppetlabs.emdalo.com/~dmcnamara/marvell_ota/kratos_app.upg.bin";
char wifi_fw_url[] = "http://muppetlabs.emdalo.com/~dmcnamara/marvell_ota/kratos_wifi.upg.bin";
char st_fw_url[] =   "http://muppetlabs.emdalo.com/~dmcnamara/marvell_ota/kratos_st.upg.bin";


int ftfs_api_version = 100;
char *ftfs_part_name = "ftfs";


static void app_reboot_cb()
{
	ll_printf("Rebooting...");
	pm_reboot_soc();
}

static int reboot_device()
{
	static os_timer_t app_reboot_timer;
	if (!app_reboot_timer) {
		if (os_timer_create(&app_reboot_timer, "app_reboot_timer",
					os_msec_to_ticks(2000),
					app_reboot_cb, NULL,
					OS_TIMER_ONE_SHOT,
					OS_TIMER_AUTO_ACTIVATE)
				!= WM_SUCCESS)
			return -WM_FAIL;

	}
	return WM_SUCCESS;
}


int client_mode_upgrade_fw(char *url, enum flash_comp flash_comp)
{
	dbg_printf("Using ED-Chacha client mode upgrade");
	uint8_t decrypt_key[ED_CHACHA_DECRYPT_KEY_LEN];
	uint8_t verification_key[ED_CHACHA_VERIFICATION_KEY_LEN];
	hex2bin(fwupg_verification_key, verification_key,
			sizeof(verification_key));
	hex2bin(fwupg_encrypt_decrypt_key, decrypt_key,
			sizeof(decrypt_key));

  switch(flash_comp)
  {
    case FC_COMP_FW:
      dbg_printf("Upgrading FW from %s", url);
	    return ed_chacha_client_mode_upgrade_fw(FC_COMP_FW,
			  url, verification_key, decrypt_key);
    break;

    case FC_COMP_BOOT2:
      dbg_printf("Upgrading bootloader from %s", url);
	    return ed_chacha_client_mode_upgrade_fw(FC_COMP_BOOT2,
			  url, verification_key, decrypt_key);
    break;

    case FC_COMP_WLAN_FW:
      dbg_printf("Upgrading WLAN FW from %s", url);
	    return ed_chacha_client_mode_upgrade_fw(FC_COMP_WLAN_FW,
			  url, verification_key, decrypt_key);
    break;

    case FC_COMP_FTFS:
      dbg_printf("Upgrading Filesystem from %s", url);
	    return ed_chacha_client_mode_upgrade_fw(FC_COMP_FTFS,
			  url, verification_key, decrypt_key);
    break;


    default:
      dbg_printf("Can't upgrade unknown component: %d", flash_comp);
      return WM_FAIL;
    break;
  }
}



/* This function is defined for handling critical error.
 * For this application, we just stall and do nothing when
 * a critical error occurs.
 *
 */
void appln_critical_error_handler(void *data)
{
	while (1)
		;
	/* do nothing -- stall */
}


/*-----------------------Local declarations----------------------*/
static int provisioned;

/*
 * Event: INIT_DONE
 *
 * The application framework is initialized.
 *
 * The data field has information passed by boot2 bootloader
 * when it loaded the application.
 *
 * ?? What happens if app is loaded via jtag
 */
static void event_init_done(void *data)
{
#if APPCONFIG_DEBUG_ENABLE
	struct app_init_state *state;
	state = (struct app_init_state *)data;
#endif /* APPCONFIG_DEBUG_ENABLE */

	dbg_printf("Event: INIT_DONE");
	dbg_printf("Factory reset bit status: %d", state->factory_reset);
	dbg_printf("Booting from backup firmware status: %d", state->backup_fw);
	dbg_printf("Previous reboot cause: %u", state->rst_cause);
}

/*
 * Handler invoked on WLAN_INIT_DONE event.
 *
 * When WLAN is started, the application framework looks to
 * see whether a home network information is configured
 * and stored in PSM (persistent storage module).
 *
 * The data field returns whether a home network is provisioned
 * or not, which is used to determine what network interfaces
 * to start (station, micro-ap, or both).
 *
 * If provisioned, the station interface of the device is
 * connected to the configured network.
 *
 * Else, Micro-AP network is configured.
 *
 * (If desired, the Micro-AP network can also be started
 * along with the station interface.)
 *
 * We also start all the services which don't need to be
 * restarted between provisioned and non-provisioned mode
 * or between connected and disconnected state.
 *
 * Accordingly:
 *      -- Start mDNS and advertize services
 *	-- Start HTTP Server
 *	-- Register WSGI handlers for HTTP server
 */
static struct wlan_network net;

static void event_wlan_init_done(void *data)
{
	/* We receive provisioning status in data */
	provisioned = (int)data;

	dbg_printf("Event: WLAN_INIT_DONE provisioned=%d", provisioned);

	if (provisioned) {
		app_sta_start();
	} else {
		//app_uap_start_with_dhcp(appln_cfg.ssid, appln_cfg.passphrase);
    strcpy(net.security.psk, "AbbeyHouse");
    net.security.psk_len = strlen("AbbeyHouse");
    net.role = WLAN_BSS_ROLE_STA;
    strcpy(net.name, "Station");
    memset(net.bssid, 0, 6);
    net.channel = 0; // any
    net.security.type = WLAN_SECURITY_WPA2;
    net.ip.ipv4.addr_type = ADDR_TYPE_DHCP;
    net.type = WLAN_BSS_ROLE_STA;
    app_sta_start_by_network(&net);    

	}
}

/*
 * Event: Micro-AP Started
 *
 * If we are not provisioned, then start provisioning on
 * the Micro-AP network.
 *
 * Also, enable WPS.
 *
 * Since Micro-AP interface is UP, announce mDNS service
 * on the Micro-AP interface.
 */
static void event_uap_started(void *data)
{

	dbg_printf("Event: Micro-AP Started");
}


/*
 * Event: PROV_DONE
 *
 * Provisioning is complete. We can stop the provisioning
 * service.
 *
 * Stop WPS.
 *
 * Enable Reset to Prov Button.
 */
static void event_prov_done(void *data)
{
	dbg_printf("Provisioning successful");
}

/* Event: PROV_CLIENT_DONE
 *
 * Provisioning Client has terminated session.
 *
 * We can now safely stop the Micro-AP network.
 *
 * Note: It is possible to keep the Micro-AP network alive even
 * when the provisioning client is done.
 */
static void event_prov_client_done(void *data)
{
  dbg_printf("Stopped Micro-AP");
}

/*
 * Event UAP_STOPPED
 *
 * Normally, we will get here when provisioning is complete,
 * and the Micro-AP network is brought down.
 *
 * If we are connected to an AP, we can enable IEEE Power Save
 * mode here.
 */
static void event_uap_stopped(void *data)
{
	dbg_printf("Event: Micro-AP Stopped");
}

/*
 * Event: CONNECTING
 *
 * We are attempting to connect to the Home Network
 *
 * Note: We can come here:
 *
 *   1. After boot -- if already provisioned.
 *   2. After provisioning
 *   3. After link loss
 *
 * This is just a transient state as we will either get
 * CONNECTED or have a CONNECTION/AUTH Failure.
 *
 */
static void event_normal_connecting(void *data)
{
	dbg_printf("Connecting to Home Network");
}

/* Event: AF_EVT_NORMAL_CONNECTED
 *
 * Station interface connected to home AP.
 *
 * Network dependent services can be started here. Note that these
 * services need to be stopped on disconnection and
 * reset-to-provisioning event.
 */
static void event_normal_connected(void *data)
{
	char ip[16];

	app_network_ip_get(ip);
	dbg_printf("Connected to Home Network with IP address = %s", ip);
}

/*
 * Event: CONNECT_FAILED
 *
 * We attempted to connect to the Home AP, but the connection did
 * not succeed.
 *
 * This typically indicates:
 *
 * -- Authentication failed.
 * -- The access point could not be found.
 * -- We did not get a valid IP address from the AP
 *
 */
static void event_connect_failed(void *data)
{
	char failure_reason[32];

	if (*(app_conn_failure_reason_t *)data == AUTH_FAILED)
		strcpy(failure_reason, "Authentication failure");
	if (*(app_conn_failure_reason_t *)data == NETWORK_NOT_FOUND)
		strcpy(failure_reason, "Network not found");
	if (*(app_conn_failure_reason_t *)data == DHCP_FAILED)
		strcpy(failure_reason, "DHCP failure");

	os_thread_sleep(os_msec_to_ticks(2000));
	dbg_printf("Application Error: Connection Failed: %s", failure_reason);
}

/*
 * Event: USER_DISCONNECT
 *
 * This means that the application has explicitly requested a network
 * disconnect
 *
 */
static void event_normal_user_disconnect(void *data)
{
	dbg_printf("User disconnect");
}

/*
 * Event: LINK LOSS
 *
 * We lost connection to the AP.
 *
 * The App Framework will attempt to reconnect. We dont
 * need to do anything here.
 */
static void event_normal_link_lost(void *data)
{
	dbg_printf("Link Lost");
}

static void event_normal_reset_prov(void *data)
{
  dbg_printf("Reset to provisioning");
	/* Reset to provisioning */
	provisioned = 0;
}

/* This is the main event handler for this project. The application framework
 * calls this function in response to the various events in the system.
 */
int common_event_handler(int event, void *data)
{
	switch (event) {
	case AF_EVT_INIT_DONE:
		event_init_done(data);
		break;
	case AF_EVT_WLAN_INIT_DONE:
		event_wlan_init_done(data);
		break;
	case AF_EVT_NORMAL_CONNECTING:
		event_normal_connecting(data);
		break;
	case AF_EVT_NORMAL_CONNECTED:
		event_normal_connected(data);
		break;
	case AF_EVT_NORMAL_CONNECT_FAILED:
		event_connect_failed(data);
		break;
	case AF_EVT_NORMAL_LINK_LOST:
		event_normal_link_lost(data);
		break;
	case AF_EVT_NORMAL_USER_DISCONNECT:
		event_normal_user_disconnect(data);
		break;
	case AF_EVT_NORMAL_RESET_PROV:
		event_normal_reset_prov(data);
		break;
	case AF_EVT_UAP_STARTED:
		event_uap_started(data);
		break;
	case AF_EVT_UAP_STOPPED:
		event_uap_stopped(data);
		break;
	case AF_EVT_PROV_DONE:
		event_prov_done(data);
		break;
	case AF_EVT_PROV_CLIENT_DONE:
		event_prov_client_done(data);
		break;
	default:
		break;
	}

	return 0;
}

bool upgrade_app_fw = false;
bool upgrade_wifi_fw = false;
bool upgrade_st_fw = false;

mdev_t * uart_dev;

static void uart_read_task(os_thread_arg_t data)
{
  uint8_t read_buf[1];

  while(1) {
    uart_drv_read(uart_dev, read_buf, 1);
    switch(read_buf[0])
    {
      case 'f':
        upgrade_app_fw = 1;
      break;

      case 'w':
        upgrade_wifi_fw = 1;
      break;

      case 's':
        upgrade_st_fw = 1;
      break;
    }
  }
  os_thread_sleep(os_msec_to_ticks(rand() % 10));
}



static void ota_task(os_thread_arg_t data)
{
  int ret;

  while(1)
  {
    if(upgrade_app_fw) {
	    ret = client_mode_upgrade_fw(app_fw_url, FC_COMP_FW);
      upgrade_app_fw = 0;
      if(ret == WM_SUCCESS) {
        dbg_printf("Upgrade succeeded.  Rebooting ...");
	      reboot_device();
      } 
      else {
        dbg_printf("Upgrade failed: Error Code: %d", ret);
      }
    }
    else if(upgrade_wifi_fw) {
	    ret = client_mode_upgrade_fw(wifi_fw_url, FC_COMP_WLAN_FW);
      upgrade_wifi_fw = 0;
      if(ret == WM_SUCCESS) {
        dbg_printf("Upgrade succeeded.  Rebooting ...");
	      reboot_device();
      } 
      else {
        dbg_printf("Upgrade failed: Error Code: %d", ret);
      }
    }
    else if(upgrade_st_fw) {
	    ret = client_mode_upgrade_fw(st_fw_url, FC_COMP_FTFS);
      upgrade_st_fw = 0;
      if(ret == WM_SUCCESS) {
        dbg_printf("Upgrade succeeded.  Rebooting ...");
	      reboot_device();
      } 
      else {
        dbg_printf("Upgrade failed: Error Code: %d", ret);
      }
    }

    os_thread_sleep(os_msec_to_ticks(rand() % 10));
  }
}

static void modules_init()
{
	int ret;

	ret = wmstdio_init(UART1_ID, 0);
  wmprintf("You are connected to UART1, connect to UART0 instead\r\n");

  ret = uart_drv_init(UART0_ID, UART_8BIT);

  ret = uart_drv_blocking_read(UART0_ID, true);

  uart_dev = uart_drv_open(UART0_ID, 115200);

  dbg_init(uart_dev);

	/*
	 * Initialize Power Management Subsystem
	 */
	ret = pm_init();
	if (ret != WM_SUCCESS) {
		dbg_printf("Error: pm_init failed");
		appln_critical_error_handler((void *) -WM_FAIL);
	}
 

	return;
}

static struct fs *fs = 0;
/* Flag to indicate failure of the file system */
static uint8_t fs_failed = 0;
static struct ftfs_super ftfs;
static struct partition_entry *ftfs_passive;

struct partition_entry *fs_get_passive(void)
{
  return ftfs_passive;
}

int main()
{

#if 0
  short history = 0;
  struct partition_entry *f1, *f2;
#endif

	modules_init();

  part_init();
#if 0

  f1 = part_get_layout_by_id(FC_COMP_WLAN_FW, &history);
  f2 = part_get_layout_by_id(FC_COMP_WLAN_FW, &history);

  part_set_active_partition(f1);
#endif

  //wmprintf("Build Time: " __DATE__ " " __TIME__ "\r\n");

	dbg_printf("Build Time: " __DATE__ " " __TIME__ "");

	/* Start the application framework */
	if (app_framework_start(common_event_handler) != WM_SUCCESS) {
		dbg_printf("Failed to start application framework");
		appln_critical_error_handler((void *) -WM_FAIL);
	}

  os_thread_create(NULL, "uart_rx", uart_read_task, NULL, &uart_rx_stack, OS_PRIO_2);
  os_thread_create(NULL, "ota", ota_task, NULL, &ota_stack, OS_PRIO_2);

  fs = rfget_ftfs_init(&ftfs, 100, "ftfs", &ftfs_passive);
  if(!fs) {
    dbg_printf("fs: Failed to init FTFS");
    fs_failed = 1;
  }

  uint32_t addr = ftfs.active_addr + sizeof(FT_HEADER);
  struct ft_entry entry;
  file * f;
  unsigned int len;
  #define BUF_SIZE (16)
  char buf[BUF_SIZE + 1];
  do {
    flash_drv_read(ftfs.dev, (uint8_t *)&entry, sizeof(entry), addr);

    if(entry.name[0] != '\0') {
        dbg_printf("\"%s\" (%d bytes)", entry.name, entry.length);

        f = fs->fopen(fs, entry.name, "r");
        if(!f) {
          dbg_printf("failed to open \"%s\"", entry.name);
        }

        fs->fseek(f, 0, SEEK_END);
        dbg_printf("opened \"%s\", length: %d bytes", entry.name, (int)fs->ftell(f));
        fs->fseek(f, 0, SEEK_SET);

        if(0 == strcmp(entry.name, "st_image.bin")) {
          while((len = fs->fread(buf, BUF_SIZE, 1, f)) > 0) {
            buf[len] = '\0';
            dbg_printf("%s", buf);
          }
        }

        addr += sizeof(entry);
    }

  } while(entry.name[0] != '\0');


	return 0;
}
