# Copyright (C) 2008-2013 Marvell International Ltd.
# All Rights Reserved.
#
# Common makefile for using CMSIS DSP Library

# FLOAT_ABI can be soft, softfp or hard. Refer to GCC manual for details
# Applicable to CM4 only
FLOAT_ABI = softfp

# Compiler Include Flags for library headers
CFLAGS += -I $(SDK_PATH)/incl/cmsis

ifeq ($(CONFIG_CPU_MC200), y)
CFLAGS += -DARM_MATH_CM3
STARTGRP_WITH_LIBS += -lm
endif

ifeq ($(CONFIG_CPU_MW300), y)
CFLAGS += -DARM_MATH_CM4
STARTGRP_WITH_LIBS += -lm

ifneq ($(FLOAT_ABI), soft)
CFLAGS += -D__FPU_PRESENT=1 -mfpu=fpv4-sp-d16 -mfloat-abi=$(FLOAT_ABI)
STARTGRP_WITH_LIBS += -mfpu=fpv4-sp-d16 -mfloat-abi=softfp
endif
endif
