/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */
#include <openssl/ssl.h>
#include <wmtypes.h>
#include <wmstdio.h>
#include <wmtime.h>
#include <string.h>
#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <httpc.h>
#include <cli.h>
#include <cli_utils.h>
#include <cyassl/ctaocrypt/settings.h>
#include <appln_dbg.h>
#include <wlan.h>
#include <wm-tls.h>
#include <appln_dbg.h>
#include <mdev_aes.h>
#include <jsonv2.h>
#include <httpc.h>
#include <wmstdio.h>
#define MAX_DOWNLOAD_DATA 1024
#define THREAD_COUNT 1
#define STACK_SIZE 8000

static const char *url = "http://api.evrythng.com/time";
static os_thread_stack_define(test_thread_stack, STACK_SIZE);
static os_thread_t test_thread_handle[THREAD_COUNT];
static http_session_t handle;
static http_resp_t *http_resp;

extern void print_data(http_session_t l_handle, const char *l_url);
/* Establish HTTP connection */
static void httpc_connect()
{
	wmprintf("Connecting to : %s \r\n", url);
	int rv = httpc_get(url, &handle, &http_resp, NULL);
	if (rv != WM_SUCCESS) {
		wmprintf("%s httpc_get: failed\r\n", url);
		return;
	}
	wmprintf("%s (%x): Status code: %d\r\n", url,
		 os_get_current_task_handle(),
		 http_resp->status_code);
}

/* HTTP get function */
static void httpc_get_url(void *arg)
{
	int index = (int)arg;
	httpc_connect();
	print_data(handle, url);
	test_thread_handle[index] = NULL;
	os_thread_delete(NULL);
}

/* cli function */
static void cmd_httpc_get(int argc, char *argv[])
{
	char name_buf[20];
	int index = 0;
	snprintf(name_buf, sizeof(name_buf), "Thread-%d", index);
	os_thread_create(&test_thread_handle[index],
				 name_buf, httpc_get_url,
				 (void *) index,
				 &test_thread_stack, OS_PRIO_3);
}

/* httpc secure get cli */
static struct cli_command httpc_get_cmds[] = {
	{"httpc-get", NULL, cmd_httpc_get}
};

/* cli_init function (registration of cli)*/
int httpc_get_cli_init(void)
{
		if (cli_register_commands(&httpc_get_cmds[0],
						sizeof(httpc_get_cmds) /
						sizeof(struct cli_command))) {
				wmprintf("Unable to register commands\r\n");
		}
	return 0;
}
