/*
 *  Copyright (C) 2008-2015 Marvell International Ltd.
 *  All Rights Reserved.
 */
/*
 * OS demo
 *
 * Summary:
 * This demo is present to give developers a ready reference of using
 * various OS abstraction layer APIs. It uses a semaphore to control the
 * syncronization between two functions.
 */

#include <wmstdio.h>
#include <wm_os.h>

int semaphore_demo(void);

/*
 * The application entry point is main(). At this point, minimal
 * initialization of the hardware is done, and also the RTOS is
 * initialized.
 */
int main(void)
{
	/* Initialize console on uart0 */
	wmstdio_init(UART0_ID, 0);
	wmprintf("Semaphore demo started\r\n");
	semaphore_demo();
	return 0;
}
