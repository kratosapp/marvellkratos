/*  Copyright 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */
#ifndef __PSM_INTERNAL_V2_H__
#define __PSM_INTERNAL_V2_H__

#if defined(__linux__) || defined(__PSM_UTIL__)
#include <pthread.h>
#include <stdbool.h>
#else
#include <wm_os.h>
#endif /* __linux__ */

#include <psm-v2.h>

#if defined(BUILD_MINGW) && defined(__PSM_UTIL__)
size_t strnlen(const char *s, size_t maxlen);
#endif

#if defined(__linux__) || defined(__PSM_UTIL__)
#define mdev_t void

#define os_mem_alloc malloc
#define os_mem_realloc realloc
#define os_mem_free free
static void *os_mem_calloc(int size)
{
	void *ptr = malloc(size);
	if (!ptr)
		return NULL;

	memset(ptr, 0x00, size);
	return ptr;
}

#endif /* __linux__ */

#define OBJECT_TYPE_INVALID (object_type_t)0x0000
#define OBJECT_TYPE_SMALL_BINARY (object_type_t)0xAA55
#define OBJECT_TYPE_SMALL_BINARY_MAX_SIZE (uint16_t)(~0)

/*
 * Terminology:
 *
 * offset: Offset from the assigned flash block
 * address: Address of the flash device
 * bit clear: value is '1'
 * bit set: value is '0'
 * compaction: remove all inactive variables from flash.
 * indexing: Save the offset of active objects in RAM for direct access.
 */

/**
 * Defines the single read size during verification. Bigger size will speed
 * up verification but may cause stack overflows depending on application
 * stack usage pattern.
 */
#define READ_BUFFER_SIZE 64
#define PSM_INVALID_OBJ_ID 0xFFFF
#define PSM_DATA_SWAP_DEMARCATION 4
#define CHECKPOINT_MARKER 0x55AA
#define PSM_MAX_OBJNAME_LEN 256

/*
 * This is soft limit. Is defined to optimize memory usage considering
 * real world scenario. Can be increased upto what is supported by flash
 * object object-id data type.
 */
#define PSM_MAX_OBJECTS 512
#define PSM_DEFAULT_INDEX_ELEMENTS 10

/**
 * fixme: This value needs to be asked from flash driver. Fix this later
 * when appropriate API is added in flash driver.
 */
#define PSM_FLASH_SECTOR_SIZE 4096

enum {
	PSM_OBJ_INACTIVE = 0x01,
	PSM_OBJ_CACHE_REQ = 0x02,
	PSM_OBJ_INDEX_REQ = 0x04,
};

enum {
	PSM_WRITE_NO_VERIFY = false,
	PSM_WRITE_VERIFY = true
};

typedef uint16_t objectid_t;
typedef uint16_t object_type_t;

PACK_START typedef struct {
	uint8_t name_len;
	uint16_t data_len;
	/* If secure PSM is enabled, the name variable will be encrypted. */
	/* void *name */
	/* If secure PSM is enabled, the data buffer will be in decrypted
	   state */
	/* void *data */
} PACK_END psm_cache_t;

typedef struct psm_index_s {
	uint32_t name_hash;
	uint16_t obj_id;
	uint32_t flash_offset;
	psm_cache_t *cache;
} psm_index_t;

/* Every PSM object has this structure on the flash */
PACK_START typedef struct {
	object_type_t object_type;
	/** Flags:
	 * [7-3]: Reserved
	 * [2]  : Index request
	 * [1]  : Cache request
	 * [0]  : Active status
	 */
	uint8_t flags;
	/** CRC32 of entire psm object including metadata */
	uint32_t crc32;
	/** Length of the data field */
	uint16_t data_len;

	/** Object ID: A unique id for the object name ASCII string */
	objectid_t obj_id;
	/** Length of object name. If this is zero the object has been
	 * seen before and obj_id->name mapping is present. X-Ref: macro
	 * PSM_MAX_OBJNAME_LEN
	 */
	uint8_t name_len;
	/** Variable length object name string. */
	/* uint8_t name[<name_len>]; */
	/** Binary data here. Length == psm_object.data_len member above */
	/* uint8_t bin_data[<len>]; */
} PACK_END psm_object_t;

PACK_START typedef struct {
	uint32_t swap_start_offset;
	uint32_t swap_start_offset_inv;
	uint16_t checkpoint_marker;
	uint16_t checkpoint_marker_inv;
} PACK_END psm_swap_desc_t;

typedef struct {
#if defined(__linux__) || defined(__PSM_UTIL__)
	pthread_rwlock_t prwlock;
#else
	os_rw_lock_t prwlock;
#endif /* __linux__ */

	uint32_t total_active_object_size;
	uint32_t object_offset_limit;
	uint32_t data_free_offset;
	/*
	 * We do not re-use object ID's of inactivated objects. It is
	 * expected that number of used object ID will never actually reach
	 * the highest number we support. After erase object ID's are
	 * re-programmed.
	 */
	objectid_t highest_used_obj_id;
	uint32_t index_max_elements;
	uint32_t index_elements;
	psm_index_t *index;
	psm_cfg_t psm_cfg;

	/* Flash driver handle */
	mdev_t *fl_dev;
	uint32_t block_size;
	uint32_t block_cnt;
	uint32_t part_size;
	uint32_t start_address;
#ifdef CONFIG_SECURE_PSM
	psm_sec_hnd_t psm_sec_hnd;
	uint8_t psm_enc_buf[READ_BUFFER_SIZE];
	/* This will hold encrypted copy of the name for the whole time
	   the object is open in write mode. For read mode it is valid
	   only for some time during open */
	uint8_t psm_name_enc_buf[PSM_MAX_OBJNAME_LEN];
#endif
} psm_t;

/* Ephemeral RAM structure corresponding to PSM object in flash */
typedef struct psm_objinfo_s {
	psm_t *psm;
	psm_object_t pobj;
	/* Start of object offset */
	uint32_t object_flash_offset;
	/* Will move according to user operations on the object */
	uint32_t curr_flash_offset;
	psm_mode_t mode;
	/* Communicated by caller in psm_object_open(). */
	uint32_t max_data_len;
	/** CRC32 calculated on the fly in read mode */
	uint32_t crc32;
	bool old_active_object_present;
	/* Pointer to old object structure, if any */
	uint32_t old_object_flash_offset;
	uint32_t old_object_size;

	psm_index_t *index;
} psm_objinfo_t;

#define block_size(__psm__) (__psm__->block_size)
#define block_cnt(__psm__) (__psm__->block_cnt)
#define part_size(__psm__) (__psm__->part_size)
#define start_address(__psm__) (__psm__->start_address)
#define fldev(__psm__)  (__psm__->fl_dev)

#define offset_to_address(__psm__, __offset__)	\
	(__psm__->start_address + __offset__)

#define align_to_block_offset(__psm__, __offset__)	\
	(__offset__ & ~(__psm__->block_size - 1))

#define is_offset_block_aligned(__psm__, __offset__)	\
	(!(__offset__ & (__psm__->block_size - 1)))

#define object_size(__pobj__)						\
	(sizeof(psm_object_t) + (__pobj__)->name_len + (__pobj__)->data_len)

#define psm_is_data_len_sane(__psm__, __len__)			\
	((__len__ < OBJECT_TYPE_SMALL_BINARY_MAX_SIZE) &&	\
	 (__len__ < (part_size(psm)/2)))

#define metadata_size(__pobj__)				\
	(sizeof(psm_object_t) + (__pobj__)->name_len)

#define is_object_active(__pobj__)		\
	((__pobj__)->flags & PSM_OBJ_INACTIVE)
#define is_object_indexable(__pobj__)			\
	(!((__pobj__)->flags & PSM_OBJ_INDEX_REQ))
#define is_object_cacheable(__pobj__)			\
	(!((__pobj__)->flags & PSM_OBJ_CACHE_REQ))

#define remaining_part_space(__psm__) (part_size(psm) - data_free_offset)

#define part_encrypted(__psm__) (__psm__->psm_cfg.secure)

int resetkey(psm_t *psm, psm_resetkey_mode_t mode);
int encrypt_buf(psm_t *psm, const void* plain, void *cipher, int len);
int decrypt_buf(psm_t *psm, const void *cipher, void *plain, int len);
bool psm_is_free_space(const psm_object_t *pobj);
int psm_rwlock_part(psm_t *psm, psm_mode_t mode);
void psm_rwunlock_part(psm_t *psm, psm_mode_t mode);
int psm_read_flash(psm_t *psm, void *buffer, uint32_t size, uint32_t offset);
#endif /* __PSM_INTERNAL_V2_H__ */
