/*  Copyright 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdint.h>
#include <ctype.h>
#include <wmlist.h>
#include <wmerrno.h>
#include <wm_utils.h>

#if defined(__linux__) || defined(__PSM_UTIL__)
#include <pthread.h>
#include <stdbool.h>
#else
#include <wm_os.h>
#endif /* __linux__ */

#include <psm-v2.h>
#include "psm-internal.h"
#include "psm-v2-secure.h"
#include "psm-internal-v2.h"


static void dump_object(psm_t *psm, psm_object_t *pobj,
			uint32_t object_offset)
{
	uint8_t buf[READ_BUFFER_SIZE + 1];
	uint32_t read_size, remaining;
	int index, rv;
	uint32_t name_offset = object_offset + sizeof(psm_object_t);

#ifdef CONFIG_SECURE_PSM
	if (part_encrypted(psm)) {
		rv = resetkey(psm, SET_DECRYPTION_KEY);
		if (rv)
			return;
	}
#endif

	/* Print the name field */
	for (index = 0; index < pobj->name_len; index += read_size) {
		remaining = pobj->name_len - index;
		read_size = remaining > READ_BUFFER_SIZE ?
			READ_BUFFER_SIZE : remaining;
		rv = psm_read_flash(psm, buf, read_size, name_offset + index);
		if (rv != WM_SUCCESS) {
			psm_d("Failed to read name field @ 0x%x", name_offset);
			return;
		}

		buf[read_size] = 0;
#ifdef CONFIG_SECURE_PSM
		if (part_encrypted(psm)) {
			rv = decrypt_buf(psm, buf, buf, read_size);
			if (rv)
				return;
		}

		if (part_encrypted(psm))
			wmprintf("(enc) ");
#endif
		int cnt;
		const int width = 32;
		if (isprint(buf[0]))
			wmprintf("%s", buf);
		else {
			for (cnt = 1; cnt <= pobj->name_len; cnt++) {
				wmprintf(" %x", buf[cnt]);
				if (!(cnt % width))
					wmprintf("\r\n");
			}
		}
	}

	wmprintf(" = ");

	if (!psm_is_data_len_sane(psm, pobj->data_len)) {
		wmprintf("<invalid length of data: %d>\r\n", pobj->data_len);
		return;
	}

	uint32_t data_offset = object_offset + metadata_size(pobj);
	/* Print the data field. Complete data field is not printed */
	read_size = pobj->data_len > READ_BUFFER_SIZE ?
		READ_BUFFER_SIZE : pobj->data_len;
	rv = psm_read_flash(psm, buf, read_size, data_offset);
	if (rv != WM_SUCCESS) {
		psm_d("dump: could not read object data @ %x", data_offset);
		return;
	}

#ifdef CONFIG_SECURE_PSM
		if (part_encrypted(psm)) {
			rv = decrypt_buf(psm, buf, buf, read_size);
			if (rv)
				return;
		}
#endif

	bool binary_data = false;
	for (index = 0; index < read_size; index++) {
		if (!isprint(buf[index])) {
			wmprintf("<binary data> ");
			binary_data = true;
			break;
		}
	}

	for (index = 0; index < read_size;) {
		if (binary_data) {
			wmprintf("%x ", buf[index++]);
			if (!(index % 8) && (index != read_size))
				wmprintf("\r\n");
		} else {
			wmprintf("%c", buf[index++]);
		}
	}

	if (read_size < pobj->data_len) {
		wmprintf("..(contd. total %d bytes)", pobj->data_len);
	}

	wmprintf("\r\n");
}

static void _psm_objects_dump(psm_t *psm)
{
	psm_entry_i();

	uint32_t offset = 0;

	/*
	 * Don't try reading flash if remaining size is less than
	 * necessary.
	 */
	uint32_t limit = psm->object_offset_limit;
	psm_object_t pobj;
	uint32_t dumped_object_count = 0;

#ifdef CONFIG_SECURE_PSM
	if (part_encrypted(psm))
		wmprintf("Note: Entries prepended with \"(enc)\" "
			 "are encrypted\n\r");
#endif

	while (offset < limit) {
		int rv = psm_read_flash(psm, &pobj, sizeof(psm_object_t),
					offset);
		if (rv != WM_SUCCESS) {
			psm_d("dump: could not read object @ off: %x", offset);
			return;
		}

		if (psm_is_free_space(&pobj)) {
			psm_d("Object dump done");
			break;
		}

		if (pobj.object_type != OBJECT_TYPE_SMALL_BINARY) {
			psm_d("Invalid object at the end of data area");
			break;
		}

		if (!is_object_active(&pobj)) {
			offset += object_size(&pobj);
			continue;
		}

		if (!pobj.name_len) {
			psm_d("Zero name length objects not supported");
			offset += object_size(&pobj);
			continue;
		}

		dump_object(psm, &pobj, offset);
		offset += object_size(&pobj);
		dumped_object_count++;
	}

	if (!dumped_object_count)
		wmprintf("No objects found\r\n");
}

void psm_objects_dump(psm_hnd_t phandle)
{
	psm_entry();
	if (!phandle)
		return;

	psm_t *psm = (psm_t *) phandle;

	psm_rwlock_part(psm, PSM_MODE_READ);
	_psm_objects_dump(psm);
	psm_rwunlock_part(psm, PSM_MODE_READ);
}
