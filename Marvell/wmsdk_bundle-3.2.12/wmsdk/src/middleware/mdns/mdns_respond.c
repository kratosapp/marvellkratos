/*
 * Copyright 2008-2013 cozybit Inc.
 *
 * This file is part of libmdns.
 *
 * libmdns is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as
 * published by the Free Software Foundation, either version 3 of
 * the License, or (at your option) any later version.
 *
 * libmdns is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
 * GNU Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with libmdns. If not, see <http://www.gnu.org/licenses/>.
 *
 */
/*
 *  Copyright (C) 2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * All functions in this file are implementation of mDNS APIs exposed to the
 * application. All functions sends control message to mDNS responder thread.
 */
#include <errno.h>
#include <mdns.h>
#include <mdns_port.h>
#include "mdns_private.h"


/* Function first validates the service and then sends control message to
 * mdns responder thread for announcing service on the specified n/w interface.
 */
int mdns_announce_service(struct mdns_service *service, void *iface)
{
	int ret;
	struct mdns_service *service_arr[2];
	mdns_ctrl_data msg;

	if (service == NULL)
		return -WM_E_MDNS_INVAL;

	service_arr[0] = service;
	service_arr[1] = NULL;

	ret = mdns_verify_service(service_arr, iface);
	if (ret != WM_SUCCESS)
		return ret;

	msg.cmd = MDNS_CTRL_ANNOUNCE_SERVICE;
	msg.iface = iface;
	msg.service = service;
	ret = mdns_send_ctrl_iface_msg((int *)&msg, MDNS_CTRL_RESPONDER,
			sizeof(mdns_ctrl_data));

	return ret;
}

/* Function sends control message to mdns responder thread for deannouncing
 * service over the given n/w interface. */
int mdns_deannounce_service(struct mdns_service *service, void *iface)
{
	int ret;
	mdns_ctrl_data msg;

	if (service == NULL)
		return -WM_E_MDNS_INVAL;

	msg.cmd = MDNS_CTRL_DEANNOUNCE_SERVICE;
	msg.iface = iface;
	msg.service = service;
	ret = mdns_send_ctrl_iface_msg((int *)&msg, MDNS_CTRL_RESPONDER,
		sizeof(mdns_ctrl_data));

	return ret;
}

/* Function validates set of mdns services and sends a control message to mdns
 * responder thread to announce all services simultaneously on the specified
 * n/w interface. */
int mdns_announce_service_arr(struct mdns_service *services[], void *iface)
{
	int ret;
	mdns_ctrl_data msg;

	ret = mdns_verify_service(services, iface);
	if (ret != WM_SUCCESS)
		return ret;

	msg.cmd = MDNS_CTRL_ANNOUNCE_SERVICE_ARR;
	msg.iface = iface;
	msg.service = services;
	ret = mdns_send_ctrl_iface_msg((int *)&msg, MDNS_CTRL_RESPONDER,
			sizeof(mdns_ctrl_data));
	return ret;
}

/* Function sends out a control message to mdns responder thread for
 * deannouncing given set of services on the specified n/w interface. */
int mdns_deannounce_service_arr(struct mdns_service *services[], void *iface)
{
	int ret;
	mdns_ctrl_data msg;

	msg.cmd = MDNS_CTRL_DEANNOUNCE_SERVICE_ARR;
	msg.iface = iface;
	msg.service = services;
	ret = mdns_send_ctrl_iface_msg((int *)&msg, MDNS_CTRL_RESPONDER,
		sizeof(mdns_ctrl_data));
	return ret;
}

/* Function sends a control message to mdns responder thread to deannounce all
 * mdns services previously announced on a specified n/w interface. */
int mdns_deannounce_service_all(void *iface)
{
	int ret;
	mdns_ctrl_data msg;

	if (iface == NULL)
		return -WM_E_MDNS_INVAL;

	msg.cmd = MDNS_CTRL_DEANNOUNE_ALL;
	msg.iface = iface;
	msg.service = NULL;
	ret = mdns_send_ctrl_iface_msg((int *)&msg, MDNS_CTRL_RESPONDER,
		sizeof(mdns_ctrl_data));
	return ret;
}

