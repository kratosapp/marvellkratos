
/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/** dhcp-server-main.c: CLI based APIs for the DHCP Server
 */
#include <string.h>

#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <cli.h>
#include <cli_utils.h>
#include <dhcp-server.h>

#include "dhcp-priv.h"

/*
 * Command-Line Interface
 */

#define DHCPS_COMMAND	"dhcp-server"
#define DHCPS_HELP	"<mlan0/uap0/wfd0> <start|stop>"
#define WAIT_FOR_UAP_START	5
extern uint32_t dhcp_address_timeout;
extern struct dhcp_server_data dhcps;
void dhcp_stat(int argc, char **argv)
{
	int i = 0;
	wmprintf("DHCP Server Lease Duration : %d seconds\r\n",
		 (int)dhcp_address_timeout);
	if (dhcps.count_clients == 0) {
		wmprintf("No IP-MAC mapping stored\r\n");
	} else {
		wmprintf("Client IP\tClient MAC\r\n");
		for (i = 0; i < dhcps.count_clients && i < MAC_IP_CACHE_SIZE;
		     i++) {
			wmprintf("%s\t%02X:%02X:%02X:%02X:%02X:%02X\r\n",
				 inet_ntoa(dhcps.ip_mac_mapping[i].client_ip),
				 dhcps.ip_mac_mapping[i].client_mac[0],
				 dhcps.ip_mac_mapping[i].client_mac[1],
				 dhcps.ip_mac_mapping[i].client_mac[2],
				 dhcps.ip_mac_mapping[i].client_mac[3],
				 dhcps.ip_mac_mapping[i].client_mac[4],
				 dhcps.ip_mac_mapping[i].client_mac[5]);
		}
	}
}

static void dhcp_cli(int argc, char **argv)
{
	void *intrfc_handle = NULL;
	int wait_for_sec = WAIT_FOR_UAP_START;

	if (argc >= 3 && string_equal(argv[2], "start")) {
		if (!strncmp(argv[1], "mlan0", 5))
			intrfc_handle = net_get_mlan_handle();
		else if (!strncmp(argv[1], "uap0", 4)) {
			while (wait_for_sec) {
				wait_for_sec--;
				if (!is_uap_started()) {
					if (wait_for_sec == 0) {
						dhcp_e("unable"
						" to start DHCP server. Retry"
						" after uAP is started");
						return;
					}
				} else
					break;
				os_thread_sleep(os_msec_to_ticks(1000));
			}
			intrfc_handle = net_get_uap_handle();
		}

		if (dhcp_server_start(intrfc_handle))
			dhcp_e("unable to "
				       "start DHCP server");
	} else if (argc >= 3 && string_equal(argv[2], "stop"))
		dhcp_server_stop();
	else {
		wmprintf("Usage: %s %s\r\n", DHCPS_COMMAND, DHCPS_HELP);
		wmprintf("Error: invalid argument\r\n");
	}
}

static struct cli_command dhcp_cmds[] = {
	{DHCPS_COMMAND, DHCPS_HELP, dhcp_cli},
	{"dhcp-stat", NULL, dhcp_stat},
#ifdef CONFIG_DHCP_SERVER_TESTS
	{"dhcp-server-tests", NULL, dhcp_server_tests},
#endif
};

int dhcpd_cli_init(void)
{
	int i;

	for (i = 0; i < sizeof(dhcp_cmds) / sizeof(struct cli_command); i++)
		if (cli_register_command(&dhcp_cmds[i]))
			return -WM_E_DHCPD_REGISTER_CMDS;

	return WM_SUCCESS;
}
