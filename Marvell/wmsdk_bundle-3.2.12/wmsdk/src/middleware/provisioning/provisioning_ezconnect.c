/*
 *  Copyright 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/** provisioning_ezconnect.c: The EZConnect provisioning state machine
 */

/* Protocol description:
 * In this provisioning method, the WLAN radio starts sniffing each channel
 * for CHANNEL_SWITCH_INTERVAL. It looks for multicast MAC frames destined to
 * different IP addresses. It attempts to extract information from the least
 * significant 23 bits of multicast destination MAC address. Each 23 bits are
 * organized in following format
 *
 *	|       7 bits         |           16 bits      |
 *	|      <metadata>      |         <data>         |
 *	| Frame Type |  Seq no |   data1    |  data0    |
 *
 * Preamble -
 * Frame Type	= 0b11110
 * Seq no	= 0 - 2
 * data		= preamble bytes (0, 1), (2, 3), (4, 5) in (data0, data1)
 *
 * SSID -
 * Frame Type   = 0b10
 * Seq no       = 0
 * data		= (len_ssid, len_ssid) in (data0, data1)
 * Seq no       = 1 - 2
 * data		= CRC of the SSID byte by byte
 * Seq no       = 3 onwards
 * data		= SSID byte by byte (LSB in data0)
 *
 * Passphrasse - Encrypted or plaintext passphrase
 * Frame Type	= 0b0
 * Seq no       = 0
 * data		= (len_pass, len_pass) in (data0, data1)
 * Seq no       = 1 - 2
 * data		= CRC of the Passphrase byte by byte
 * Seq no	= 3 onwards
 * data		= Encrypted or plaintext passphrase byte by byte (LSB in data0)
 *
 */

#include <stdio.h>
#include <string.h>

#include <wmtypes.h>
#include <wmstdio.h>
#include <wm_os.h>
#include <wm_net.h>
#include <provisioning.h>
#include <app_framework.h>
#include <mdev_aes.h>
#include <wlan_smc.h>
#include <work-queue.h>
#include <system-work-queue.h>
#include "provisioning_int.h"

#define SSID_MAX_LENGTH                 33

extern struct prov_gdata prov_g;

struct ezconn_state {
	uint8_t state;
	uint8_t substate;
	uint8_t pass_len;
	uint8_t ssid_len;
	char cipher_pass[WLAN_PSK_MAX_LENGTH];
	char plain_pass[WLAN_PSK_MAX_LENGTH];
	uint32_t crc_ssid;
	uint32_t crc_passphrase;
	uint8_t BSSID[6];
	uint8_t source[6];
	char ssid[SSID_MAX_LENGTH];
	int8_t security;
};

static struct ezconn_state *es, *es1;

typedef struct {
	uint8_t bssid[MLAN_MAC_ADDR_LENGTH];
	uint8_t channel;
	uint8_t dtim_count;
	uint8_t dtim_period;
} network_info_t;

static network_info_t *beacon_info;
/* Maximum number of beacon information cached */
#define MAX_BEACON_COUNT 40
/* In beacon frame, we get 2 channels. First one indicates the channel on which
 * beacon is sniffed and second one is the actual channel on which beacon frame
 * was sent. TLV with type 3 gives actual channel */
#define BEACON_CHANNEL_ELE_ID 3
/* In beacon frame, TLV with type 5 gives DTIM information */
#define BEACON_DTIM_ELE_ID 5

/* Number of beacon information cached */
static int count_beacon_info;

/* Channel information of home network */
static uint8_t network_channel;

/* After successful provisioning, below pointer contains correct state
   structure */
struct ezconn_state *g_esp;

/* In SMC mode based EZConnect provisioning support, start uAP on receiving
 * AUTH request or directed probe request or start EZConnect state machine on
 * receiving multicast packets */
enum packet_type {
	NULL_TYPE = 0,
	EZ_MULTICAST,
	SMC_AUTH,
	SMC_PROBE_REQ,
};

static enum packet_type received_packet_type;

/* preamble as EZPR22 */
static char preamble[] = {0x45, 0x5a, 0x50, 0x52, 0x32, 0x32};

struct wlan_network uap_network;

uint8_t ezconn_device_key[16];
static uint8_t ezconn_device_key_len;

static uint8_t iv[16];

static os_semaphore_t smc_cs_sem;

#define DEFAULT_SCAN_PROTECTION_TIMEOUT (30 * 1000)
extern os_semaphore_t prov_scan_protection_sem;
extern int prov_handle_scan_results(unsigned int count);

static network_info_t *lookup_mac_beacon_info(const char *bssid)
{
	int i;
	for (i = 0; i < count_beacon_info; i++) {
		if (!memcmp(beacon_info[i].bssid, bssid,
			    MLAN_MAC_ADDR_LENGTH)) {
			return &beacon_info[i];
		}
	}
	return NULL;
}

static int ezconn_parse_beacon_frame(const wlan_frame_t *frame,
				     const uint16_t len)
{
	if (count_beacon_info == MAX_BEACON_COUNT)
		return -WM_FAIL;

	network_info_t *beacon = lookup_mac_beacon_info(
		frame->frame_data.data_info.bssid);
	if (beacon != NULL)
		return WM_SUCCESS;

	const char *frame_traversal = frame->frame_data.beacon_info.ssid +
		frame->frame_data.beacon_info.ssid_len;

	memcpy(beacon_info[count_beacon_info].bssid,
	       frame->frame_data.data_info.bssid, MLAN_MAC_ADDR_LENGTH);

	while ((frame_traversal - (char *)frame) < len) {
		switch (*frame_traversal) {
		case BEACON_CHANNEL_ELE_ID:
			beacon_info[count_beacon_info].channel =
				*(frame_traversal + 2);
			break;
		case BEACON_DTIM_ELE_ID:
			beacon_info[count_beacon_info].dtim_count =
				*(frame_traversal + 2);
			beacon_info[count_beacon_info].dtim_period =
				*(frame_traversal + 3);
			break;
		}
		frame_traversal += *(frame_traversal + 1) + 2;
	}
	if (beacon_info[count_beacon_info].channel != 0)
		count_beacon_info++;
	return WM_SUCCESS;
}

#define DECRYPT(key, keylen, iv, cipher, plain, size)		\
	{							\
		aes_t enc_aes;					\
		mdev_t *aes_dev;				\
		aes_dev = aes_drv_open("MDEV_AES_0");		\
		aes_drv_setkey(aes_dev, &enc_aes, key, keylen,	\
			       iv, AES_DECRYPTION,		\
			       HW_AES_MODE_CBC);		\
		aes_drv_encrypt(aes_dev, &enc_aes,		\
				(uint8_t *) cipher,		\
				(uint8_t *) plain, size);	\
		aes_drv_close(aes_dev);				\
	}


int prov_ezconn_set_device_key(uint8_t *key, int len)
{
	if (len != 16) {
		prov_e("Invalid provisioning key length");
		return -WM_FAIL;
	}

	memcpy(ezconn_device_key, key, len);
	ezconn_device_key_len = len;
	return WM_SUCCESS;
}

void prov_ezconn_unset_device_key(void)
{
	memset(ezconn_device_key, 0, sizeof(ezconn_device_key));
	ezconn_device_key_len = 0;
	return;
}

static uint32_t crc32_tab[] = {
	0x00000000, 0x77073096, 0xee0e612c, 0x990951ba, 0x076dc419, 0x706af48f,
	0xe963a535, 0x9e6495a3,	0x0edb8832, 0x79dcb8a4, 0xe0d5e91e, 0x97d2d988,
	0x09b64c2b, 0x7eb17cbd, 0xe7b82d07, 0x90bf1d91, 0x1db71064, 0x6ab020f2,
	0xf3b97148, 0x84be41de,	0x1adad47d, 0x6ddde4eb, 0xf4d4b551, 0x83d385c7,
	0x136c9856, 0x646ba8c0, 0xfd62f97a, 0x8a65c9ec,	0x14015c4f, 0x63066cd9,
	0xfa0f3d63, 0x8d080df5,	0x3b6e20c8, 0x4c69105e, 0xd56041e4, 0xa2677172,
	0x3c03e4d1, 0x4b04d447, 0xd20d85fd, 0xa50ab56b, 0x35b5a8fa, 0x42b2986c,
	0xdbbbc9d6, 0xacbcf940,	0x32d86ce3, 0x45df5c75, 0xdcd60dcf, 0xabd13d59,
	0x26d930ac, 0x51de003a, 0xc8d75180, 0xbfd06116, 0x21b4f4b5, 0x56b3c423,
	0xcfba9599, 0xb8bda50f, 0x2802b89e, 0x5f058808, 0xc60cd9b2, 0xb10be924,
	0x2f6f7c87, 0x58684c11, 0xc1611dab, 0xb6662d3d, 0x76dc4190, 0x01db7106,
	0x98d220bc, 0xefd5102a, 0x71b18589, 0x06b6b51f, 0x9fbfe4a5, 0xe8b8d433,
	0x7807c9a2, 0x0f00f934, 0x9609a88e, 0xe10e9818, 0x7f6a0dbb, 0x086d3d2d,
	0x91646c97, 0xe6635c01, 0x6b6b51f4, 0x1c6c6162, 0x856530d8, 0xf262004e,
	0x6c0695ed, 0x1b01a57b, 0x8208f4c1, 0xf50fc457, 0x65b0d9c6, 0x12b7e950,
	0x8bbeb8ea, 0xfcb9887c, 0x62dd1ddf, 0x15da2d49, 0x8cd37cf3, 0xfbd44c65,
	0x4db26158, 0x3ab551ce, 0xa3bc0074, 0xd4bb30e2, 0x4adfa541, 0x3dd895d7,
	0xa4d1c46d, 0xd3d6f4fb, 0x4369e96a, 0x346ed9fc, 0xad678846, 0xda60b8d0,
	0x44042d73, 0x33031de5, 0xaa0a4c5f, 0xdd0d7cc9, 0x5005713c, 0x270241aa,
	0xbe0b1010, 0xc90c2086, 0x5768b525, 0x206f85b3, 0xb966d409, 0xce61e49f,
	0x5edef90e, 0x29d9c998, 0xb0d09822, 0xc7d7a8b4, 0x59b33d17, 0x2eb40d81,
	0xb7bd5c3b, 0xc0ba6cad, 0xedb88320, 0x9abfb3b6, 0x03b6e20c, 0x74b1d29a,
	0xead54739, 0x9dd277af, 0x04db2615, 0x73dc1683, 0xe3630b12, 0x94643b84,
	0x0d6d6a3e, 0x7a6a5aa8, 0xe40ecf0b, 0x9309ff9d, 0x0a00ae27, 0x7d079eb1,
	0xf00f9344, 0x8708a3d2, 0x1e01f268, 0x6906c2fe, 0xf762575d, 0x806567cb,
	0x196c3671, 0x6e6b06e7, 0xfed41b76, 0x89d32be0, 0x10da7a5a, 0x67dd4acc,
	0xf9b9df6f, 0x8ebeeff9, 0x17b7be43, 0x60b08ed5, 0xd6d6a3e8, 0xa1d1937e,
	0x38d8c2c4, 0x4fdff252, 0xd1bb67f1, 0xa6bc5767, 0x3fb506dd, 0x48b2364b,
	0xd80d2bda, 0xaf0a1b4c, 0x36034af6, 0x41047a60, 0xdf60efc3, 0xa867df55,
	0x316e8eef, 0x4669be79, 0xcb61b38c, 0xbc66831a, 0x256fd2a0, 0x5268e236,
	0xcc0c7795, 0xbb0b4703, 0x220216b9, 0x5505262f, 0xc5ba3bbe, 0xb2bd0b28,
	0x2bb45a92, 0x5cb36a04, 0xc2d7ffa7, 0xb5d0cf31, 0x2cd99e8b, 0x5bdeae1d,
	0x9b64c2b0, 0xec63f226, 0x756aa39c, 0x026d930a, 0x9c0906a9, 0xeb0e363f,
	0x72076785, 0x05005713, 0x95bf4a82, 0xe2b87a14, 0x7bb12bae, 0x0cb61b38,
	0x92d28e9b, 0xe5d5be0d, 0x7cdcefb7, 0x0bdbdf21,	0x86d3d2d4, 0xf1d4e242,
	0x68ddb3f8, 0x1fda836e, 0x81be16cd, 0xf6b9265b, 0x6fb077e1, 0x18b74777,
	0x88085ae6, 0xff0f6a70,	0x66063bca, 0x11010b5c, 0x8f659eff, 0xf862ae69,
	0x616bffd3, 0x166ccf45,	0xa00ae278, 0xd70dd2ee, 0x4e048354, 0x3903b3c2,
	0xa7672661, 0xd06016f7, 0x4969474d, 0x3e6e77db,	0xaed16a4a, 0xd9d65adc,
	0x40df0b66, 0x37d83bf0,	0xa9bcae53, 0xdebb9ec5, 0x47b2cf7f, 0x30b5ffe9,
	0xbdbdf21c, 0xcabac28a,	0x53b39330, 0x24b4a3a6,	0xbad03605, 0xcdd70693,
	0x54de5729, 0x23d967bf, 0xb3667a2e, 0xc4614ab8, 0x5d681b02, 0x2a6f2b94,
	0xb40bbe37, 0xc30c8ea1, 0x5a05df1b, 0x2d02ef8d
};

static uint32_t ezconn_crc32(uint32_t crc, const void *buf, size_t size)
{
	const uint8_t *p;

	p = buf;
	crc = crc ^ ~0U;

	while (size--)
		crc = crc32_tab[(crc ^ *p++) & 0xFF] ^ (crc >> 8);

	return crc ^ ~0U;
}


static inline int ezconn_match_crc(struct ezconn_state *esp)
{
	if (esp->crc_passphrase != ezconn_crc32(0, esp->plain_pass,
						esp->pass_len))
		return -WM_FAIL;
	if (esp->crc_ssid != ezconn_crc32(0, esp->ssid, esp->ssid_len))
		return -WM_FAIL;
	return WM_SUCCESS;
}

/* Masking for frame type bitwise x11111xx */
#define SM_S0_FRAME_TYPE_MASKING 0x7c
/* Preamble frame type bitwise x11110xx */
#define SM_S0_FRAME_TYPE_PREAMBLE 0x78
/* Masking for substate bitwise xxxxxx11 */
#define SM_S0_SUBSTATE_MASKING 0x03

static int ezconnect_sm_s0(const char *src, const char *dest,
			   const char *bssid, const uint16_t len,
			   struct ezconn_state *esp)
{
	if (((dest[3] & SM_S0_FRAME_TYPE_MASKING) == SM_S0_FRAME_TYPE_PREAMBLE)
	    && ((dest[3] & SM_S0_SUBSTATE_MASKING) == esp->substate)
	    && (dest[4] == preamble[esp->substate * 2 + 1])
	    && (dest[5] == preamble[esp->substate * 2])) {
		if (esp->substate == 0) {
			memcpy(&esp->BSSID, bssid, 6);
			memcpy(&esp->source, src, 6);
		} else if (esp->substate == 2) {
			esp->state = 1;
			esp->substate = 0;
			return WM_SUCCESS;
		}
		esp->substate++;
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}

/* Substate for ssid length */
#define SM_SUBSTATE_SSID_LENGTH 0x00
/* Substate for crc */
#define SM_SUBSTATE_CRC(subs) ((subs == 1 || subs == 2) ? true : false)

/* Masking for frame type bitwise x11xxxxx */
#define SM_S1_FRAME_TYPE_MASKING 0x60
/* Preamble frame type bitwise x10xxxxx */
#define SM_S1_FRAME_TYPE_PREAMBLE 0x40
/* Masking for substate bitwise xxx11111 */
#define SM_S1_SUBSTATE_MASKING 0x1f

static int ezconnect_sm_s1(const char *src, const char *dest,
			   const char *bssid, const uint16_t len,
			   struct ezconn_state *esp)
{
	if (((dest[3] & SM_S1_FRAME_TYPE_MASKING) == SM_S1_FRAME_TYPE_PREAMBLE)
	    && ((dest[3] & SM_S1_SUBSTATE_MASKING) == esp->substate)) {
		if (esp->substate == SM_SUBSTATE_SSID_LENGTH &&
		    (dest[4] == dest[5])) {
			esp->ssid_len = dest[4];
		} else if (((dest[3] & SM_S1_SUBSTATE_MASKING) == esp->substate)
			   && (SM_SUBSTATE_CRC(esp->substate))) {
			esp->crc_ssid |= (int)
				(dest[5] << (((esp->substate - 1) * 2) * 8));
			esp->crc_ssid |= (int)
			     (dest[4] << ((((esp->substate - 1) * 2) + 1) * 8));
		} else {
			esp->ssid[(esp->substate - 3) * 2] = dest[5];
			esp->ssid[((esp->substate - 3) * 2) + 1] = dest[4];
		}

		if ((esp->ssid_len == 0) ||
		    ((esp->substate * 2) >= (esp->ssid_len + 4))) {
			esp->substate = 0;
			esp->state = 2;
		} else {
			esp->substate++;
		}
		return WM_SUCCESS;
	}
	return -WM_FAIL;
}

/* Masking for frame type bitwise x1xxxxxx */
#define SM_S2_FRAME_TYPE_MASKING 0x40
/* Preamble frame type bitwise x0xxxxxx */
#define SM_S2_FRAME_TYPE_PREAMBLE 0x00
/* Masking for substate bitwise xx111111 */
#define SM_S2_SUBSTATE_MASKING 0x3f

static int ezconnect_sm_s2(const char *src, const char *dest,
			   const char *bssid, const uint16_t len,
			   struct ezconn_state *esp)
{
	if (((dest[3] & SM_S2_FRAME_TYPE_MASKING) == SM_S2_FRAME_TYPE_PREAMBLE)
	    && ((dest[3] & 0x3f) == esp->substate)) {
		if (esp->substate == 0 && (dest[4] == dest[5])) {
			esp->pass_len = dest[4];
		} else if (((dest[3] & SM_S2_SUBSTATE_MASKING) == esp->substate)
			   && (SM_SUBSTATE_CRC(esp->substate))) {
			esp->crc_passphrase |= (int)
				(dest[5] << (((esp->substate - 1) * 2) * 8));
			esp->crc_passphrase |= (int)
			     (dest[4] << ((((esp->substate - 1) * 2) + 1) * 8));
		} else {
			esp->cipher_pass[(esp->substate - 3) * 2] = dest[5];
			esp->cipher_pass[((esp->substate - 3) * 2) + 1] =
				dest[4];
		}
		if ((esp->pass_len == 0) ||
		    ((esp->substate * 2) >= (esp->pass_len + 4))) {
			esp->substate = 0;
			esp->state = 3;
		} else {
			esp->substate++;
		}
		if (esp->state == 3 && esp->substate == 0) {
			memcpy(&esp->BSSID, bssid, 6);
		}
		return WM_SUCCESS;
	}

	return -WM_FAIL;
}

static void ezconnect_sm(const wlan_frame_t *frame, const uint16_t len)
{
	struct ezconn_state *esp;
	const char *src, *dest, *bssid;
	uint8_t client[MLAN_MAC_ADDR_LENGTH];
	network_info_t *data_frame;
	wlan_get_mac_address(client);

	/* check if the frame is beacon frame */
	if (frame->frame_type == BEACON) {
		ezconn_parse_beacon_frame(frame, len);
		return;
	}

	if (frame->frame_type == PROBE_REQ &&
	    frame->frame_data.probe_req_info.ssid_len) {

		/* To stop smc mode and start uAP on receiving directed probe
		 * request for uAP */
		if (strncmp(frame->frame_data.probe_req_info.ssid,
			    uap_network.ssid,
			    frame->frame_data.probe_req_info.ssid_len))
			return;
		received_packet_type = SMC_PROBE_REQ;
		os_semaphore_put(&smc_cs_sem);

	} else if (frame->frame_type == AUTH &&
		   (memcmp(frame->frame_data.auth_info.dest, client, 6) == 0)) {
		received_packet_type = SMC_AUTH;
		os_semaphore_put(&smc_cs_sem);
	}

	if (frame->frame_data.data_info.frame_ctrl_flags & 0x01) {
		esp = es1;
		dest = frame->frame_data.data_info.src;
		bssid = frame->frame_data.data_info.dest;
		src = frame->frame_data.data_info.bssid;
	} else {
		esp = es;
		src = frame->frame_data.data_info.src;
		bssid = frame->frame_data.data_info.bssid;
		dest = frame->frame_data.data_info.dest;
	}

	/* We are not interested in non-multicast packets */
	if (!(dest[0] == 0x01 && dest[1] == 0x00 && dest[2] == 0x5e)) {
		return;
	}

	/* Once we get the first packet of interest, we'll ignore other
	   packets from other sources */
	if (esp->state + esp->substate > 0) {
		if (memcmp(src, esp->source, MLAN_MAC_ADDR_LENGTH) != 0)
			return;
	}

	switch (esp->state) {
	case 0:
		ezconnect_sm_s0(src, dest, bssid, len, esp);
		break;
	case 1:
		if (!network_channel) {
			data_frame = lookup_mac_beacon_info(bssid);
			if (data_frame != NULL) {
				network_channel = data_frame->channel;
				received_packet_type = EZ_MULTICAST;
				os_semaphore_put(&smc_cs_sem);
			}
		}
		ezconnect_sm_s1(src, dest, bssid, len, esp);
		break;
	case 2:
		ezconnect_sm_s2(src, dest, bssid, len, esp);
		if (esp->state == 3) {
			if (ezconn_device_key_len) {
				if (esp->pass_len % 16 != 0) {
					prov_e("Invalid passphrase length");
					esp->state = 0;
					esp->substate = 0;
					return;
				}

				DECRYPT(ezconn_device_key,
					ezconn_device_key_len, iv,
					esp->cipher_pass, esp->plain_pass,
					esp->pass_len);
				if (strlen(esp->plain_pass) < esp->pass_len) {
					esp->pass_len = strlen(esp->plain_pass);
				}
			} else {
				memcpy(esp->plain_pass, esp->cipher_pass,
				       esp->pass_len);
			}
			if (ezconn_match_crc(esp) == WM_SUCCESS) {
				received_packet_type = EZ_MULTICAST;
				os_semaphore_put(&smc_cs_sem);
			} else {
				prov_e("Checksum mismatch");
				esp->state = 0;
				esp->substate = 0;
				return;
			}
		}
		break;
	default:
		break;
	}
}

static int ezconnect_scan_cb(unsigned int count)
{
	int i;
	struct wlan_scan_result res;

	for (i = 0; i < count; i++) {
		wlan_get_scan_result(i, &res);
		if (memcmp(res.bssid, g_esp->BSSID, 6) == 0) {
			g_esp->security = WLAN_SECURITY_NONE;
			if (res.wpa && res.wpa2) {
				g_esp->security = WLAN_SECURITY_WPA_WPA2_MIXED;
			} else if (res.wpa2) {
				g_esp->security = WLAN_SECURITY_WPA2;
			} else if (res.wpa) {
				g_esp->security = WLAN_SECURITY_WPA;
			} else if (res.wep) {
				g_esp->security = WLAN_SECURITY_WEP_OPEN;
			}
			break;
		}
	}
	os_semaphore_put(&smc_cs_sem);
	return 0;
}

static wq_handle_t wq_handle;
static job_handle_t job_handle;

static void ezconn_wlan_scan()
{
	if (os_semaphore_get(&prov_scan_protection_sem,
			     os_msec_to_ticks(DEFAULT_SCAN_PROTECTION_TIMEOUT))
	    != WM_SUCCESS)
		prov_e("Scan protection timeout occured. Failed to get the scan"
		       " protection semaphore.");

	wlan_scan(prov_handle_scan_results);
}

int sys_wq_enqueue_job_wlan_scan()
{
	wq_job_t job = {
		.job_func = ezconn_wlan_scan,
		.param = NULL,
		.periodic_ms = 5000,
		.initial_delay_ms = 0,
	};
	job.owner[0] = 0;
	if (sys_work_queue_init() != WM_SUCCESS) {
		prov_e("Failed to create system work queue\r\n");
		return -WM_FAIL;
	}
	wq_handle = sys_work_queue_get_handle();
	if (!wq_handle) {
		prov_e("Failed to get system work queue "
		      "handler\r\n");
		return -WM_FAIL;
	}

	if (os_mutex_create(&survey.lock, "site-survey",
			    OS_MUTEX_INHERIT)) {
		prov_e("Failed to create site-survey semaphore\r\n");
		return -WM_FAIL;
	}

	if (os_semaphore_create(&prov_scan_protection_sem,
				"prov_scan_protection_sem")) {
		prov_e("Failed to create prov scan protection semaphore\r\n");
		return -WM_FAIL;
	}

	if (work_enqueue(wq_handle, &job, &job_handle)) {
		prov_e("Failed to enqueue wlan_scan job to "
		      "system work queue\r\n");
		return -WM_FAIL;
	}
	return WM_SUCCESS;
}

int sys_wq_dequeue_job_wlan_scan()
{
	if (os_semaphore_get(&prov_scan_protection_sem,
			     os_msec_to_ticks(DEFAULT_SCAN_PROTECTION_TIMEOUT))
	    != WM_SUCCESS)
		prov_e("Scan protection timeout occured. Failed to get the scan"
		       " protection semaphore.");

	if (wq_handle && job_handle)
		work_dequeue(wq_handle, &job_handle);
	if (survey.lock) {
		os_mutex_put(&survey.lock);
		os_mutex_delete(&survey.lock);
		survey.lock = NULL;
	}
	os_semaphore_delete(&prov_scan_protection_sem);
	return WM_SUCCESS;
}

void register_ezconnect_provisioning_ssid(char *ssid)
{
	wlan_initialize_uap_network(&uap_network);
	memcpy(uap_network.ssid, ssid, sizeof(uap_network.ssid));
}

#define PROV_EZCONNECT_LOCK "prov_ezconnect_lock"
void prov_ezconnect(os_thread_arg_t thandle)
{
	int ret;

	struct wlan_network *net;

	ret = os_semaphore_create(&smc_cs_sem, "smc_cs_sem");
	os_semaphore_get(&smc_cs_sem, OS_WAIT_FOREVER);

	beacon_info = os_mem_alloc(MAX_BEACON_COUNT * sizeof(network_info_t));
	if (!beacon_info) {
		prov_e("Failed to allocate memory.\r\n");
		return;
	}

	ret = aes_drv_init();
	if (ret != WM_SUCCESS) {
		prov_e("Unable to initialize AES engine.\r\n");
		return;
	}

	received_packet_type = NULL_TYPE;

	wakelock_get(PROV_EZCONNECT_LOCK);
	uint8_t smc_frame_filter[] = {0x02, 0x10, 0x01, 0x2c, 0x04, 0x02, 0x02,
				      0x20};
	uint8_t start_addr[] = {0x01, 0x00, 0x5e, 0x00, 0x00, 0x00};
	uint8_t end_addr[] = {0x01, 0x00, 0x5e, 0xff, 0xff, 0xff};

	smart_mode_cfg_t smart_mode_cfg;
	bzero(&smart_mode_cfg, sizeof(smart_mode_cfg_t));
	uap_network.channel = 6;
	smart_mode_cfg.beacon_period = 20;
	smart_mode_cfg.country_code = wlan_get_country();
	/* Configuring channel as auto */
	smart_mode_cfg.channel = 0;
	smart_mode_cfg.min_scan_time = 60;
	smart_mode_cfg.max_scan_time = 70;
	smart_mode_cfg.filter_type = 0x03;
	smart_mode_cfg.smc_frame_filter = smc_frame_filter;
	smart_mode_cfg.smc_frame_filter_len = sizeof(smc_frame_filter);
	smart_mode_cfg.custom_ie_len = 0;

	memcpy(smart_mode_cfg.smc_start_addr, start_addr, sizeof(start_addr));
	memcpy(smart_mode_cfg.smc_end_addr, end_addr, sizeof(end_addr));

	ret = wlan_set_smart_mode_cfg(&uap_network, &smart_mode_cfg,
				      ezconnect_sm);
	if (ret != WM_SUCCESS)
		prov_e("Error: wlan_set_smart_mode_cfg failed.");

	es = os_mem_calloc(sizeof(struct ezconn_state));
	es1 = os_mem_calloc(sizeof(struct ezconn_state));

	wlan_start_smart_mode();

	os_semaphore_get(&smc_cs_sem, OS_WAIT_FOREVER);
	if (received_packet_type == EZ_MULTICAST) {
		if (es->state == 1 || es1->state == 1) {
			wlan_stop_smart_mode();
			smart_mode_cfg.channel = network_channel;
			smart_mode_cfg.min_scan_time = 120;
			smart_mode_cfg.max_scan_time = 140;
			ret = wlan_set_smart_mode_cfg(&uap_network,
						      &smart_mode_cfg,
						      ezconnect_sm);
			wlan_start_smart_mode();
			os_semaphore_get(&smc_cs_sem, OS_WAIT_FOREVER);
		}
		if (es->state == 3) {
			prov_d("EZConnect provisioning done (from-DS).\r\n");
			g_esp = es;
		} else {
			prov_d("EZConnect provisioning done (to_DS).\r\n");
			g_esp = es1;
		}
		wlan_stop_smart_mode();
		g_esp->security = -1;

		while (g_esp->security == -1) {
			prov_d("WLAN Scanning...\r\n");
			ret = wlan_scan(ezconnect_scan_cb);
			if (ret != WM_SUCCESS) {
				prov_e("Scan request failed. Retrying.");
				os_thread_sleep(os_msec_to_ticks(100));
				continue;
			}

			os_semaphore_get(&smc_cs_sem, OS_WAIT_FOREVER);
		}

		net = &prov_g.current_net;
		bzero(net, sizeof(*net));

		snprintf(net->name, 8, "default");
		strncpy(net->ssid, g_esp->ssid, 33);
		net->security.type = g_esp->security;
		net->security.psk_len = g_esp->pass_len;
		if (g_esp->security == WLAN_SECURITY_WEP_OPEN) {
			ret = load_wep_key((const uint8_t *) g_esp->plain_pass,
					   (uint8_t *) net->security.psk,
					   (uint8_t *) &net->security.psk_len,
					   WLAN_PSK_MAX_LENGTH);
		} else {
			strncpy(net->security.psk, g_esp->plain_pass,
				WLAN_PSK_MAX_LENGTH);
		}
		net->ip.ipv4.addr_type = 1; /* DHCP */
		prov_g.state = PROVISIONING_STATE_DONE;

		prov_d("SSID: %s\r\n", g_esp->ssid);
		prov_d("Passphrase %s\r\n", g_esp->plain_pass);
		prov_d("Security %d\r\n", g_esp->security);

		(prov_g.pc)->provisioning_event_handler(
			PROV_NETWORK_SET_CB, &prov_g.current_net,
			sizeof(struct wlan_network));
	} else if (received_packet_type == SMC_PROBE_REQ ||
		   received_packet_type == SMC_AUTH) {
		wlan_stop_smart_mode_start_uap(&uap_network);
		(prov_g.pc)->provisioning_event_handler(
			PROV_MICRO_AP_UP_REQUESTED, &uap_network,
			sizeof(struct wlan_network));
	}
	os_mem_free(beacon_info);

	wakelock_put(PROV_EZCONNECT_LOCK);

	os_thread_self_complete((os_thread_t *)thandle);

	return;
}


void prov_ezconnect_stop()
{
	wlan_stop_smart_mode();
	if (es) {
		os_mem_free(es);
		es = NULL;
	}
	if (es1) {
		os_mem_free(es1);
		es1 = NULL;
	}
	if (smc_cs_sem) {
		os_semaphore_put(&smc_cs_sem);
		os_semaphore_delete(&smc_cs_sem);
		smc_cs_sem = NULL;
	}
	return;
}
