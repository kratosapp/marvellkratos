
/*
 *  Copyright 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * mdev_pinmux.c: generic Pinmux helper functions
 */

#include <wmtypes.h>
#include <wmstdio.h>
#include <mdev_pinmux.h>
#include <wm_os.h>

static mdev_t MDEV_pinmux;
static const char *MDEV_NAME_pinmux = "MDEV_PINMUX";
static os_mutex_t pinmux_mutex;

/* Please note this depends on mc200_pinmux.h */
int pinmux_drv_get_gpio_func(int pin)
{
	if (pin < GPIO_0 || pin > GPIO_79)
		return -WM_E_INVAL;

	if (((pin >= GPIO_20) && (pin <= GPIO_26))
		|| (pin == GPIO_57) || (pin == GPIO_58)) {
		return PINMUX_FUNCTION_1;
	} else {
		return PINMUX_FUNCTION_0;
	}
}

int pinmux_drv_setfunc(mdev_t *dev, int pin, int func)
{
	int ret = WM_SUCCESS;
	if (func < 0)
		return -WM_FAIL;
	ret = os_mutex_get(&pinmux_mutex, OS_WAIT_FOREVER);
	if (ret == -WM_FAIL) {
		PINMUX_LOG("failed to get mutex\n\r");
		return ret;
	}
	GPIO_PinMuxFun(pin, func);
	os_mutex_put(&pinmux_mutex);

	return ret;
}

int pinmux_drv_close(mdev_t *dev)
{
	return 0;
}

mdev_t *pinmux_drv_open(const char *name)
{
	mdev_t *mdev_p = mdev_get_handle(name);

	if (mdev_p == NULL)
		PINMUX_LOG("driver open called without registering device");
	return mdev_p;
}

int pinmux_drv_init(void)
{
	int ret = WM_SUCCESS;

	if (mdev_get_handle(MDEV_NAME_pinmux) != NULL)
		return ret;

	MDEV_pinmux.name = MDEV_NAME_pinmux;
	ret = os_mutex_create(&pinmux_mutex, "pmux", OS_MUTEX_INHERIT);
	if (ret == -WM_FAIL)
		return ret;

	mdev_register(&MDEV_pinmux);

	return ret;
}
