/*
 *  Copyright 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */

/*
 * mdev_gpio.c: mdev driver for GPIO
 */
#include <wmstdio.h>
#include <mdev_gpio.h>
#include <wm_os.h>
#include <lowlevel_drivers.h>



static mdev_t MDEV_gpio;
static const char *MDEV_NAME_gpio = "MDEV_GPIO";
static os_mutex_t gpio_mutex;


#define PortTotal         ((GPIO_MaxNo >> 5) + 1)

#define PortNum(gpioNo)   (gpioNo >> 5)
#define GpioNum(gpioNo)   (gpioNo & 0x1F)


/**
   This structure holds callback function to be called
   when interrupt occurs on the pin
*/
typedef struct {
	void *data;
	gpio_irq_cb gpio_cb;
} gpio_irq_obj_t;

static gpio_irq_obj_t gpio_irq_obj_list[GPIO_MaxNo + 1];

int gpio_drv_get_io_domain(int pin)
{
#ifdef CONFIG_CPU_MW300
	if (pin >= GPIO_0 && pin <= GPIO_15)
		return 0;
	else if (pin >= GPIO_16 && pin <= GPIO_21)
		return 1;
	else if (pin >= GPIO_28 && pin <= GPIO_33)
		return 2;
	else if (pin == GPIO_27 || (pin >= GPIO_34 && pin <= GPIO_49))
		return 3;
	else
		return -1;
#else
	if (pin >= GPIO_0 && pin <= GPIO_17)
		return 0;
	else if (pin >= GPIO_28 && pin <= GPIO_50)
		return 1;
	else if (pin >= GPIO_59 && pin <= GPIO_79)
		return 2;
	else
		return -1;
#endif
}

int gpio_drv_write(mdev_t *dev, int pin, int val)
{
	int ret = WM_SUCCESS;

	ret = os_mutex_get(&gpio_mutex, OS_WAIT_FOREVER);
	if (ret == -WM_FAIL) {
		GPIO_LOG("failed to get mutex\n\r");
		return ret;
	}
	GPIO_WritePinOutput(pin, val);
	os_mutex_put(&gpio_mutex);
	return ret;
}

int gpio_drv_read(mdev_t *dev, int pin, int *val)
{
	int ret = WM_SUCCESS;

	ret = os_mutex_get(&gpio_mutex, OS_WAIT_FOREVER);
	if (ret == -WM_FAIL) {
		GPIO_LOG("failed to get mutex\n\r");
		return ret;
	}
	*val = GPIO_ReadPinLevel(pin);
	os_mutex_put(&gpio_mutex);
	return ret;
}

int gpio_drv_setdir(mdev_t *dev, int pin, int dir)
{
	int ret = WM_SUCCESS;

	ret = os_mutex_get(&gpio_mutex, OS_WAIT_FOREVER);
	if (ret == -WM_FAIL) {
		GPIO_LOG("failed to get mutex\n\r");
		return ret;
	}
	GPIO_SetPinDir(pin, dir);
	os_mutex_put(&gpio_mutex);
	return ret;
}

void GPIO_IRQHandler(void)
{
	uint32_t i, intStatus[PortTotal];

	for (i = 0; i < PortTotal; i++) {
		/* Get current interrupt status */
		intStatus[i] = GPIO->GEDR[i].WORDVAL;
		/* Clear the generated interrupts */
		GPIO->GEDR[i].WORDVAL = intStatus[i];
	}
	/* Check which GPIO pin has interrupt */
	for (i = GPIO_MinNo; i <= GPIO_MaxNo; i++) {
		if (intStatus[PortNum(i)] & (0x01 << GpioNum(i))) {
			/* Call interrupt callback function */
			if (gpio_irq_obj_list[i].gpio_cb) {
				void *data = gpio_irq_obj_list[i].data;
				gpio_irq_obj_list[i].gpio_cb(i,
							     data);
			} else{
				/* Disable interrupt if interrupt
				   callback is not install
				*/
				GPIO->GCRER[PortNum(i)].WORDVAL =
					(0x01 << GpioNum(i));
				GPIO->GCFER[PortNum(i)].WORDVAL =
					(0x01 << GpioNum(i));
			}
		}
	}
}

static void gpio_register_cb(int pin,
			    gpio_irq_cb gpio_cb,
			    void *data)
{
	gpio_irq_obj_list[pin].gpio_cb = gpio_cb;
	gpio_irq_obj_list[pin].data = data;
}


int gpio_drv_set_cb(mdev_t *dev, int pin, GPIO_Int_Type type,
		    void *data,
		    gpio_irq_cb gpio_cb)
{
	if (pin < 0)
		return -WM_FAIL;

	if (pin > GPIO_MaxNo)
		return -WM_FAIL;

	if (type != GPIO_INT_DISABLE && type != GPIO_INT_BOTH_EDGES
	    && type != GPIO_INT_FALLING_EDGE && type != GPIO_INT_RISING_EDGE)
		return -WM_FAIL;

	int ret = os_mutex_get(&gpio_mutex, OS_WAIT_FOREVER);
	if (ret == -WM_FAIL) {
		GPIO_LOG("failed to get mutex\n\r");
		return ret;
	}
	/* Check if any interrupt is pending for this pin
	 * if so clear it before installing callback
	 */
	if (GPIO_GetIntStatus(pin))
		GPIO_IntClr(pin);

	/* install_int_callback(INT_GPIO, pin, gpio_cb); */
	if (gpio_cb)
		gpio_register_cb(pin, gpio_cb, data);

	GPIO_IntConfig(pin, type);
	GPIO_IntMask(pin, (gpio_cb == NULL) ? MASK : UNMASK);
	NVIC_SetPriority(GPIO_IRQn, 0xf);
	NVIC_EnableIRQ(GPIO_IRQn);
	os_mutex_put(&gpio_mutex);
	return ret;
}

int gpio_drv_close(mdev_t *dev)
{
	return WM_SUCCESS;
}

mdev_t *gpio_drv_open(const char *name)
{
	mdev_t *mdev_p = mdev_get_handle(name);

	if (mdev_p == NULL) {
		GPIO_LOG("driver open called without registering device"
			 " (%s)\n\r", name);
	}
	return mdev_p;
}

int gpio_drv_init(void)
{
	int ret = WM_SUCCESS;

	if (mdev_get_handle(MDEV_NAME_gpio) != NULL)
		return ret;

	MDEV_gpio.name = MDEV_NAME_gpio;

	ret = os_mutex_create(&gpio_mutex, "gpio", OS_MUTEX_INHERIT);
	if (ret == -WM_FAIL)
		return ret;

	/* Enable GPIO Clock */
	CLK_ModuleClkEnable(CLK_GPIO);

	mdev_register(&MDEV_gpio);

	return ret;
}
