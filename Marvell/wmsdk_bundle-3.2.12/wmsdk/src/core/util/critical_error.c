/*
 *  Copyright (C) 2008-2015, Marvell International Ltd.
 *  All Rights Reserved.
 */
#include <wmstdio.h>
#include <wmlog.h>
#include <wm_utils.h>
#include <critical_error.h>

/* Default action for critical error. Can be overriden by appplications
 * by writing their own function with this same signature.
 */
WEAK void critical_error(critical_errno_t errno, void *data)
{
	wmlog_e("crit", "Critical error number: %d", errno);
	wmlog("crit", "Halting processor");
	while (1)
		;
}

char *critical_error_msg(critical_errno_t errno)
{
	/* We pass errno to critical_error function as a negative value,
	 * hence we set a switch on -errno. */
	switch (-errno) {
	case CRIT_ERR_WLAN_INIT_FAILED:
		return "WLAN initialization failed!";
	case CRIT_ERR_APP_FRAMEWORK_INIT_FAILED:
		return "App Framework init failed!";
	case CRIT_ERR_APP_FRAMEWORK:
		return "App Framework error!";
	case CRIT_ERR_HARD_FAULT:
		return "Hardfault occurred!";
	default:
		return "Other error!";
	}
}
