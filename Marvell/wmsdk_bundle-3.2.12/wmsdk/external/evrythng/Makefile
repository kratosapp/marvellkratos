# Copyright (C) 2008-2014 Marvell International Ltd.
# All Rights Reserved.

EVRYTHNG_SRC_DIR=$(CURDIR)/src/
EVRYTHNG_INC_DIR=$(CURDIR)/src/
MQTT_SRC_DIR=$(CURDIR)/mqtt/src/
MQTT_INC_DIR=$(CURDIR)/mqtt/src/

# Directory for output files (lst, obj, dep, elf, sym, map, hex, bin etc.).
OUTDIR = build

# Toolchain definition.
CC=arm-none-eabi-gcc
AR=arm-none-eabi-ar
OBJCOPY=arm-none-eabi-objcopy
OBJDUMP=arm-none-eabi-size
NM = arm-none-eabi-nm

ifeq ($(NOISY),1)
AT=
else
AT=@
endif

# Debugging format.
#DEBUG = stabs
#DEBUG = dwarf-2
DEBUG= gdb

# Optimization level, can be [0, 1, 2, 3, s].
# 0 = turn off optimization. s = optimize for size.
# (Note: 3 is not always the best optimization level. See avr-libc FAQ.)
#OPT = s
#OPT = 2
#OPT = 3
#OPT = 0
OPT = s


# Compiler flag to set the C Standard level.
# c89   - "ANSI" C
# gnu89 - c89 plus GCC extensions
# c99   - ISO C99 standard (not yet fully implemented)
# gnu99 - c99 plus GCC extensions
CSTANDARD = gnu89
FIT_FOR_PM3=n

# Compiler flags definition.
CFLAGS+=-g$(DEBUG) \
		-DCONFIG_OS_FREERTOS \
		-DCONFIG_HW_RTC \
		-DNOSTACKTRACE \
		-DNO_HEAP_TRACKING \
		-DNO_PERSISTENCE \
		-DNO_FILESYSTEM \
		-DTLSSOCKET \
		-O$(OPT) \
		-std=$(CSTANDARD) \
		-I $(FREERTOS_INC_DIR) \
		-I $(FREERTOS_INC_DIR)/../../ \
		-I $(LWIPLIB_PATH)/lwip/ \
		-I $(LWIPLIB_PATH)/lwip/ipv4/ \
		-I $(LWIPLIB_PATH)/lwip/ipv6/ \
		-I $(CURDIR)/../../src/incl/sdk/ \
		-I $(MQTT_INC_DIR)/ \
		-I $(EVRYTHNG_INC_DIR)/ \
                -I $(EXT_CYASSL_PATH)/ \
                -I $(EXT_CYASSL_PATH)/cyassl/ctaocrypt/ \
                -I $(EXT_CYASSL_PATH)/cyassl/ \
		-D inline= \
		-mthumb \
		-MMD \
		-mcpu=cortex-m3 \
		-ffunction-sections \
		-fdata-sections \
		-fno-common \
		-Wall -Wno-unused-function \
		-Wno-address
#CFLAGS+=-DEVRYTHNG_DEBUG
#CFLAGS+=-DOPENSSL

ifeq ($(CONFIG_ENABLE_LTO), y)
CFLAGS += -flto -ffat-lto-objects
endif

ifeq (y, $(FIT_FOR_PM3))
	CFLAGS += -DFIT_FOR_PM3
endif

EVRYTHNG_SOURCE = \
		$(EVRYTHNG_SRC_DIR)/evrythng.c \
		$(MQTT_SRC_DIR)/marvell_api.c \
		$(MQTT_SRC_DIR)/Clients.c \
		$(MQTT_SRC_DIR)/Heap.c \
		$(MQTT_SRC_DIR)/LinkedList.c \
		$(MQTT_SRC_DIR)/Messages.c \
		$(MQTT_SRC_DIR)/MQTTClient.c \
		$(MQTT_SRC_DIR)/MQTTPacket.c \
		$(MQTT_SRC_DIR)/MQTTPacketOut.c \
		$(MQTT_SRC_DIR)/MQTTProtocolClient.c \
		$(MQTT_SRC_DIR)/MQTTProtocolOut.c \
		$(MQTT_SRC_DIR)/SSLSocket.c \
		$(MQTT_SRC_DIR)/TLSSocket.c \
		$(MQTT_SRC_DIR)/Socket.c \
		$(MQTT_SRC_DIR)/SocketBuffer.c \
		$(MQTT_SRC_DIR)/Thread.c \
		$(MQTT_SRC_DIR)/Tree.c \
		$(MQTT_SRC_DIR)/utf-8.c

SOURCE+=$(EVRYTHNG_SOURCE)

# List of all source files without directory and file-extension.
ALLSRCBASE = $(notdir $(basename $(SOURCE)))


LIBS=

# List of all objects files.
OBJS = $(addprefix $(OUTDIR)/, $(addsuffix .o, $(ALLSRCBASE)))
D_LIST = $(wildcard $(addprefix $(CURDIR)/$(OUTDIR)/, $(addsuffix .dep, $(ALLSRCBASE))))


# Define Messages.
# English
MSG_BEGIN = -------- begin --------
MSG_END = --------  end  --------


# Rules definition. ***********************************************************

FINAL_TARGET = sdk_artifacts

all: begin gccversion $(FINAL_TARGET) end


ifneq ($(D_LIST),)
  include $(D_LIST)
endif

sdk_artifacts: include_dir libs

INC_COPY_FILES_BASENAME = evrythng.h
INC_COPY_FILES = $(addprefix $(RTOS_SOURCE_DIR)/include/,$(INC_COPY_FILES_BASENAME))
include_dir:
	@mkdir -p build/evrythng
	@$(COPY_CMD) -pfr $(EVRYTHNG_INC_DIR)/*.h build/evrythng/

LIBS_WM =  $(OUTDIR)/libevrythng.a 
libs: $(LIBS_WM)

### EVRYTHNG
EVRYTHNG_SRC_BASE = $(notdir $(basename $(EVRYTHNG_SOURCE)))
EVRYTHNG_OBJS = $(addprefix $(OUTDIR)/, $(addsuffix .o, $(EVRYTHNG_SRC_BASE)))
$(OUTDIR)/libevrythng.a: $(EVRYTHNG_OBJS)  
	@echo " [ar] $@"
	@echo ""
	$(AT)$(AR) cru $@ $^

# Compile: create object files from C source files.
define COMPILE_C_TEMPLATE
ifneq ($(findstring CYGWIN, ${OS}), )
$(OUTDIR)/$(notdir $(basename $(1))).o : $(1)
##	@echo
	@echo " [cc] $$@"
	$(AT)(`echo $(CC) -c  $$(CFLAGS) $$< -o $$@ | sed -e 's/\/cygdrive\/\([a-zA-Z]\)\//\1:\//g'`)
	$(AT)(sed -i -e 's/ \([a-zA-Z]\):/ \/cygdrive\/\1/g' $(OUTDIR)/$(notdir $(basename $(1))).d && mv $(OUTDIR)/$(notdir $(basename $(1))).d $(OUTDIR)/$(notdir $(basename $(1))).dep)
else
$(OUTDIR)/$(notdir $(basename $(1))).o : $(1)
##	@echo
	@echo " [cc] $$@"
	$(AT)($(CC) -c  $$(CFLAGS) $$< -o $$@)
	$(AT)(mv $(OUTDIR)/$(notdir $(basename $(1))).d $(OUTDIR)/$(notdir $(basename $(1))).dep)
endif
endef
$(foreach src, $(SOURCE), $(eval $(call COMPILE_C_TEMPLATE, $(src))))


clean :
	-@rm -f $(OBJS)
	-@rm -f $(D_LIST)
	-@rm -rf $(OUTDIR)


# Eye candy.
begin:
##	@echo
	@echo $(MSG_BEGIN)
ifeq ($(FREERTOS_INC_DIR),)
  $(error Please Specify path to the FreeRTOS artifacts, using FREERTOS_INC_DIR variable)
endif


end:
	@echo $(MSG_END)
##	@echo

# Display compiler version information.
gccversion :
	@$(CC) --version
