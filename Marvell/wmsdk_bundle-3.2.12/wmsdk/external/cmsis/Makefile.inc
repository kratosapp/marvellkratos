# Copyright (C) 2015 Marvell International Ltd.
# All Rights Reserved.

# Makefile.inc: Some variable definitions to build CMSIS DSPLib

include $(SDK_DIR)/.config
ARCH_DRIVER_DIR-$(CONFIG_CPU_MW300) := mw300
ARCH_DRIVER_DIR-$(CONFIG_CPU_MC200) := mc200

# Prefix to the cross compiler
PREFIX ?= arm-none-eabi-
# Common stuff
CC = gcc
AR = ar
LD = ld

# Some directories
OBJDIR := $(DSPLROOT)/obj
INCDIR := $(DSPLROOT)/Include

# FLOAT_ABI can be soft, softfp or hard. Refer to GCC manual for details
# Applicable to CM4 only
FLOAT_ABI = softfp

# Common flags for building stuff
CFLAGS =			\
	-mthumb			\
	-Os			\
	-g			\
	-Wall			\
	-I$(INCDIR)		\
	-I $(SDK_DIR)/src/incl/sdk/drivers/$(ARCH_DRIVER_DIR-y) \
	-fno-strict-aliasing	\
	-ffunction-sections	\
	-fdata-sections		\
	-DARM_MATH_MATRIX_CHECK	\
	-DARM_MATH_ROUNDING	\
	-DUNALIGNED_SUPPORT_DISABLE

ifeq ($(CONFIG_CPU_MC200), y)
CFLAGS += -mcpu=cortex-m3	\
	-DARM_MATH_CM3
endif

ifeq ($(CONFIG_CPU_MW300), y)
CFLAGS += -mcpu=cortex-m4	\
	-DARM_MATH_CM4

ifneq ($(FLOAT_ABI), soft)
CFLAGS += -mfpu=fpv4-sp-d16	\
	-mfloat-abi=$(FLOAT_ABI)\
	-ffp-contract=off	\
	-D__FPU_PRESENT=1
endif
endif

