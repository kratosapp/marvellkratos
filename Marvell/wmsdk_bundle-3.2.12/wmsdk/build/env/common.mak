# -*- makefile -*-
# Copyright (C) 2008-2014 Marvell International Ltd.
# All Rights Reserved.

# common.mak provides common variables required by the build system
#
# The following are some of the important variables
#
# CFLAGS: The common flags that are passed to the compiler for
# building .o (object) files from .c files
#

ifneq "$(INCLUDE_COMMON_MAK)" "1"
INCLUDE_COMMON_MAK = 1

# Pull in the build configurations
include $(SDK_PATH)/.config
-include $(SDK_PATH)/Makefile.vars

####### Configurable Parameters
ARCH_NAME-$(CONFIG_CPU_MW300) := mw300
ARCH_NAME-$(CONFIG_CPU_MC200) := mc200

####### Default Variables used in the build system
TOOL_CHAIN?=GNU
INC_DIR?= $(SDK_PATH)/src/incl
OBJ_DIR        := .

#
# On Windows/Cygwin, make uses paths that start with
# /cygdrive/c/folder, while the cross toolchain uses paths like
# C:\folder.
# If the variable CYGPATH is available, then toolchains intelligently
# convert between the two path notations
#
ifneq ($(OS), Linux)
export CYGPATH=cygpath
# The GNU/ARM toolchain doesn't yet support CYGPATH
export CC := $(SDK_PATH)/build/toolchain/convertcc.sh
export LD := $(SDK_PATH)/build/toolchain/convertld.sh
export AR := $(SDK_PATH)/build/toolchain/convertar.sh
endif

####### Flags to control output to screen
ifeq ($(strip $(NOISY)),)
AT := @
ARQUIET := 2> /dev/null
endif

#######
ifeq ($(TOOL_CHAIN), GNU)
 include $(SDK_PATH)/build/toolchain/GNU/GNU.flags
 INCLUDE_DIRS-y +=  $(INC_DIR)/libc
endif


####### Include Directories
INCLUDE_DIRS-y += $(INC_DIR)/platform/os/freertos $(FREERTOS_INC_DIR) \
         $(INC_DIR)/platform/net/lwip $(LWIP_INC_DIR) \
         $(INC_DIR)/sdk/drivers/wlan \
         $(INC_DIR)/sdk/drivers/wlan/mlan \
         $(EXT_FW_SUPPLICANT_PATH)/incl \
         $(INC_DIR)/sdk \
	 $(INC_DIR)/sdk/crypto \
	 $(INC_DIR)/sdk/drivers \
	 $(INC_DIR)/sdk/drivers/$(ARCH_NAME-y) \
	 $(INC_DIR)/sdk/drivers/$(ARCH_NAME-y)/regs \
	 $(EXT_CYASSL_PATH)/cyassl/ctaocrypt \
	 $(EXT_CYASSL_PATH)  \
	 $(EXT_CYASSL_PATH)/cyassl  \
	 $(SDK_PATH)/src/incl/tls

INCLUDE_DIRS-$(CONFIG_USB_DRIVER) += $(USB_INC_DIR)
INCLUDE_DIRS-$(CONFIG_USB_DRIVER_HOST) += $(USB_HOST_INC_DIR)

####### CFLAGS/ARFLAGS
CFLAGS += $(addprefix -I, $(INCLUDE_DIRS-y))
CFLAGS += -DSDK_VERSION=$(SDK_VERSION)

ifeq ($(CONFIG_PROFILER), y)
CFLAGS += -pg
endif

# See if LTO needs to be enabled or not
ifeq ($(CONFIG_ENABLE_LTO), y)
 CFLAGS += -flto -ffat-lto-objects
endif

ARFLAGS   = -r

##########################
# As per decided policy if secure boot is enabled secure PSM is also
# enabled by default.

ifneq ($(SECURE_CONF_FILE),)
CFLAGS += -DCONFIG_SECURE_PSM
endif
##########################

# Export environment variables for rules.mak to use
export CC LD AS OS TOOL_CHAIN CFLAGS EXTRACFLAGS ASFLAGS

##### End INCLUDE_COMMON_MAK
endif
