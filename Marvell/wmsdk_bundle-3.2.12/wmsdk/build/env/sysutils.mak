# This file contains paths various system utilities required during WMSDK
# build. You can edit this file as per your system requirements. This file
# can be directly included in any other Makefile.

# Check if this file was already included
ifeq ($(COPY_CMD),)

########### Common variables (Pre) ##########
export OS := $(shell uname)
export MKFTFS = $(SDK_DIR)/tools/bin/flash_pack.py
export COPY_CMD := $(shell which cp | tail -1 )
export CMP_CMD := $(shell which cmp | tail -1 )

ifeq ($(OS), Linux)
export TOOLS_BINDIR=Linux
endif

ifneq ($(findstring CYGWIN, ${OS}), )
export TOOLS_BINDIR=Windows
#
# On Cygwin, make uses paths that start with "/cygdrive/c/folder", while
# the cross toolchain uses paths like "C:\folder". If the variable CYGPATH
# is available, then toolchains intelligently convert between the two path
# notations.
#
export CYGPATH=cygpath
export CYG_COMPAT :=  CC=$(SDK_DIR)/build/toolchain/convertcc.sh LD=$(SDK_DIR)/build/toolchain/convertld.sh  AR=$(SDK_DIR)/build/toolchain/convertar.sh
endif

ifneq ($(findstring MINGW, ${OS}), )
# Override default values as mingw does not have 'which' command
export COPY_CMD := cp
export CMP_CMD := cmp
export TOOLS_BINDIR=mingw
endif

ifeq ($(OS), Darwin)
export TOOLS_BINDIR=Darwin
endif

########### Common variables (Post) ##########
export KCONFIG_CMD=$(SDK_DIR)/tools/bin/$(TOOLS_BINDIR)/conf
export KCONFIG_CMD_M=$(SDK_DIR)/tools/bin/$(TOOLS_BINDIR)/mconf
export AXF2FW=$(SDK_DIR)/tools/bin/$(TOOLS_BINDIR)/axf2firmware
export SECURE_BOOT_TOOL=$(SDK_DIR)/tools/bin/$(TOOLS_BINDIR)/secureboot

else
endif

