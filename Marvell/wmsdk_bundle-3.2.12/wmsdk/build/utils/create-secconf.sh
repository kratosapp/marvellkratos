#!/bin/bash

# Copyright (C) 2015 Marvell International Ltd.
# All Rights Reserved.

# Secure Configuration File Generator Script

CONF_DIR=${2:-sboot_conf}
CONF_FILE=$1

COPY_CMD=${COPY_CMD:-cp}

if  [ -e $CONF_FILE ]; then
	exit 0
fi

if [ -z $SDK_DIR ]; then
	echo "Please specify SDK_DIR path"
	return 1
fi

OS=`uname`
case "$OS" in
	Linux | Darwin )
		FIXED_SDK_DIR=$SDK_DIR
		FIXED_CONF_DIR=$CONF_DIR
	;;
	CYGWIN* )
		FIXED_SDK_DIR=`cygpath -m $SDK_DIR`
		FIXED_CONF_DIR=`cygpath -m $CONF_DIR`
	;;
	MINGW* )
		FIXED_SDK_DIR=`cmd //c echo $SDK_DIR`
		FIXED_CONF_DIR=`cmd //c echo $CONF_DIR`
	;;
	* )
		echo "Platform $OS is not supported"
		exit 1
	;;
esac

# Create dir if not present
mkdir -p $CONF_DIR

gen_random ()
{
	echo `od -N$1 -t x1 < /dev/urandom | cut -b9-  | tr '\n' ' '  | tr '[:lower:]' '[:upper:]' | sed 's/ //g'`
}

openssl_check () {
	OPENSSL_PATH=`which openssl`
	if [ ! -e "$OPENSSL_PATH" ]; then
		echo "Please install OpenSSL to generate RSA keys"
		rm -f $CONF_FILE
		exit 1
	fi
	return 0
}

jtag_disable () {
	echo "" >> $CONF_FILE
	if [ "$1" == "0" ]; then
		echo "# JTAG is not disabled" >> $CONF_FILE
		return 0
	fi

	echo "# JTAG is disabled" >> $CONF_FILE
	echo "KEY_OTP_JTAG_ENABLE	DEC	0" >> $CONF_FILE
	return 0
}

nonflash_boot_disable () {
	echo "" >> $CONF_FILE
	if [ "$1" == "0" ]; then
		echo "# UART/USB boot is not disabled" >> $CONF_FILE
		return 0
	fi

	echo "# UART/USB boot is disabled" >> $CONF_FILE
	echo "KEY_OTP_UART_USB_BOOT_ENABLE	DEC	0" >> $CONF_FILE
	return 0
}

# Arguments : 0 = Unsigned, 1 = Use sample RSA keys, 2 = Generate RSA keys
boot2_sign () {
	echo "" >> $CONF_FILE
	case "$1" in
		"0" )
			echo "# Boot2 is not signed" >> $CONF_FILE
			return 0
		;;
		"1" )
			BOOT2_PRIVATE_KEY=$FIXED_SDK_DIR/tools/src/secureboot/sample_keys/boot2-prvKey.der
			BOOT2_PUBLIC_KEY=$FIXED_SDK_DIR/tools/src/secureboot/sample_keys/boot2-pubKey.der
		;;
		"2" )
			BOOT2_PEM_KEY=$FIXED_CONF_DIR/boot2-prvKey.pem
			BOOT2_PRIVATE_KEY=$FIXED_CONF_DIR/boot2-prvKey.der
			BOOT2_PUBLIC_KEY=$FIXED_CONF_DIR/boot2-pubKey.der

			if [ ! -e $BOOT2_PRIVATE_KEY ]; then
				openssl_check
				openssl genrsa 2048 > $BOOT2_PEM_KEY || exit 1
				openssl rsa -in $BOOT2_PEM_KEY -outform DER -out $BOOT2_PRIVATE_KEY || exit 1
				openssl rsa -in $BOOT2_PEM_KEY -pubout -out $BOOT2_PUBLIC_KEY -outform DER || exit 1
				rm -f $BOOT2_PEM_KEY
			fi
		;;
	esac

	echo "# Boot2 is signed using RSA-2048" >> $CONF_FILE
	echo "KEY_BOOT2_SIGNING_ALGO	STR	RSA_SIGN" >> $CONF_FILE
	echo "KEY_BOOT2_PRIVATE_KEY	FILE	$BOOT2_PRIVATE_KEY" >> $CONF_FILE
	echo "KEY_BOOT2_PUBLIC_KEY	FILE	$BOOT2_PUBLIC_KEY" >> $CONF_FILE

	return 0
}

# Arguments : 0 = Unsigned, 1 = Use sample AES keys, 2 = Generate AES keys
boot2_encrypt () {
	echo "" >> $CONF_FILE
	case "$1" in
		"0" )
			echo "# Boot2 is not encrypted" >> $CONF_FILE
			return 0
		;;
		"1" )
			ENC_KEY="0123456776543210010101011212121212345678232323238765432100112233"
		;;
		"2" )
			ENC_KEY=$(gen_random 32)
		;;
	esac

	echo "# Boot2 is encrypted using AES CCM-256" >> $CONF_FILE
	echo "KEY_BOOT2_ENCRYPT_ALGO	STR	AES_CCM_256_ENCRYPT" >> $CONF_FILE

	echo -n "KEY_BOOT2_ENCRYPT_KEY	HEX	" >> $CONF_FILE
	echo $ENC_KEY >> $CONF_FILE

	return 0
}

# Arguments : 0 = Unsigned, 1 = Use sample RSA keys, 2 = Generate RSA keys
mcufw_sign () {
	echo "" >> $CONF_FILE
	case "$1" in
		"0" )
			echo "# MCUFW is not signed" >> $CONF_FILE
			return 0
		;;
		"1" )
			MCUFW_PRIVATE_KEY=$FIXED_SDK_DIR/tools/src/secureboot/sample_keys/mcufw-prvKey.der
			MCUFW_PUBLIC_KEY=$FIXED_SDK_DIR/tools/src/secureboot/sample_keys/mcufw-pubKey.der
		;;
		"2" )
			MCUFW_PEM_KEY=$FIXED_CONF_DIR/mcufw-prvKey.pem
			MCUFW_PRIVATE_KEY=$FIXED_CONF_DIR/mcufw-prvKey.der
			MCUFW_PUBLIC_KEY=$FIXED_CONF_DIR/mcufw-pubKey.der

			if [ ! -e $MCUFW_PRIVATE_KEY ]; then
				openssl_check
				openssl genrsa 2048 > $MCUFW_PEM_KEY || exit 1
				openssl rsa -in $MCUFW_PEM_KEY -outform DER -out $MCUFW_PRIVATE_KEY || exit 1
				openssl rsa -in $MCUFW_PEM_KEY -pubout -out $MCUFW_PUBLIC_KEY -outform DER || exit 1
				rm -f $MCUFW_PEM_KEY
			fi
		;;
	esac

	echo "# MCUFW is signed using RSA-2048" >> $CONF_FILE
	echo "KEY_MCUFW_SIGNING_ALGO	STR	RSA_SIGN" >> $CONF_FILE
	echo "KEY_MCUFW_HASH_ALGO	STR	SHA256_HASH" >> $CONF_FILE
	echo "KEY_MCUFW_PRIVATE_KEY	FILE	$MCUFW_PRIVATE_KEY" >> $CONF_FILE
	echo "KEY_MCUFW_PUBLIC_KEY	FILE	$MCUFW_PUBLIC_KEY" >> $CONF_FILE

	return 0
}

# Arguments : 0 = Unsigned, 1 = Use sample AES keys, 2 = Generate AES keys
mcufw_encrypt () {
	echo "" >> $CONF_FILE
	case "$1" in
		"0" )
			echo "# MCUFW is not encrypted" >> $CONF_FILE
			return 0
		;;
		"1" )
			ENC_KEY="2B7E151628AED2A6ABF7158809CF4F3C"
		;;
		"2" )
			ENC_KEY=$(gen_random 16)
		;;
	esac

	echo "# MCUFW is encrypted using AES CTR-128" >> $CONF_FILE
	echo "KEY_MCUFW_ENCRYPT_ALGO	STR	AES_CTR_128_ENCRYPT" >> $CONF_FILE
	echo -n "KEY_MCUFW_ENCRYPT_KEY	HEX	" >> $CONF_FILE
	echo $ENC_KEY >> $CONF_FILE

	return 0
}

psm_encrypt () {
	echo "" >> $CONF_FILE
	case "$1" in
		"0" )
			echo "# PSM is not encrypted" >> $CONF_FILE
			return 0
		;;
		"1" )
			ENC_KEY="21D85243231C2A9A4AB667A946AA1F89"
			NONCE="F0F1F2F3F4F5F6F7F8F9FAFBFCFDFEFF"
		;;
		"2" )
			ENC_KEY=$(gen_random 16)
			NONCE=$(gen_random 16)
		;;
	esac

	KEY_PSM_ENCRYPT_KEY=`cat $SDK_DIR/src/incl/sdk/keystore.h | grep KEY_PSM_ENCRYPT_KEY | cut -d " " -f3 | cut -d "," -f1`
	KEY_PSM_NONCE=`cat $SDK_DIR/src/incl/sdk/keystore.h | grep KEY_PSM_NONCE | cut -d " " -f3 | cut -d "," -f1`

	echo "# PSM is encrypted using AES CTR-128" >> $CONF_FILE
	echo -n "$KEY_PSM_ENCRYPT_KEY	HEX	" >> $CONF_FILE
	echo $ENC_KEY >> $CONF_FILE
	echo -n "$KEY_PSM_NONCE	HEX	" >> $CONF_FILE
	echo $NONCE >> $CONF_FILE
}

case "$CONF_FILE" in
	*-c0.csv )
		echo "# Security Configuration 0" >> $CONF_FILE
		jtag_disable 0
		nonflash_boot_disable 0
		boot2_sign 0
		boot2_encrypt 0
		mcufw_sign 0
		mcufw_encrypt 0
		psm_encrypt 1
	;;
	*-c1.csv )
		echo "# Security Configuration 1" >> $CONF_FILE
		jtag_disable 0
		nonflash_boot_disable 0
		boot2_sign 0
		boot2_encrypt 1
		mcufw_sign 1
		mcufw_encrypt 0
		psm_encrypt 1
	;;
	*-c2.csv )
		echo "# Security Configuration 2" >> $CONF_FILE
		jtag_disable 0
		nonflash_boot_disable 0
		boot2_sign 1
		boot2_encrypt 1
		mcufw_sign 1
		mcufw_encrypt 0
		psm_encrypt 1
	;;
	*-c3.csv )
		echo "# Security Configuration 3" >> $CONF_FILE
		jtag_disable 0
		nonflash_boot_disable 0
		boot2_sign 1
		boot2_encrypt 1
		mcufw_sign 1
		mcufw_encrypt 1
		psm_encrypt 1
	;;
	*-c51.csv )
		echo "# Security Configuration 4" >> $CONF_FILE
		jtag_disable 1
		nonflash_boot_disable 1
		boot2_sign 0
		boot2_encrypt 2
		mcufw_sign 2
		mcufw_encrypt 0
		psm_encrypt 2
	;;
	*-c52.csv )
		echo "# Security Configuration 5" >> $CONF_FILE
		jtag_disable 1
		nonflash_boot_disable 1
		boot2_sign 2
		boot2_encrypt 2
		mcufw_sign 2
		mcufw_encrypt 0
		psm_encrypt 2
	;;
	*-c53.csv )
		echo "# Security Configuration 6" >> $CONF_FILE
		jtag_disable 1
		nonflash_boot_disable 1
		boot2_sign 2
		boot2_encrypt 2
		mcufw_sign 2
		mcufw_encrypt 2
		psm_encrypt 2
	;;
	* )
		echo "This security configuration is not supported"
		exit 1
	;;
esac

exit 0

