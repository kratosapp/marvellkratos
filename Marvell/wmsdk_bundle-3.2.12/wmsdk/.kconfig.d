deps_config := \
	src/drivers/mc200/Kconfig \
	src/middleware/rfget/Kconfig \
	src/middleware/json/Kconfig \
	src/middleware/httpc/Kconfig \
	src/middleware/httpd/Kconfig \
	src/middleware/provisioning/Kconfig \
	src/middleware/tls/Kconfig \
	src/middleware/mdns/Kconfig \
	src/wlan/Kconfig \
	src/app_framework/Kconfig \
	./build/config/Kconfig

.config include/autoconf.h: $(deps_config)

$(deps_config):
