#!/bin/bash

# Copyright (C) 2015 Marvell International Ltd.
# All Rights Reserved.

# Load application to ram helper script
OS=`uname`
MACHINE=`uname -m`
IFC_FILE=ftdi.cfg
SEMIHOST=false
SCRIPT_DIR=`dirname $0`

if [ $OS == "Linux" ]; then
	if [ $MACHINE == "i686" ]; then
		OPENOCD_PATH="$SCRIPT_DIR/Linux/openocd"
	else
		OPENOCD_PATH="$SCRIPT_DIR/Linux/openocd64"
	fi
	FILE=$1
else
	if [ $OS == "Darwin" ]; then
		OPENOCD_PATH=`which openocd`
		FILE=$1
	else
		OPENOCD_PATH="$SCRIPT_DIR/Windows/openocd"
		FILE=`cygpath -m $1`
	fi
fi

if [ ! -e "$OPENOCD_PATH" ]; then
	echo "Please install OpenOCD for your machine"
	exit 1
fi

print_usage() {
	echo ""
	echo "Usage: $0 <app.axf> [options]"
	echo "Optional Usage:"
	echo "[-s | --semihosting] Enable semihosting based console output"
	echo "[-i | --interface] JTAG hardware interface name, supported ones
	are ftdi, jlink, amontec, malink. Default is ftdi."
}

if [ -z "$FILE" -o $# -lt 1 ]; then
	print_usage
	exit 1
fi

while test -n "$2"; do
	case "$2" in
	--interface|-i)
		if [ $# -gt 2 ]
		then
			IFC_FILE=$3.cfg
		else
			print_usage
			exit 1
		fi
		shift 2
	;;
	--semihosting|-s)
		SEMIHOST=true
		shift 1
	;;
	*)
		print_usage
		exit 1
	;;
	esac
done

test -f "$FILE" ||
{ echo "Error: Firmware image $FILE does not exists"; exit 1; }

# Check if overlays are enabled, warn and exit if yes
nm $FILE | grep "_ol_ovl" > /dev/null &&
{ echo "Error: Provided firmware image (.axf) is overlays enabled and hence
ramload is not possible, please use flashprog to flash the image."; exit 1;
}

READELF=`which arm-none-eabi-readelf 2> /dev/null`

# Even if toolchain is not installed, native readelf works
if [ "$READELF" == "" ]; then
	READELF=`which readelf`
	if [ "$READELF" == "" ]; then
		echo "Error: readelf utility not found, please install binutils or appropriate package"
		exit 1
	fi
fi

entry_point=`"$READELF" -h $FILE | grep "Entry point" | awk '{print $4}'`
((($entry_point & 0x1f000000) == 0x1f000000)) &&
{ echo "Error: Provided firmware image (.axf) is XIP enabled and hence
ramload is not possible, please use flashprog to flash the image (.bin)"; exit 1;
}

echo "Using OpenOCD interface file $IFC_FILE"

if [ "$SEMIHOST" == "false" ]; then
echo $OPENOCD_PATH -s $SCRIPT_DIR/interface -f $IFC_FILE -s $SCRIPT_DIR -f openocd.cfg -c init -c "load $FILE $entry_point" -c shutdown
else
$OPENOCD_PATH -s $SCRIPT_DIR/interface -f $IFC_FILE -s $SCRIPT_DIR -f openocd.cfg -c init -c "sh_load $FILE $entry_point"
fi
