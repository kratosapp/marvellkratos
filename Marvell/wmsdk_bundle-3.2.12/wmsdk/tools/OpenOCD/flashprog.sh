#!/bin/bash

# Copyright (C) 2015 Marvell International Ltd.
# All Rights Reserved.

# Flashprog wrapper/helper script

SCRIPT_DIR=`dirname $0`

LAYOUT_FILE=flashprog.layout
CONFIG_FILE=flashprog.config
MFG_FILE=flashprog.mfg
FLASHDUMPINT=flash_int.bin
FLASHDUMPEXT=flash_ext.bin
MAX_FLASH_TYPE=2

OS=`uname`
MACHINE=`uname -m`
IFC_FILE=ftdi.cfg

if [ $OS == "Linux" ]; then
	if [ $MACHINE == "i686" ]; then
		OPENOCD_PATH="$SCRIPT_DIR/Linux/openocd"
	else
		OPENOCD_PATH="$SCRIPT_DIR/Linux/openocd64"
	fi
else
	if [ $OS == "Darwin" ]; then
		OPENOCD_PATH=`which openocd`
	else
		OPENOCD_PATH="$SCRIPT_DIR/Windows/openocd"
	fi
fi

if [ ! -e "$OPENOCD_PATH" ]; then
	echo "Please install OpenOCD for your machine"
	exit 1
fi

print_usage() {
	echo ""
	echo "Usage: $0"
	echo ""
	echo "Optional Usage:"
	echo "[-l | --new-layout] Flash partition layout file <layout.txt>"
	echo "[-b | --batch]  Batch processing mode config file <config.txt>"
	echo "[-i | --interface] JTAG hardware interface name, supported ones
	are ftdi, jlink, amontec, malink. Default is ftdi."
	echo "[-d | --debug]  Debug mode, dump current flash contents"
	echo "[-r | --read] Read flash <flash_id> (0: Internal QSPI, 1: External
		QSPI, 2: External SPI) starting from <start_address> and of length
		<length> into output file <filename>"
	echo "[-w | --write] Write to flash <flash_id> (0: Internal QSPI, 1: External
		QSPI, 2: External SPI) starting from <start_address> from input file <filename>
		Note: '-w | --write' option does not erase the flash before writing. If
		required, explicitly erase the flash before."
	echo "[-e | --erase] Erase flash <flash_id> (0: Internal QSPI, 1: External
		QSPI, 2: External SPI) starting from <start_address> and length
		<length>"
	echo "[-ea | --erase-all] Erase entire flash <flash_id> (0: Internal
		QSPI, 1: External QSPI, 2: External SPI)"
	echo "[-p | --prod] Batch processing mode mfg file <mfg.txt>"
	echo "[--<component_name> <file_path>] As an alternate to batch mode"
}

copy_file() {
	if [ -z "$2" ]; then
		echo "$1 file name missing"
		print_usage
		exit 1
	fi

	if [ "$1" = "config" ]; then
		FILE=$CONFIG_FILE
	else if [ "$1" = "prod" ]; then
		FILE=$MFG_FILE
	else if [ "$1" = "layout" ]; then
		FILE=$LAYOUT_FILE
	fi
	fi
	fi

	if [ -f "$2" ]; then
		rm -f $FILE
		cp -a "$2" $FILE
	else if [ "$1" == "layout" ]; then
		FIX_LAYOUT_FILE=$2
	else
		echo "$1 file $2 does not exist"
		print_usage
		exit 1
	fi
	fi
}

copy_config_option() {
	comp=`echo "$1" | sed -e 's/--//'`
	if [ -z "$2" ]; then
		print_usage
		exit 1
	fi

	if [ ! -f "$2" ]; then
		echo "File $2 does not exists"
		print_usage
		exit 1
	fi

	if [ $OS == "Linux" ] || [ $OS == "Darwin" ]; then
		if [ -z `echo "$2" | grep '^/'` ]; then
			echo "$comp " `pwd`/"$2" >> $CONFIG_FILE
		else
			echo "$comp " "$2" >> $CONFIG_FILE
		fi
	else
		if [ -z `echo "$2" | grep '^.:'` ] && [ -z `echo "$2" | grep '^/'` ]; then
			echo "$comp " `pwd`/"$2" >> $CONFIG_FILE
		else
			echo "$comp " "$2" >> $CONFIG_FILE
		fi
	fi
}

cleanup() {
	rm -f $LAYOUT_FILE
	rm -f $CONFIG_FILE
	rm -f $MFG_FILE
}

cleanup

while test -n "$1"; do
	case "$1" in
	--new-layout|-l)
		copy_file "layout" $2
		shift 2
	;;
	--batch|-b)
		copy_file "config" $2
		shift 2
	;;
	--debug|-d)
		echo "debug" "flash" > $CONFIG_FILE
		shift 1
	;;
	--read|-r)
		if [ $# -gt 4 ]
		then
		    if [ $2 -lt 0 ] || [ $2 -gt $MAX_FLASH_TYPE ]
		    then
			print_usage
			exit 1
		    else
			echo "read" $2 $3 $4 $5>> $MFG_FILE
			shift 5
		    fi
		else
		    print_usage
		    exit 1
		fi
	;;
	--write|-w)
		if [ $# -gt 3 ]
		then
		    if [ $2 -lt 0 ] || [ $2 -gt $MAX_FLASH_TYPE ]
		    then
			print_usage
			exit 1
		    else
			echo "write" $2 $3 $4>> $MFG_FILE
			shift 4
		    fi
		else
		    print_usage
		    exit 1
		fi
	;;
	--erase|-e)
		if [ $# -gt 3 ]
		then
		    if [ $2 -lt 0 ] || [ $2 -gt $MAX_FLASH_TYPE ]
		    then
			print_usage
			exit 1
		    else
			echo "erase" $2 $3 $4>> $MFG_FILE
			shift 4
		    fi
		else
		    print_usage
		    exit 1
		fi
	;;
	--erase-all|-ea)
		if [ $# -gt 1 ]
		then
		    if [ $2 -lt 0 ] || [ $2 -gt $MAX_FLASH_TYPE ]
		    then
			print_usage
			exit 1
		    else
			echo "erase-all" $2>> $MFG_FILE
			shift 2
		    fi
		else
		    print_usage
		    exit 1
		fi
	;;
	--prod|-p)
		copy_file "prod" $2
		shift 2
	;;
	--interface|-i)
		if [ $# -gt 1 ]
		then
			IFC_FILE=$2.cfg
		else
			print_usage
			exit 1
		fi
		shift 2
	;;
	--help|-h)
		print_usage
		exit 1
	;;
	*)
		copy_config_option $1 $2
		shift 2
	;;
	esac
done

READELF=`which arm-none-eabi-readelf 2> /dev/null`

# Even if toolchain is not installed, native readelf works
if [ "$READELF" == "" ]; then
	READELF=`which readelf`
	if [ "$READELF" == "" ]; then
		echo "Error: readelf utility not found, please install binutils or appropriate package"
		exit 1
	fi
fi

echo "Using OpenOCD interface file $IFC_FILE"

CHIP_ID=`$OPENOCD_PATH -s $SCRIPT_DIR/interface -f $IFC_FILE -s $SCRIPT_DIR -f openocd.cfg -c init -c "chip_id" -c shutdown`

if [ -z "$CHIP_ID" ]; then
	echo "Chip ID could not be detected! Aborting..."
	exit 1
fi
if [ "$CHIP_ID" == "unknown" ]; then
	echo "Unsupported Board! Aborting..."
	exit 1
fi

if [ ! -z "$FIX_LAYOUT_FILE" ]; then
	if [ -f $SCRIPT_DIR/$CHIP_ID/$FIX_LAYOUT_FILE ]; then
		rm -f $LAYOUT_FILE
		cp -a $SCRIPT_DIR/$CHIP_ID/$FIX_LAYOUT_FILE $LAYOUT_FILE
	else
		echo "File $FIX_LAYOUT_FILE does not exists"
		print_usage
		exit 1
	fi
fi

entry_point=`"$READELF" -h $SCRIPT_DIR/$CHIP_ID/flashprog.axf | grep "Entry point" | awk '{print $4}'`

echo "Flashing $CHIP_ID..."
$OPENOCD_PATH -s $SCRIPT_DIR/interface -f $IFC_FILE -s $SCRIPT_DIR -f openocd.cfg -c init -c "sh_load $CHIP_ID/flashprog.axf $entry_point"

cleanup
