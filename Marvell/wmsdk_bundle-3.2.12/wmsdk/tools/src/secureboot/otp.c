/*
 * Copyright (C) 2015, Marvell International Ltd.
 * All Rights Reserved.
 */

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <errno.h>
#include <string.h>
#include <soft_crc.h>
#include "utils.h"

int otp_write_header(FILE *otp)
{
	int ret;
	char *line;

	line = "/*\n"
		" * Copyright (C) 2008-2015, Marvell International Ltd.\n"
		" * All Rights Reserved.\n\n"
		" * This is an auto-generated header file.\n"
		" * Do not modify unless you know what you are doing.\n"
		" */\n\n"
		"#ifndef __SECURE_CONFIG_H__\n"
		"#define __SECURE_CONFIG_H__\n";
	ret = fwrite(line, 1, strlen(line), otp);
	sboot_assert((ret == strlen(line)), , -errno, "");
	return 0;
}

/* Macro to write a #define */
#define WRITE_FLAG_DEF(line, flag) \
	do { \
		ret = fwrite(line, 1, strlen(line), otp); \
		sboot_assert((ret == strlen(line)), , -errno, ""); \
		line = (flag) ? "1\n" : "0\n"; \
		ret = fwrite(line, 1, strlen(line), otp); \
		sboot_assert((ret == strlen(line)), , -errno, ""); \
	} while (0);

#define WRITE_STR_DEF(line, data, len) \
	do { \
		ret = fwrite(line, 1, strlen(line), otp); \
		sboot_assert((ret == strlen(line)), , -errno, ""); \
		\
		if (len != 0 && data != NULL) \
			bin2hex(data, hex, len, sizeof(hex)); \
		ret = fwrite(hex, 1, strlen(hex), otp); \
		sboot_assert((ret == strlen(hex)), , -errno, ""); \
		\
		line = "\"\n"; \
		ret = fwrite(line, 1, strlen(line), otp); \
		sboot_assert((ret == strlen(line)), , -errno, ""); \
	} while (0);

int otp_write_boot_config(FILE *otp, uint8_t boot_config)
{
	int ret;
	char *line;

	line = "\n/* Enable JTAG after boot, UART/USB without password\n"
		" * Set this to 0 for disabling JTAG.\n"
		" */\n#define JTAG_STATUS ";
	WRITE_FLAG_DEF(line, !(boot_config & OTP_JTAG_DISABLE));

	line = "\n/* Enable UART/USB boot\n"
		" * Set this to 0 for disabling UART/USB boot modes.\n"
		" */\n#define UART_USB_BOOT ";
	WRITE_FLAG_DEF(line, !(boot_config & OTP_USB_UART_BOOT_DISABLE));

	line = "\n/* Enable encrypted image boot\n"
		" * Set this to 0 for disabling encrypted boot mode.\n"
		" */\n#define ENCRYPTED_BOOT ";
	WRITE_FLAG_DEF(line, boot_config & OTP_ENCRYPTED_BOOT);

	line = "\n/* Enable signed image boot\n"
		" * Set this to 0 for disabling signed boot mode.\n"
		" */\n#define SIGNED_BOOT ";
	WRITE_FLAG_DEF(line, boot_config & OTP_SIGNED_BOOT);

	return 0;
}

int otp_write_decrypt_key(FILE *otp, const uint8_t *key, uint32_t len)
{
	char hex[128] = {0};
	int ret;
	char *line;

	line = "\n/* AES-CCM 256 bit key, 64 hex characters */\n"
		"#define AES_CCM256_KEY \\\n\t\"";
	WRITE_STR_DEF(line, key, len);
	return 0;
}

int otp_write_public_key_hash(FILE *otp, const uint8_t *hash, uint32_t len)
{
	char hex[128] = {0};
	int ret;
	char *line;

	line = "\n/* SHA256 checksum of RSA Public Key, 64 hex characters */\n"
		"#define RSA_PUB_KEY_SHA256 \\\n\t\"";
	WRITE_STR_DEF(line, hash, len);
	return 0;
}

int otp_write_user_data(FILE *otp, const uint8_t *udata, uint32_t len)
{
	char hex[2 * MAX_OTP_USER_DATA] = {0};
	int ret;
	char *line;

	if (len > MAX_OTP_USER_DATA)
		return ERR_INVALID_ARGS;

	line = "\n/* Enable writing user specific data to OTP memory\n"
		" * Set this to 1 for enabling user data write.\n"
		" * Note: Maximum size for user data can be up-to 252 bytes.\n"
		" */\n#define USER_DATA ";
	WRITE_FLAG_DEF(line, (len != 0));

	line = "\n/* User specific additional data in OTP memory */\n"
		"#define OTP_USER_DATA \\\n\t\"";
	WRITE_STR_DEF(line, udata, len);
	return 0;
}

int otp_close_file(FILE *otp)
{
	int ret;
	char *line;

	line = "\n#endif /* __SECURE_CONFIG_H__ */\n";
	ret = fwrite(line, 1, strlen(line), otp);
	sboot_assert((ret == strlen(line)), fclose(otp), -errno, "");

	fclose(otp);
	return 0;
}

#if 0
/* This code generates otp file in labtool format */

/* This is what kingletOtpTool does i.e. it changes the endianness of data
 * before calculating crc and endianness of crc itself.
 */
static uint32_t get_crc32(const uint8_t *data, uint32_t len)
{
	uint8_t *buf;
	uint32_t crc, result, i;

	buf = malloc(len);
	sboot_assert(buf, , 0, "");

	for (i = 0; i < len / 4; i++) {
		buf[i * 4] = data[i * 4 + 3];
		buf[i * 4 + 1] = data[i * 4 + 2];
		buf[i * 4 + 2] = data[i * 4 + 1];
		buf[i * 4 + 3] = data[i * 4];
	}

	crc = soft_crc32(buf, len, 0);

	*((uint8_t *)&result) = *((uint8_t *)&crc + 3);
	*((uint8_t *)&result + 1) = *((uint8_t *)&crc + 2);
	*((uint8_t *)&result + 2) = *((uint8_t *)&crc + 1);
	*((uint8_t *)&result + 3) = *((uint8_t *)&crc);

	free(buf);
	return result;
}

int otp_write_decrypt_key(FILE *otp, const uint8_t *key, uint32_t len)
{
	uint8_t decrypt_key[40];
	uint32_t crc, i;
	char hex[128];
	int ret;

	if (len == 0 || key == NULL)
		return 0;

	memset(decrypt_key, 0, sizeof(decrypt_key));
	/* Byte 0 is signature of OTP line, Byte 1 is length of OTP data
	 * in 32-byte lines. Refer to MW30X Bootrom Functional Spec 0.8 Section
	 * 5 (OTP data format) for details.
	 */
	decrypt_key[0] = 0x0A;
	decrypt_key[1] = 0x05;
	memcpy(&decrypt_key[2], key, len);

	crc = get_crc32(key, len);

	memcpy(&decrypt_key[len + 2], &crc, sizeof(crc));
	bin2hex(decrypt_key, hex, sizeof(decrypt_key), sizeof(hex));

	for (i = 0; i < 80; i += 16) {
		ret = fwrite(hex + i, 1, 16, otp);
		sboot_assert((ret == 16), , -errno, "");
		fwrite("\n", 1, 1, otp);
	}

	return 0;
}

int otp_write_public_key_hash(FILE *otp, const uint8_t *hash, uint32_t len)
{
	uint8_t key_hash[40];
	uint32_t crc, i;
	char hex[81];
	int ret;

	if (len == 0 || hash == NULL)
		return 0;

	memset(key_hash, 0, sizeof(key_hash));
	/* Byte 0 is signature of OTP line, Byte 1 is length of OTP data
	 * in 32-byte lines. Refer to MW30X Bootrom Functional Spec 0.8 Section
	 * 5 (OTP data format) for details.
	 */
	key_hash[0] = 0x0E;
	key_hash[1] = 0x05;
	memcpy(&key_hash[2], hash, len);

	crc = get_crc32(hash, len);

	memcpy(&key_hash[len + 2], &crc, sizeof(crc));
	bin2hex(key_hash, hex, sizeof(key_hash), sizeof(hex));

	for (i = 0; i < 80; i += 16) {
		ret = fwrite(hex + i, 1, 16, otp);
		sboot_assert((ret == 16), , -errno, "");
		fwrite("\n", 1, 1, otp);
	}

	return 0;
}

int otp_write_boot_config(FILE *otp, uint8_t boot_config)
{
	uint8_t boot_line[8];
	char hex[17];
	int ret;

	memset(boot_line, 0, sizeof(boot_line));
	/* Byte 0 is signature of OTP line, Byte 1 is length of OTP data
	 * in 32-byte lines. Refer to MW30X Bootrom Functional Spec 0.8 Section
	 * 5 (OTP data format) for details.
	 */
	boot_line[0] = 0xC;
	boot_line[1] = 1;
	boot_line[2] = boot_config;

	bin2hex(boot_line, hex, sizeof(boot_line), sizeof(hex));

	ret = fwrite(hex, 1, 16, otp);
	sboot_assert((ret == 16), , -errno, "");
	fwrite("\n", 1, 1, otp);

	return 0;
}

int otp_write_user_data(FILE *otp, const uint8_t *udata, uint32_t len)
{
	/* TODO: Add support. At the moment don't know the 0th byte
	 * (signature) for OTP user data
	 */
	return -1;
}

int otp_write_header(FILE *otp)
{
	return 0;
}

int otp_close_file(FILE *otp)
{
	fclose(otp);
	return 0;
}
#endif
