#
# Copyright (C) 2008-2014, Marvell International Ltd.
# All Rights Reserved.
#
# Makefile to build boot2
#

ifeq ($(SDK_PATH),)
$(error "Please specify absolute path to SDK: SDK_PATH=/path/to/sdk")
endif

-include $(SDK_PATH)/.config

ifeq ($(CONFIG_CPU_MW300), y)
export CPU=mw300
export MCPU=cortex-m4
else
export CPU=mc200
export MCPU=cortex-m3
endif

# Handle boot2 version
BOOT2_VERSION_INTERNAL=1.1
BOOT2_VERSION=\"$(BOOT2_VERSION_INTERNAL)$(BOOT2_EXTRA_VERSION)\"
export BOOT2_VERSION
export CONFIG_ENABLE_ROM_LIBS
export CONFIG_CPU_MC200

#Config option - JTAG lock password for 88MC200 only
#export JTAG_LOCK_PASSWORD ?= 0x12345678

#Config option - Debug image (semi-hosting based)
#export	DEBUG ?= boot2-semihost.axf

#Config option - Enable UART debug messages for 88MW300 only
#export UART_DEBUG=1

ifeq ($(CONFIG_CPU_MC200), y)
ifneq ($(JTAG_LOCK_PASSWORD),)
SECURITY=-secure
endif
endif

BOOT2_NAME=boot2

ifeq ($(AT),)
SB_VERBOSE = -v
endif

TARGETS = bin/$(BOOT2_NAME)$(SECURITY).bin

ifneq ($(SECURE_CONF_FILE),)
TARGETS += secure_boot2
endif

.PHONY : secure_boot2 clean bin/$(BOOT2_NAME)$(SECURITY).bin

all: $(TARGETS)

bin/$(BOOT2_NAME)$(SECURITY).bin:
	@$(MAKE) -C src/$(CPU) $(DEBUG)
	@if [ -e src/$(CPU)/$(BOOT2_NAME).bin ]; then \
	mkdir -p bin; \
	rm -f bin/*.bin; \
	cp src/$(CPU)/$(BOOT2_NAME).bin bin/$(BOOT2_NAME)$(SECURITY).bin; \
	if [ -z $(SECURE_CONF_FILE) ]; then \
	echo " [bin] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).bin"; fi; \
	fi
	@if [ -n "$(DEBUG)" ]  && [ -e src/$(CPU)/$(DEBUG) ]; then \
	cp src/$(CPU)/$(DEBUG) $(SDK_PATH)/tools/$(CPU)/OpenOCD/; \
	echo " [debug] $(SDK_PATH)/tools/$(CPU)/OpenOCD/$(DEBUG)"; \
	echo; \
	echo "############################################"; \
	echo "boot2 debug build is ready!"; \
	echo "Now, you may execute it using:"; \
	echo " # cd $(SDK_PATH)/tools/$(CPU)/OpenOCD"; \
	echo " # sudo ./ramload.sh $(DEBUG) -s"; \
	echo "############################################"; \
	echo; fi

secure_boot2: bin/$(BOOT2_NAME)$(SECURITY).bin $(SECURE_CONF_FILE)
	$(AT)$(SECURE_BOOT_TOOL) -b $< -o $(OTP_FILE) -c $(SECURE_CONF_FILE) $(SB_VERBOSE)
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).kes.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).kes.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).ks.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).ks.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).ke.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).ke.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).es.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).es.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).k.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).k.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).e.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).e.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).s.bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).s.bin"; rm -f $<; fi
	$(AT)if [ -e $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).bin ]; then echo " [secure] $(CURDIR)/bin/$(BOOT2_NAME)$(SECURITY).bin"; fi
	$(AT)if [ -e $(OTP_FILE) ]; then echo " [secure] $(OTP_FILE)"; fi

clean:
	@$(MAKE) -C src/$(CPU) clean
	@rm -f bin/*.bin
	-@rm -f $(OTP_FILE)
	@if [ -n "$(DEBUG)" ]; then \
	rm -f $(SDK_PATH)/tools/$(CPU)/OpenOCD/$(DEBUG); fi
